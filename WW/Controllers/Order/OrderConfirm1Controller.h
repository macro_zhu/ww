//
//  OrderConfirm1Controller.h
//  WW
//
//  Created by Macro on 15/6/11.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "BasicViewController.h"
#import "ProductDetailData.h"

@interface OrderConfirm1Controller : BasicViewController

@property (nonatomic,strong) ProductDetailData *productDataModel;
@end
