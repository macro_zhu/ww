//
//  OrderPaySuccessController.m
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "OrderPaySuccessController.h"

@interface OrderPaySuccessController ()
@property (strong, nonatomic) IBOutlet UIImageView *successInfoImg;
@property (strong, nonatomic) IBOutlet UILabel *consignessNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *consignessPhoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *consifnessAddressLbl;
@property (strong, nonatomic) IBOutlet UIButton *lookOverOrderBtn;
@property (strong, nonatomic) IBOutlet UIButton *continueDuiHuanBtn;

@end

@implementation OrderPaySuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
    [self setNavTitle:@"订单完成"];
    @weakify(self)
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分

#pragma mark - Delegate


@end
