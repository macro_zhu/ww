//
//  OrderConfirm1Controller.m
//  WW
//
//  Created by Macro on 15/6/11.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "OrderConfirm1Controller.h"
#import "AddressGetProductListData.h"
#import "AddressGetProductListAddressList.h"

@interface OrderConfirm1Controller (){
    NSArray *_addressArray;
    
    NSInteger shopNum;
}

@property (strong, nonatomic) IBOutlet UIView *addAddressView;
@property (strong, nonatomic) IBOutlet UILabel *consignessNameLbl;
@property (strong, nonatomic) IBOutlet UIView *addressInfoView;
@property (strong, nonatomic) IBOutlet UIImageView *locationIconImg;

@property (strong, nonatomic) IBOutlet UILabel *consifnessPhoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *consignessAddressLbl;

@property (strong, nonatomic) IBOutlet UIView *shoppingInfoView;
@property (strong, nonatomic) IBOutlet UIImageView *shoppingImg;
@property (strong, nonatomic) IBOutlet UILabel *shoppingNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingInfoLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingNumLbl;

@property (strong, nonatomic) IBOutlet UIButton *subtractBtn;
@property (strong, nonatomic) IBOutlet UILabel *duihuanCountLbl;
@property (strong, nonatomic) IBOutlet UIButton *addBtn;

@property (strong, nonatomic) IBOutlet UILabel *transportTypeLbl;

@property (strong, nonatomic) IBOutlet UILabel *shoppingCountLbl;
@property (strong, nonatomic) IBOutlet UIButton *confirmBtn;

@end

@implementation OrderConfirm1Controller

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    shopNum = 1;
}


- (void)initControls{
    _addressInfoView.hidden = YES;
    
    @weakify(self)
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    gesture1.numberOfTapsRequired = 1;
    [_addAddressView addGestureRecognizer:gesture1];
    
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    gesture2.numberOfTapsRequired = 1;
    [_shoppingInfoView addGestureRecognizer:gesture2];
    
    [[_confirmBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self performSegueWithIdentifier:@"OrderConfirmToOrderDetail" sender:nil];
    }];
    
    [[_subtractBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        shopNum--;
        if (shopNum<1) {
            shopNum = 1;
        }
        self.duihuanCountLbl.text = [NSString stringWithFormat:@"%ld",shopNum];
        self.shoppingNumLbl.text = [NSString stringWithFormat:@"x%ld",shopNum];
        self.shoppingCountLbl.text = [NSString stringWithFormat:@"%ld",shopNum*_productDataModel.price];
    }];
    
    [[_addBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        shopNum++;
        self.duihuanCountLbl.text = [NSString stringWithFormat:@"%ld",shopNum];
        self.shoppingNumLbl.text = [NSString stringWithFormat:@"x%ld",shopNum];
        self.shoppingCountLbl.text = [NSString stringWithFormat:@"%ld",shopNum*_productDataModel.price];
    }];
    
    [self setNavTitle:@"订单确认"];
    
    [self setNavBackWithComeplete:^{
       @strongify(self)
      [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)initData{
    if ([UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
       [self getAddressListService];
    }
    
    //数据部分
    [self.shoppingImg sd_setImageWithURL:[NSURL URLWithString:[_productDataModel.imageList objectAtIndex:0]] placeholderImage:nil];
    self.shoppingNameLbl.text = _productDataModel.categoryName;
    self.shoppingInfoLbl.text = _productDataModel.productName;
    self.shoppingPriceLbl.text = [NSString stringWithFormat:@"汪汪币 %ld",_productDataModel.price];
    self.shoppingNumLbl.text = [NSString stringWithFormat:@"x%ld",shopNum];
    self.shoppingCountLbl.text = [NSString stringWithFormat:@"%ld",shopNum*_productDataModel.price];
}

#pragma mark - 自定义函数部分
- (void)resetAddress{
    if (_addressArray.count>0) {
        AddressGetProductListAddressList *model = _addressArray[0];
        self.consignessNameLbl.text = [NSString stringWithFormat:@"收货人:%@",model.consignee];
        self.consifnessPhoneLbl.text = model.phone;
        self.consignessAddressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.district,model.address];
    }
}

#pragma mark - 响应用户操作部分
- (void)tapGesture:(UITapGestureRecognizer*)gesture{
    UIView *view = gesture.view;
    if (view == _addAddressView) {
        [self performSegueWithIdentifier:@"OrderConfirmToDistrictList" sender:nil];
    }else if (view == _shoppingInfoView){
        
    }else{
    
    }
}

#pragma mark - 网络请求部分
- (void)getAddressListService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    [AppNetService getAddressListService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"]} complete:^(BOOL isSuccess, AddressGetProductListData *model) {
        [hub hide:YES];
        if (isSuccess) {
            _addressArray = model.addressList;
            [self resetAddress];
        }
    }];
}

#pragma mark - Delegate
#pragma mark - Push 
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}

@end
