//
//  OrderDetailController.m
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "OrderDetailController.h"

@interface OrderDetailController ()
@property (strong, nonatomic) IBOutlet UILabel *orderNumLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderCountLbl;
@property (strong, nonatomic) IBOutlet UIImageView *shoppingImg;
@property (strong, nonatomic) IBOutlet UILabel *shoppingNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingInfoLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingNumLbl;

@property (strong, nonatomic) IBOutlet UILabel *consignesLbl;
@property (strong, nonatomic) IBOutlet UIButton *payBtn;

@property (strong, nonatomic) IBOutlet UIView *consigneeView;
@property (strong, nonatomic) IBOutlet UILabel *consigneeNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *consigneePhoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *consigneeAddressLbl;

@end

@implementation OrderDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
   
    [self setNavTitle:@"订单详情"];
    @weakify(self)
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    if (_isMineOrderList) {
        self.payBtn.hidden = YES;
        self.consignesLbl.hidden = NO;
    }else{
        self.payBtn.hidden = NO;
        self.consignesLbl.hidden = YES;
    }
}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分

#pragma mark - Delegate


@end
