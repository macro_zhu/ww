//
//  DistrictManageTableController.m
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "DistrictManageTableController.h"
#import "DistrictManageTableCell.h"
#import "AddressDistrictListData.h"
#import "AddressDistrictListRegionList.h"
#import "DistrictDetailTableController.h"

@interface DistrictManageTableController (){
    NSArray *_addressArray;
    
    NSString *_provinceStr;
    NSString *_cityStr;
    NSString *_districtStr;
    NSString *_addressStr;
    
    NSInteger _provinceId;
    NSInteger _cityId;
    NSInteger _districtId;
   
    NSNumber *_regionId;
    //标志现在处于哪一个层数
    NSInteger _cengFlag;
}

@end

@implementation DistrictManageTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    _cengFlag = 1;
}

- (void)initControls{
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    
    [self setNavTitle:@"收货地址管理"];
    @weakify(self)
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)initData{
    [self getAddressServiceWithRegionId:@(_cengFlag)];
}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分
- (void)getAddressServiceWithRegionId:(NSNumber*)regionId{
    
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    [AppNetService getAddressDistrictListService:@{@"region_id":regionId} complete:^(BOOL isSuccess, AddressDistrictListData*model) {
        [hub hide:YES];
        if (isSuccess) {
            _addressArray = model.regionList;
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Delegate
#pragma mark - TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _cengFlag;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_cengFlag == 1) {
        return _addressArray.count;
    }else if(_cengFlag == 2){
        if (section == 0) {
            return 1;
        }else{
            return _addressArray.count;
        }
    }else if (_cengFlag == 3){
        if (section ==0) {
            return 1;
        }else if (section == 1){
            return 1;
        }else{
            return _addressArray.count;
        }
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DistrictManageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DistrictManageTableCell"];
    cell.userInteractionEnabled = YES;
    AddressDistrictListRegionList *model = _addressArray[indexPath.row];
    if (_cengFlag == 1) {
        cell.districtLbl.text = model.regionName;
    }else if(_cengFlag == 2){
        if (indexPath.section==0) {
            cell.backgroundColor = [UIColor colorWithHexString:@"D5D5D5"];
            cell.userInteractionEnabled = NO;
            cell.districtLbl.text = _provinceStr;
        }else{
            cell.districtLbl.text = model.regionName;
        }
    }else{
        if (_cengFlag == 3) {
            if (indexPath.section == 0) {
                cell.backgroundColor = [UIColor colorWithHexString:@"D5D5D5"];
                cell.userInteractionEnabled = NO;
                cell.districtLbl.text = _provinceStr;
            }else if (indexPath.section == 1){
                cell.backgroundColor = [UIColor colorWithHexString:@"E5E5E5"];
                cell.userInteractionEnabled = NO;
                cell.districtLbl.text = _cityStr;
            }else{
                cell.districtLbl.text = model.regionName;
            }
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DistrictManageTableCell *cell = (DistrictManageTableCell*)[tableView cellForRowAtIndexPath:indexPath];

    AddressDistrictListRegionList *model = _addressArray[indexPath.row];
    _regionId = @(model.regionId);
    [self getAddressServiceWithRegionId:_regionId];
    if (_cengFlag== 1) {
        _provinceId = model.regionId;
        _provinceStr = cell.districtLbl.text;
    }else if(_cengFlag == 2){
        _cityId = model.regionId;
        _cityStr = cell.districtLbl.text;
    }else{
        _districtId = model.regionId;
        _districtStr = cell.districtLbl.text;
        [self performSegueWithIdentifier:@"DistrictManageToDistrictDetail" sender:nil];
    }
    
        ++_cengFlag;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DistrictDetailTableController *districtDetailCtrl = (DistrictDetailTableController*)segue.destinationViewController;
    districtDetailCtrl.districtStr = [NSString stringWithFormat:@"%@%@%@",_provinceStr,_cityStr,_districtStr];
    districtDetailCtrl.provinceId = _provinceId;
    districtDetailCtrl.cityId = _cityId;
    districtDetailCtrl.districtId = _districtId;
}

@end
