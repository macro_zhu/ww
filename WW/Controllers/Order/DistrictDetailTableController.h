//
//  DistrictDetailTableController.h
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistrictDetailTableController : UITableViewController
@property (nonatomic,copy) NSString *districtStr;
@property (nonatomic,assign) NSInteger provinceId;
@property (nonatomic,assign) NSInteger cityId;
@property (nonatomic,assign) NSInteger districtId;
@end
