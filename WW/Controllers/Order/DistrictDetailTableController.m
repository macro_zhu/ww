//
//  DistrictDetailTableController.m
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "DistrictDetailTableController.h"
#import "OrderConfirm1Controller.h"
#import "DistrictListController.h"

@interface DistrictDetailTableController ()<UITableViewDelegate,UITableViewDataSource>{
    BOOL isDetailAddressValid;
    BOOL isConsignessNameValid;
    BOOL isConsignessPhoneValid;
}
@property (strong, nonatomic) IBOutlet UITextField *districtTF;
@property (strong, nonatomic) IBOutlet UILabel *districtLbl;

@property (strong, nonatomic) IBOutlet UITextField *detailAddressTF;
@property (strong, nonatomic) IBOutlet UILabel *detailAddressLbl;

@property (strong, nonatomic) IBOutlet UITextField *consignessNameTF;
@property (strong, nonatomic) IBOutlet UILabel *consignessNameLbl;

@property (strong, nonatomic) IBOutlet UITextField *consignessPhoneTF;
@property (strong, nonatomic) IBOutlet UILabel *consignessPhoneLbl;
@end

@implementation DistrictDetailTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBaseProperties];
    [self initControls];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    _districtTF.text = _districtStr;
}

- (void)initControls{
    _districtTF.userInteractionEnabled = NO;
    _districtLbl.hidden = NO;
    
    [[_detailAddressTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length >0);
    }] subscribeNext:^(NSNumber* x) {
        _detailAddressLbl.hidden = !x.boolValue;
        isDetailAddressValid = x.boolValue;
    }];
    
    [[_consignessNameTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length >0);
    }] subscribeNext:^(NSNumber* x) {
        _consignessNameLbl.hidden = !x.boolValue;
        isConsignessNameValid = x.boolValue;
    }];
    
    [[_consignessPhoneTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length >0);
    }] subscribeNext:^(NSNumber* x) {
        _consignessPhoneLbl.hidden = !x.boolValue;
    }];
    
    [[_consignessPhoneTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length == 11);
    }] subscribeNext:^(NSNumber* x) {
        isConsignessNameValid = x.boolValue;
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self setNavTitle:@"收货地址管理"];
    @weakify(self)
    [self setNavBackWithComeplete:^{
        @strongify(self)
        for (UIViewController *ctrl  in self.navigationController.viewControllers) {
            if ([ctrl isKindOfClass:[DistrictListController class]]) {
                DistrictListController *orderCtrl = (DistrictListController*)ctrl;
                [self.navigationController popToViewController:orderCtrl animated:YES];
            }
        }
    }];
}

- (void)initData{
    
}

#pragma mark - 自定义函数部分
- (void)clickRightNavButton{
    [_detailAddressTF resignFirstResponder];
    [_consignessNameTF resignFirstResponder];
    [_consignessPhoneTF resignFirstResponder];
    if (isConsignessPhoneValid && isDetailAddressValid && isConsignessNameValid) {
        [self getAddressAddService];
    }else if (!isDetailAddressValid){
        [ToastUtil showToast:@"地址输入不规范，请重新输入!"];
        [_detailAddressTF becomeFirstResponder];
    }else if (!isConsignessNameValid){
        [ToastUtil showToast:@"收货人姓名输入不规范，请重新输入!"];
        [_consignessNameTF becomeFirstResponder];
    }else{
        [ToastUtil showToast:@"收货人联系电话输入不规范，请重新输入!"];
        [_consignessPhoneTF becomeFirstResponder];
    }
}

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分
- (void)getAddressAddService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[UserDefaultsUtils valueWithKey:@"TOKEN"] forKey:@"token"];
    [dic setObject:_consignessNameTF.text forKey:@"consigness"];
    [dic setObject:_consignessPhoneTF.text forKey:@"phone"];
    [dic setObject:@(_provinceId) forKey:@"province_id"];
    [dic setObject:@(_cityId) forKey:@"city_id"];
    [dic setObject:@(_districtId) forKey:@"district_id"];
    [dic setObject:_districtTF.text forKey:@"address"];
    [AppNetService getAddressAddService:dic complete:^(BOOL isSuccess, NSString *errorMsg) {
        [hub hide:YES];
        if (isSuccess) {
            [ToastUtil showToast:@"添加地址成功"];
        }else{
            [ToastUtil showToast:@"添加地址失败，请重新添加"];
        }
    }];
}

#pragma mark - Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        [_detailAddressTF becomeFirstResponder];
    }else if (indexPath.row == 2){
        [_consignessNameTF becomeFirstResponder];
    }else if (indexPath.row == 3){
        [_consignessPhoneTF becomeFirstResponder];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

@end
