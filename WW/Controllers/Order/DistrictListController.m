//
//  DistrictListController.m
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "DistrictListController.h"
#import "DistrictListNormalTableCell.h"
#import "DistrictListDefaultTableCell.h"
#import "AddressGetProductListData.h"

@interface DistrictListController ()<UITableViewDelegate,UITableViewDataSource>{
    NSArray *_addressListArray;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *addAddressBtn;

@end

@implementation DistrictListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBaseProperties];
    [self initControls];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
    [self setNavTitle:@"订单确认"];
    @weakify(self)
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
}

- (void)initData{
    [self getAddressListService];
}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分

- (void)getAddressListService{
    [AppNetService getAddressListService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"]} complete:^(BOOL isSuccess, AddressGetProductListData *listData) {
        if (isSuccess) {
            _addressListArray = listData.addressList;
            [self.tableView reloadData];
        }
    }];
}

- (void)setDefaultAddressService:(NSInteger)addressId{
    [AppNetService setDefaultAddressService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"address_id":@(addressId)} complete:^(BOOL isSuccess, NSString* errorMsg) {
        if (isSuccess) {
            [ToastUtil showToast:@"设置默认地址成功!"];
            [self getAddressListService];
        }
    }];
}

#pragma mark - Delegate
#pragma mark - TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _addressListArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DistrictListNormalTableCell * normalCell = [tableView dequeueReusableCellWithIdentifier:@"DistrictListNormalTableCell"];
    DistrictListDefaultTableCell *defaultCell = [tableView dequeueReusableCellWithIdentifier:@"DistrictListDefaultTableCell"];
    AddressGetProductListAddressList *model = _addressListArray[indexPath.row];
    if (model.isDefault) {
        defaultCell.nameLbl.text = model.consignee;
        defaultCell.phoneLbl.text = model.phone;
        defaultCell.districtlbl.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.district,model.address];
        return defaultCell;
    }else{
        normalCell.nameLbl.text = model.consignee;
        normalCell.phoneLbl.text = model.phone;
        normalCell.districtLbl.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.district,model.address];
        return normalCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressGetProductListAddressList *model = _addressListArray[indexPath.row];
    if (model.isDefault) {
        [ToastUtil showToast:@"已是默认地址，无需在设置"];
    }else{
        [self setDefaultAddressService:model.addressId];
    }
}

@end
