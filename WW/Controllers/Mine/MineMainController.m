 //
//  MineMainController.m
//  WW
//
//  Created by Macro on 15/6/9.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "MineMainController.h"
#import "MineMainTableCell.h"
#import "LoginController.h"
#import "MaskView.h"
#import "WriteArticleController.h"
#import "UserInfoData.h"
#import "AttentionAndFansController.h"

@interface MineMainController ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>
{
    NSMutableArray *_tableInfoMessageArray;
    NSArray *_tableInfoIconArray;
    UIActionSheet *_actionSheet;
    BOOL isNewPhoto;
    
    BOOL _isFistNetUserInfo;
    UserInfoData *_userModel;
}
@property (strong, nonatomic) IBOutlet UIImageView *bgImg;
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
//养时间
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UIView *attentionAndFansView;
@property (strong, nonatomic) IBOutlet UIButton *attentionBtn;
@property (strong, nonatomic) IBOutlet UIButton *fansBtn;

@property (strong, nonatomic) IBOutlet UIButton *mengTuBtn;
@property (strong, nonatomic) IBOutlet UILabel *mengTuLbl;

@property (strong, nonatomic) IBOutlet UIButton *shareBtn;
@property (strong, nonatomic) IBOutlet UILabel *shareLbl;

@property (strong, nonatomic) IBOutlet UIButton *commentBtn;
@property (strong, nonatomic) IBOutlet UILabel *commentLbl;

@property (strong, nonatomic) IBOutlet UIButton *dianZanBtn;
@property (strong, nonatomic) IBOutlet UILabel *dianZanLbl;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)  NSData *picData;

@end

@implementation MineMainController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (![UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
       [[[UIAlertView alloc] initWithTitle:@"提示" message:@"您未登录，请先登录再查看此部分相关的内容！"  delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil] show];
    }
    
    if ([UserDefaultsUtils boolValueWithKey:@"IsLogin"] && _isFistNetUserInfo) {
        _isFistNetUserInfo = NO;
        [self getUserMessageService];
    }
    
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    _tableInfoMessageArray = [NSMutableArray array];
//    _tableInfoMessageArray = @[@"我的汪币: 200",@"我的任务: 9/260",@"我的兑换",@"我的资料",@"兑换商城"];
    _tableInfoIconArray = @[@"qjb",@"wrw",@"gwd",@"ues",@"car"];
    isNewPhoto = NO;
    _isFistNetUserInfo = YES;
}

- (void)initControls{
    _tableView.tableFooterView = [[UIView alloc] init];
    
    [self setNavTitle:@"我"];
    
    @weakify(self)
    [self setNavRightImage:[UIImage imageNamed:@"Camera"] complete:^{
        MaskView *view = [[MaskView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, kWindowHight)];
        [[[UIApplication sharedApplication] keyWindow] addSubview:view];
        [[view.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            [view removeFromSuperview];
        }];
        
        [view responseToBtnClickBlock:^(int tag) {
            [view removeFromSuperview];
            @strongify(self)
            [self performSegueWithIdentifier:@"MineMainToWriteArticle" sender:@(tag)];
        }];
    }];
    
    [[_attentionBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        if (_userModel.followNumber > 0) {
            [self performSegueWithIdentifier:@"MineMainToAttentionAndFans" sender:@(1)];
        }else{
            [ToastUtil showToast:@"不好意思，你暂时没有受到其他人的关注！"];
        }
    }];
    
    [[_fansBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if (_userModel.fansNumber > 0) {
            [self performSegueWithIdentifier:@"MineMainToAttentionAndFans" sender:@(2)];
        }else{
            [ToastUtil showToast:@"不好意思，您暂时没有粉丝!"];
        }
    }];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(responseToTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.headImg addGestureRecognizer:tapGesture];
    
    self.headImg.layer.cornerRadius = self.headImg.frame.size.width/2;
    self.headImg.layer.masksToBounds = YES;
    self.headImg.layer.borderWidth = 1;
    self.headImg.layer.borderColor = [UIColor colorWithHexString:@"FFBB41"].CGColor;
    
    self.headImg.image = [UIImage imageWithData:[[AppContextManager shareManager] headData]];
}

#pragma mark - 自定义函数部分

- (void)openCamera{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:^{
            
        }];
        isNewPhoto = YES;
    }else{
        [ToastUtil showToast:@"相机不可使用!"];
        DLog(@"相机不可使用!");
    }
}

- (void)openAlbum{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:^{
            
        }];
        isNewPhoto = NO;
    }else{
        [ToastUtil showToast:@"相册不可使用!"];
        DLog(@"相册不可使用!");
    }
}

- (void)firstWarming{
    if (![UserDefaultsUtils boolValueWithKey:@"MainHeadImage"]) {
        [UserDefaultsUtils saveBoolValue:YES withKey:@"MainHeadImage"];
        [[[UIAlertView alloc] initWithTitle:@"提示" message:@"点击头像可以进行选择替换" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
    }
}

- (void)resetUserMessage:(UserInfoData*)model{
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:nil];
    self.nameLbl.text = model.alias;
    self.timeLbl.text = [NSString stringWithFormat:@"%@%@",model.petType,model.petAge];
    [self.attentionBtn setTitle:[NSString stringWithFormat:@"关注:%ld",model.followNumber] forState:UIControlStateNormal];
    [self.fansBtn setTitle:[NSString stringWithFormat:@"粉丝:%ld",model.fansNumber] forState:UIControlStateNormal];
    
    self.mengTuLbl.text = [NSString stringWithFormat:@"萌图:%ld",model.articleNumber];
    self.shareLbl.text = [NSString stringWithFormat:@"分享:%ld",model.shareNumber];
    self.commentLbl.text = [NSString stringWithFormat:@"评论:%ld",model.commentNumber];
    self.dianZanLbl.text = [NSString stringWithFormat:@"点赞:%ld",model.praiseNumber];
    
    [_tableInfoMessageArray addObject:[NSString stringWithFormat:@"我的汪币:%ld",model.totalCoins]];
    [_tableInfoMessageArray addObject:[NSString stringWithFormat:@"我的任务:%ld/%ld",model.coinsGainedToday,model.coinsLimit]];
    [_tableInfoMessageArray addObject:@"我的兑换"];
    [_tableInfoMessageArray addObject:@"我的资料"];
    [_tableInfoMessageArray addObject:@"兑换商城"];
    [self.tableView reloadData];
}

#pragma mark - 响应用户操作部分
- (void)responseToTapGesture:(UIGestureRecognizer*)gesture{
    if (gesture.view == self.headImg) {
        _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                     destructiveButtonTitle:@"相机"
                                          otherButtonTitles:@"相册", nil];
        [_actionSheet showInView:self.view];
    }
}

#pragma mark - 网络请求部分
- (void)getUserMessageService{
    @weakify(self)
    [AppNetService getuserMainHoneService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"]} complete:^(BOOL isSuccess, UserInfoData*model) {
        @strongify(self)
        if (isSuccess) {
            [self resetUserMessage:model];
            _userModel = model;
        }
    }];
}

#pragma mark - Delegate
#pragma mark - TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _tableInfoMessageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MineMainTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MineMainTableCell"];
    cell.infoLbl.text = _tableInfoMessageArray[indexPath.row];
    cell.iconImg.image = [UIImage imageNamed:_tableInfoIconArray[indexPath.row]];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithHexString:@"F5F5F5"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DLog(@"%@",_tableInfoMessageArray[indexPath.row]);
    
    if (![UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
        [[[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能必须先登录!" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
        return;
    }
    
    if (indexPath.row == 0) {
        
    }else if (indexPath.row == 1){
        [TabbarUtil changeViewControllerWithTabbarIndex:1];
    }else if (indexPath.row == 2){
        [self performSegueWithIdentifier:@"MineMainToMineOrder" sender:nil];
    }else if (indexPath.row == 3){
        [self performSegueWithIdentifier:@"MineMainToGouGouRegister" sender:nil];
    }else if (indexPath.row == 4){
        [TabbarUtil changeViewControllerWithTabbarIndex:2];
    }
}

#pragma mark - TableChooseAlterDelegate And DataSource
- (NSArray*) tableViewChooseAlterDataSource{
    return @[@"1",@"2",@"3",@"4",@"5"];
}

#pragma mark - Push
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"MineMainToWriteArticle"]) {
        WriteArticleController *ctrl = segue.destinationViewController;
        ctrl.photoSourceType = [(NSNumber*)sender integerValue];
    }else if ([segue.identifier isEqualToString:@"MineMainToMineOrder"]){
    
    }else if ([segue.identifier isEqualToString:@"MineMainToGouGouRegister"]){
    
    }else if([segue.identifier isEqualToString:@"MineMainToAttentionAndFans"]){
        AttentionAndFansController *ctrl = (AttentionAndFansController*)segue.destinationViewController;
        ctrl._listTypeFlag = [(NSNumber*)sender integerValue];
    }
}

#pragma mark - ActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet == _actionSheet) {
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            [actionSheet dismissWithClickedButtonIndex:actionSheet.cancelButtonIndex animated:YES];
        }else if(buttonIndex == actionSheet.destructiveButtonIndex){
            [self openCamera];
        }else if (buttonIndex == actionSheet.firstOtherButtonIndex){
            [self openAlbum];
        }
    }
}

#pragma mark - ImagePicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([type isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSData *data;
        self.headImg.image = image;
        if (UIImagePNGRepresentation(image)) {
            data = UIImagePNGRepresentation(image);
        }else{
            data = UIImageJPEGRepresentation(image, 1.0);
        }
        _picData = data;
        [[AppContextManager shareManager] setHeadData:data];
        
        NSString *documentPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"PicDocument"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        [fileManager createDirectoryAtPath:documentPath withIntermediateDirectories:YES attributes:nil error:nil];
        [fileManager createFileAtPath:[documentPath stringByAppendingPathComponent:@"touXiang.png"] contents:data attributes:nil];
        
        if (isNewPhoto) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self firstWarming];
    }];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if(error){
        [ToastUtil showToast:@"将图片添加相册失败!"];
    }
}

#pragma mark - AlterView Deleagete
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginController *loginCtrl = [storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }else if(buttonIndex == alertView.cancelButtonIndex){
        [TabbarUtil changeViewControllerWithTabbarIndex:0];
    }
}

@end
