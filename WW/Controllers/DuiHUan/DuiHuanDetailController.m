//
//  DuiHuanDetailController.m
//  WW
//
//  Created by Macro on 15/6/9.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "DuiHuanDetailController.h"
#import "ProductDetailData.h"
#import "OrderConfirm1Controller.h"
#import "MaskView.h"
#import "WriteArticleController.h"
#import "LoginController.h"

@interface DuiHuanDetailController ()<UIAlertViewDelegate>{
    ProductDetailData *detailDataModel;
}
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *peopelNumDuiLbl;
@property (strong, nonatomic) IBOutlet UIWebView *productDesWeb;
@property (strong, nonatomic) IBOutlet UIButton *duiHuanBtn;
@property (strong, nonatomic) IBOutlet UIButton *wangBiBtn;

@end

@implementation DuiHuanDetailController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
    [self getDuiHuanDetailService];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
    @weakify(self)
    [[_duiHuanBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        if ([UserDefaultsUtils valueWithKey:@"IsLogin"]) {
             [self performSegueWithIdentifier:@"DuiHUanDetailToOrderConfirm" sender:detailDataModel];
        }else{
            [[[UIAlertView alloc] initWithTitle:@"提示" message:@"您需要先登录才能进行兑换" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil] show];
        }
    }];
    
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setNavTitle:@"汪汪"];
    
    [self setNavRightImage:[UIImage imageNamed:@"Camera"] complete:^{
        MaskView *view = [[MaskView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, kWindowHight)];
        [[[UIApplication sharedApplication] keyWindow] addSubview:view];
        [[view.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            [view removeFromSuperview];
        }];
        
        [view responseToBtnClickBlock:^(int tag) {
            [view removeFromSuperview];
            @strongify(self)
            [self performSegueWithIdentifier:@"DuiHuanDetailToWriteArticle" sender:@(tag)];
        }];
    }];
}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分
- (void)getDuiHuanDetailService{
    @weakify(self)
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    [AppNetService getproductDetailService:[UserDefaultsUtils valueWithKey:@"TOKEN"]?@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"product_id":@(_productId)}:@{@"product_id":@(_productId)}
                                  complete:^(BOOL isSuccess, ProductDetailData *model) {
        @strongify(self)
        [hub hide:YES];
        if (isSuccess) {
            [self.headImg sd_setImageWithURL:[NSURL URLWithString:model.imageList[0]] placeholderImage:nil];
            self.priceLbl.text = [NSString stringWithFormat:@"%ld汪币",model.price];
            self.peopelNumDuiLbl.text = [NSString stringWithFormat:@"%ld人已兑换",model.sales];
            [self.productDesWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.detailUrl]]];
            detailDataModel = model;
        }
    }];
}


#pragma mark - Delegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"DuiHUanDetailToOrderConfirm"]) {
        OrderConfirm1Controller *ctrl = (OrderConfirm1Controller*)segue.destinationViewController;
        ctrl.productDataModel = (ProductDetailData*)sender;
    }else if ([segue.identifier isEqualToString:@"DuiHuanDetailToWriteArticle"]){
        WriteArticleController *ctrl = (WriteArticleController*)segue.destinationViewController;
        ctrl.photoSourceType = [(NSNumber*)sender integerValue];
    }
}

#pragma mark - AlterView Deleagete
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginController *loginCtrl = [storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }else if(buttonIndex == alertView.cancelButtonIndex){
       
    }
}
@end
