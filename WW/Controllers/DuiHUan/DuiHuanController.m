//
//  DuiHuanController.m
//  WW
//
//  Created by Macro on 15/6/8.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "DuiHuanController.h"
#import "DuiHuanTableCell.h"
#import "ProductListData.h"
#import "ProductList.h"
#import "DuiHuanDetailController.h"
#import "MaskView.h"
#import "WriteArticleController.h"

@interface DuiHuanController ()<UITableViewDelegate,UITableViewDataSource>{
    NSInteger _pageNo;
    NSArray *_dataModelArray;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DuiHuanController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getDuiHuanListService];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    _pageNo = 1;
}


- (void)initControls{
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"f2f2f2"];
    
    @weakify(self)
    [RACObserve([AppContextManager shareManager], headData) subscribeNext:^(id x) {
        @strongify(self)
        [self setNavLeftImageWithImageData:[[AppContextManager shareManager] headData] complete:^{
            [TabbarUtil changeViewControllerWithTabbarIndex:3];
        }];
    }];
    
    
    [self setNavTitle:@"兑换"];

    [self setNavRightImage:[UIImage imageNamed:@"Camera"] complete:^{
        MaskView *view = [[MaskView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, kWindowHight)];
        [[[UIApplication sharedApplication] keyWindow] addSubview:view];
        [[view.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            [view removeFromSuperview];
        }];
        
        [view responseToBtnClickBlock:^(int tag) {
            [view removeFromSuperview];
            @strongify(self)
            [self performSegueWithIdentifier:@"DuiHuanListToWriteArticle" sender:@(tag)];
        }];
    }];
}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分
- (void)getDuiHuanListService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    NSMutableDictionary *dic= [NSMutableDictionary dictionary];
    [dic setObject:@(_pageNo) forKey:@"page_no"];
    [dic setObject:@(KPageNum) forKey:@"page_num"];
    if ([UserDefaultsUtils valueWithKey:@"TOKEN"]) {
        [dic setObject:[UserDefaultsUtils valueWithKey:@"TOKEN"] forKey:@"token"];
    }
    [AppNetService getProductListService:dic complete:^(BOOL isSuccess, ProductListData *model) {
        [hub hide:YES];
        if (isSuccess) {
            _dataModelArray = model.productList;
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - 网络请求部分

#pragma mark - Delegate
#pragma mark - TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataModelArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DuiHuanTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DuiHuanTableCell"];
    ProductList *model = _dataModelArray[indexPath.row];
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:nil];
    cell.classifyTitleLbl.text = model.categoryName;
    cell.titleLbl.text = model.productName;
    cell.shoppinginfoLbl.text = model.summary;
    cell.priceLbl.text = [NSString stringWithFormat:@"%ld",(long)model.price];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
     ProductList *model = _dataModelArray[indexPath.row];
    [self performSegueWithIdentifier:@"DuiHuanListToDetail" sender:model];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"DuiHuanListToDetail"]) {
        DuiHuanDetailController *duiHuanDetailCtrl = (DuiHuanDetailController*)segue.destinationViewController;
        ProductList *model = sender;
        duiHuanDetailCtrl.productId = model.productId; 
    }else if([segue.identifier isEqualToString:@"DuiHuanListToWriteArticle"]){
        WriteArticleController *ctrl = (WriteArticleController*)segue.destinationViewController;
        ctrl.photoSourceType = [(NSNumber*)sender integerValue];
    }
}
@end
