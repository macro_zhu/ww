//
//  TaskTableCell.h
//  WW
//
//  Created by Macro on 15/6/26.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LDProgressView.h"

@interface TaskTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *rewardLbl;
@property (strong, nonatomic) IBOutlet UILabel *maxLimit;
@property (strong, nonatomic) IBOutlet UILabel *currentProNumLbl;
@property (strong, nonatomic) IBOutlet LDProgressView *progressView;
@property (strong, nonatomic) IBOutlet UIView *proViewBgView;

@property (strong, nonatomic) IBOutlet UILabel *stateLbl;

@end
