//
//  TaskController.m
//  WW
//
//  Created by Macro on 15/6/26.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "TaskController.h"
#import "LDProgressView.h"
#import "TaskTableCell.h"
#import "UserTaskInfoData.h"
#import "UserTaskInfoTaskInfo.h"
#import "LoginController.h"
#import "MaskView.h"
#import "WriteArticleController.h"

@interface TaskController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>{
    NSArray *_taskInfoArray;
    
    UserTaskInfoData *_dataModel;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UILabel *wangMoneyLbl;

@property (strong, nonatomic) IBOutlet UILabel *currentProNumLbl;
@property (strong, nonatomic) IBOutlet LDProgressView *moneyProgressView;
@property (strong, nonatomic) IBOutlet UILabel *maxProNumLbl;

@property (strong, nonatomic) IBOutlet UILabel *getWangMoneyLbl;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation TaskController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initBaseProperties];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initDatasource];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}

- (void)initControls{
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self setNavTitle:@"任务"];
    @weakify(self)
    [RACObserve([AppContextManager shareManager], headData) subscribeNext:^(id x) {
        @strongify(self)
        [self setNavLeftImageWithImageData:[[AppContextManager shareManager] headData] complete:^{
            [TabbarUtil changeViewControllerWithTabbarIndex:3];
        }];
    }];

    self.headImg.layer.cornerRadius = self.headImg.frame.size.width/2;
    self.headImg.layer.masksToBounds = YES;
    self.headImg.layer.borderWidth = 2;
    self.headImg.layer.borderColor = [UIColor colorWithHexString:@"FFBD3D"].CGColor;
    
    [self setNavRightImage:[UIImage imageNamed:@"Camera"] complete:^{
        MaskView *view = [[MaskView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, kWindowHight)];
        [[[UIApplication sharedApplication] keyWindow] addSubview:view];
        [[view.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            [view removeFromSuperview];
        }];
        
        [view responseToBtnClickBlock:^(int tag) {
            [view removeFromSuperview];
            @strongify(self)
            [self performSegueWithIdentifier:@"TaskToWriteArticle" sender:@(tag)];
        }];
    }];
}

- (void)initDatasource{
    if (![UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
       [[[UIAlertView alloc] initWithTitle:@"提示" message:@"您未登录，请先登录再查看此部分相关的内容！"  delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil] show];
    }
    
    if ([UserDefaultsUtils valueWithKey:@"TOKEN"]) {
        [self getTaskInfoService];
    }else{
    
    }
}

#pragma mark - 自定义函数部分

- (void)resetView{
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:_dataModel.avatar] placeholderImage:nil];
    self.wangMoneyLbl.text = [NSString stringWithFormat:@"旺币:%ld",(long)_dataModel.totalCoins];
    self.currentProNumLbl.text = [NSString stringWithFormat:@"%ld",(long)_dataModel.coinsGainedToday];
    self.maxProNumLbl.text = [NSString stringWithFormat:@"%ld",(long)_dataModel.coinsLimit];
    self.moneyProgressView.progress = (float)_dataModel.coinsGainedToday/_dataModel.coinsLimit;
    self.getWangMoneyLbl.text = [NSString stringWithFormat:@"今日已获取旺币: %ld/%ld",(long)_dataModel.coinsGainedToday,(long)_dataModel.coinsLimit];

    [self.tableView reloadData];
}

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分
- (void)getTaskInfoService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    [AppNetService getUserTaskMessageService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"]} complete:^(BOOL isSuccess, UserTaskInfoData*model) {
        [hub hide:YES];
        if (isSuccess) {
            _dataModel = model;
            _taskInfoArray = model.taskInfo;
            [self resetView];
        }
    }];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _taskInfoArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TaskTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TaskTableCell"];
    UserTaskInfoTaskInfo *model = _taskInfoArray[indexPath.row];
    cell.nameLbl.text = model.name;
    cell.rewardLbl.text =[NSString stringWithFormat:@"+%ld",(long)model.reward];
    cell.maxLimit.text = [NSString stringWithFormat:@"%ld",(long)model.gainLimit];
    if (model.gained == 0) {
        cell.stateLbl.hidden = NO;
        cell.proViewBgView.hidden = YES;
        cell.stateLbl.textColor = [UIColor lightGrayColor];
        cell.stateLbl.text = @"未开始";
    }else if(model.gained == model.gainLimit){
        cell.stateLbl.hidden = NO;
        cell.proViewBgView.hidden = YES;
        cell.stateLbl.textColor = [UIColor colorWithHexString:@"ff8c3e"];
        cell.stateLbl.text = @"已完成";
    }else{
        cell.stateLbl.hidden = YES;
        cell.proViewBgView.hidden = NO;
        cell.currentProNumLbl.text = [NSString stringWithFormat:@"+%ld",(long)model.gained];
        cell.progressView.progress = (float)model.gained/model.gainLimit;
    }
     return cell;
}

#pragma mark - AlterView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginController *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if (buttonIndex == alertView.cancelButtonIndex){
        [TabbarUtil changeViewControllerWithTabbarIndex:0];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"TaskToWriteArticle"]) {
        WriteArticleController *ctrl = (WriteArticleController*)segue.destinationViewController;
        ctrl.photoSourceType = [(NSNumber*)sender integerValue];
    }
}

@end
