//
//  LoginController.m
//  WW
//
//  Created by Macro on 15/6/10.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "LoginController.h"
#import "LoginData.h"
#import "UMSocial.h"

@interface LoginController (){
    LoginData *_loginDataModel;
}
@property (strong, nonatomic) IBOutlet UITextField *phoneTF;
@property (strong, nonatomic) IBOutlet UITextField *pwdTF;
@property (strong, nonatomic) IBOutlet UIButton *forgetPwdBtn;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *qqLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *weChatLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *sinaLoginBtn;

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBaseProperties];
    [self initControls];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
    
    @weakify(self)
    [[_registerBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self performSegueWithIdentifier:@"LoginToRegister" sender:nil];
    }];
    
    //普通登录部分
    if ([UserDefaultsUtils valueWithKey:@"USERNAME"]&&[UserDefaultsUtils valueWithKey:@"PASSWORD"]) {
        _phoneTF.text = [UserDefaultsUtils valueWithKey:@"USERNAME"];
        _pwdTF.text = [UserDefaultsUtils valueWithKey:@"PASSWORD"];
        [self getCommonLoginService];
    }
    
    self.loginBtn.enabled = NO;
    RACSignal *phoneSignal = [_phoneTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length == 11);
    }];
    
    [[self.phoneTF.rac_textSignal map:^id(NSString* value) {
        return   @(value.length >11);
    }] subscribeNext:^(NSNumber* x) {
        if (x.boolValue) {
             _phoneTF.text = [_phoneTF.text substringToIndex:11];
        }
    }];
   
    RACSignal *pwdSignal = [_pwdTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length >= 6);
    }];
    
    [[RACSignal combineLatest:@[phoneSignal,pwdSignal] reduce:^id(NSNumber* phoneValid,NSNumber *pwdValid){
        return @(phoneValid.boolValue && pwdValid.boolValue);
    }] subscribeNext:^(NSNumber* x) {
        @strongify(self)
        self.loginBtn.enabled = x.boolValue;
    }];
    
    [[_loginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self getCommonLoginService];
    }];
    
    //点击QQ登陆
    [[_qqLoginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
//        [self getQQLoginService];
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
        
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            
            //          获取微博用户名、uid、token等
            
            if (response.responseCode == UMSResponseCodeSuccess) {
                
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQQ];
                
                NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                [self.navigationController popToRootViewControllerAnimated:YES];
                [TabbarUtil changeViewControllerWithTabbarIndex:0];
            }});
    }];
    
    //点击微信登录
    [[_weChatLoginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
        
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            
            if (response.responseCode == UMSResponseCodeSuccess) {
                
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
                
                NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                [self.navigationController popToRootViewControllerAnimated:YES];
                [TabbarUtil changeViewControllerWithTabbarIndex:0];
            }
            
        });
    }];
    
    //点击微博登录
    [[_sinaLoginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
        
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            //获取微博用户名、uid、token等
            if (response.responseCode == UMSResponseCodeSuccess) {
                UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
                
                NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                
                [self getThirdLoginLoginService:snsAccount.usid photoUrl:snsAccount.iconURL nick:snsAccount.userName];
            }
        });
    }];
    
    [[UMSocialDataService defaultDataService] requestSnsInformation:UMShareToQQ  completion:^(UMSocialResponseEntity *response){
        NSLog(@"SnsInformation is %@",response.data);
    }];
    
    [self setNavBackItemWithTitle:@"返回" complete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setNavRightTitle:@"注册" complete:^{
        @strongify(self)
        [self performSegueWithIdentifier:@"LoginToRegister" sender:nil];
    }];
}

#pragma mark - 自定义函数部分
- (void)setUserDefaultDataAfterNetService:(LoginData*)model{
    [UserDefaultsUtils saveValue:_phoneTF.text forKey:@"USERNAME"];
    [UserDefaultsUtils saveValue:_pwdTF.text forKey:@"PASSWORD"];
    [UserDefaultsUtils saveValue:model.token forKey:@"TOKEN"];
    [UserDefaultsUtils saveBoolValue:YES withKey:@"IsLogin"];
    [[AppContextManager shareManager] setHeadData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.avatar]]];
}

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分
- (void)getCommonLoginService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:_phoneTF.text forKey:@"user_name"];
    [dic setObject:[SecretUtil md5:_pwdTF.text] forKey:@"password"];
    @weakify(self)
    [AppNetService getLoginCommonLoginService:dic complete:^(BOOL isSuccess, LoginData*model) {
        @strongify(self)
        [hub hide:YES];
        if (isSuccess) {
            _loginDataModel = model;
            [self setUserDefaultDataAfterNetService:model];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [ToastUtil showToast:@"登录失败！请重新登录"];
        }
    }];
}

- (void)getQQLoginService{
    
}

- (void)getThirdLoginLoginService:(NSString*)userId photoUrl:(NSString*)photoUrl nick:(NSString*)nickStr{
    [AppNetService getThirdLoginService:@{@"id":userId,@"photo":photoUrl,@"nick":nickStr} complete:^(BOOL isSuccess, LoginData*model) {
        if (isSuccess) {
            _loginDataModel = model;
            [UserDefaultsUtils saveValue:model.token forKey:@"TOKEN"];
            [UserDefaultsUtils saveBoolValue:YES withKey:@"IsLogin"];
            [[AppContextManager shareManager] setHeadData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.avatar]]];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [ToastUtil showToast:@"登录失败！请重新登录"];
        }
    }];
}

#pragma mark - Delegate

@end
