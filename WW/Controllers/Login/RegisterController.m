//
//  RegisterController.m
//  WW
//
//  Created by Macro on 15/6/10.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "RegisterController.h"
#import "UIButton+CountDown.h"
#import "LoginData.h"

static NSInteger kVerityValidTime = 60;

@interface RegisterController ()
{
    NSString *_token;
}
@property (strong, nonatomic) IBOutlet UITextField *phoneTF;
@property (strong, nonatomic) IBOutlet UITextField *verityTF;
@property (strong, nonatomic) IBOutlet UITextField *pwdTF;


@property (strong, nonatomic) IBOutlet UIButton *getVerityBtn;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (strong, nonatomic) IBOutlet UIButton *declearBtn;

@end

@implementation RegisterController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBaseProperties];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
    self.getVerityBtn.enabled = NO;
    //手机号输入框信号
    RACSignal *phoneSignal = [_phoneTF.rac_textSignal map:^id(id value) {
        return @(_phoneTF.text.length == 11);
    }];
    
    [phoneSignal subscribeNext:^(NSNumber* x) {
        self.getVerityBtn.enabled = x.boolValue;
    }];
    
    [[_getVerityBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [_getVerityBtn startTime:kVerityValidTime
                           title:@"获取验证码"
                      waitTittle:@"s剩余"];
        [self getVerigtyService];
    }];
    
     //验证码输入框信号
    RACSignal *veritySignal = [_verityTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length == 6);
    }];
    
    //密码输入框信号
    RACSignal *pwdSignal = [_pwdTF.rac_textSignal map:^id(NSString* value) {
        return @(value.length >=6 && value.length <= 20);
    }];
    
    @weakify(self)
    [[RACSignal combineLatest:@[phoneSignal,veritySignal,pwdSignal] reduce:^id(NSNumber*phoneValid,NSNumber*verityValid,NSNumber *pwdValid){
        return  @(phoneValid.boolValue&&verityValid.boolValue&&pwdValid.boolValue);
    }] subscribeNext:^(NSNumber* x) {
        @strongify(self)
        self.registerBtn.enabled = x.boolValue;
    }];
    
    [[_registerBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(NSNumber* x) {
        [self getRegisterService];
    }];
    
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setNavTitle:@"快捷注册"];
}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分
- (void)getVerigtyService{
//    @weakify(self)
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:_phoneTF forKey:@"phone"];
    [AppNetService getRegisterSendVerifyService:dic complete:^(BOOL isSuccess, NSString* msg) {
//        @strongify(self)
        [hub hide:YES];
        if (isSuccess) {
//            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [ToastUtil showToast:msg];
        }
    }];
}

- (void)getRegisterService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:_phoneTF.text forKey:@"phone"];
    [dic setObject:_verityTF.text forKey:@"verify"];
    [dic setObject:_pwdTF.text forKey:@"password"];
    @weakify(self)
    [AppNetService getRegisterRegiste:dic complete:^(BOOL isSuccess, LoginData * model) {
        @strongify(self)
        [hub hide:YES];
        if (isSuccess) {
            LoginData *tepModel = model;
            _token = tepModel.token;
            [self getLoginWithTokenService];
        }
    }];
}

- (void)getLoginWithTokenService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:_token forKey:@"token"];
    [AppNetService getLoginTokenLoginService:dic complete:^(BOOL isSuccess, LoginData*model) {
        [hub hide:YES];
        [UserDefaultsUtils saveValue:model.token forKey:@"TOKEN"];
    }];
}

#pragma mark - Delegate

@end
