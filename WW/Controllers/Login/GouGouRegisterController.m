//
//  GouGouRegisterController.m
//  WW
//
//  Created by Macro on 15/6/10.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "GouGouRegisterController.h"
#import "CustomerPickerView.h"
#import "PetListData.h"
#import "PetAreaListData.h"
#import "DatePickerView.h"
#import "UIImage+ResizeMagick.h"
#import "UserMessageData.h"

typedef NS_ENUM(NSInteger, RegisterGouGouMessageType) {
    GOUGOUClassify = 0,
    GOUGOUAge,
    GOGOUDistrict
};

@interface GouGouRegisterController ()<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    DatePickerView *_datePicker;
    CustomerPickerView *_picker;
  
    UIActionSheet *_actionSheet;
    
    NSData *_headImgData;
    
    RegisterGouGouMessageType _messageType;

    //scrollView原始可滑动高度
    CGFloat _origialHeight;
    
    //接口返回的或者初始化的宠物数据
    NSArray *_petTypeArray;
    NSArray *_petAgeArray;
    NSArray *_petDistrictArray;
    
    BOOL _isPetTypeReady;
    BOOL _isPetAgeReady;
    BOOL _isPetDistrictReady;
    
    NSString *_petTypeStr;
    NSString *_petSexStr;
    NSString *_petBirthday;
    NSString *_petDistrictStr;
    
    NSInteger _imageId;
    
    BOOL isNewPhoto;
    
    NSArray *petTypeModelArray;
    NSArray *petDistrictModelArray;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

//狗狗头像
@property (strong, nonatomic) IBOutlet UIImageView *gougouHeadImg;
@property (strong, nonatomic) IBOutlet UITextField *masterNameTF;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumTF;

@property (strong, nonatomic) IBOutlet UIView *gougouClassifyView;
@property (strong, nonatomic) IBOutlet UIImageView *gougouClassifyImg;
@property (strong, nonatomic) IBOutlet UILabel *gougouClassifyLbl;
@property (strong, nonatomic) IBOutlet UIImageView *gougouClassifyArrowImg;

@property (strong, nonatomic) IBOutlet UIView *gougouSexView;
@property (strong, nonatomic) IBOutlet UILabel *gougouSexLbl;
@property (strong, nonatomic) IBOutlet UIImageView *gougouSexArrowImg;

@property (strong, nonatomic) IBOutlet UIView *gougouAgeView;
@property (strong, nonatomic) IBOutlet UIImageView *gougouAgeImg;
@property (strong, nonatomic) IBOutlet UILabel *gougouAgeLbl;
@property (strong, nonatomic) IBOutlet UIImageView *gougouAgeArrowImg;

@property (strong, nonatomic) IBOutlet UIView *districtView;
@property (strong, nonatomic) IBOutlet UIImageView *districtImg;
@property (strong, nonatomic) IBOutlet UILabel *districeLbl;
@property (strong, nonatomic) IBOutlet UIImageView *districtArrowImg;

@property (strong, nonatomic) IBOutlet UIButton *confirmBtn;


@end

@implementation GouGouRegisterController

- (void)dealloc{
    [_datePicker removeFromSuperview];
    [_picker removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initBaseProperties];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    _origialHeight = _scrollView.contentSize.height;
    
    _isPetAgeReady = NO;
    _isPetTypeReady = NO;
    _isPetDistrictReady = NO;
    isNewPhoto = NO;
    
    [self getPetTypeListService];
    [self getPetAgeService];
    [self getPetDistrictService];
    [self getPetMessageService];
}


- (void)initControls{
    _picker = [[CustomerPickerView alloc] initWithFrame:CGRectMake(0, kViewControllerHeight - 268, kViewControllerWidth, 268)];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_picker];
    
    _datePicker = [[DatePickerView alloc] initWithFrame:CGRectMake(0, kViewControllerHeight - 268, kViewControllerWidth, 268)];
    [[UIApplication sharedApplication].keyWindow addSubview:_datePicker];
    
    UITapGestureRecognizer *headGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseHeadImage)];
    headGesture.numberOfTapsRequired = 1;
    [_gougouHeadImg addGestureRecognizer:headGesture];
    
    _gougouHeadImg.layer.cornerRadius = _gougouHeadImg.frame.size.width/2;
    _gougouHeadImg.layer.masksToBounds = YES;
    _gougouHeadImg.layer.borderWidth = 2;
    _gougouHeadImg.layer.borderColor = [UIColor colorWithHexString:@"F5F5F5"].CGColor;
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseFromPicker:)];
    gesture.numberOfTapsRequired = 1;
    [_gougouClassifyView addGestureRecognizer:gesture];
    
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseFromPicker:)];
    gesture.numberOfTapsRequired = 1;
    [_gougouAgeView addGestureRecognizer:gesture2];
    
    UITapGestureRecognizer *gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseFromPicker:)];
    gesture.numberOfTapsRequired = 1;
    [_districtView addGestureRecognizer:gesture3];
    
    UITapGestureRecognizer *gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseFromPicker:)];
    gesture4.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:gesture4];
    
    UITapGestureRecognizer *gesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseFromPicker:)];
    gesture5.numberOfTapsRequired = 1;
    [_gougouSexView addGestureRecognizer:gesture5];
    
    [RACObserve(_picker, hidden) subscribeNext:^(NSString* x) {
        if (x.boolValue) {
            CGSize size = _scrollView.contentSize;
            size.height = _origialHeight;
            _scrollView.contentSize = size;
        }else{
            CGSize size = _scrollView.contentSize;
            size.height += 266;
            _scrollView.contentSize = size;
        }
    }];
    
    [self setNavTitle:@"汪汪"];
    
    @weakify(self)
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [[_confirmBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self getUpdataPetMessageService];
    }];
}

#pragma mark - 自定义函数部分
- (void)openCamera{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera ]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        isNewPhoto = YES;
        [self presentViewController:picker animated:YES completion:^{
            
        }];
    }else{
        [ToastUtil showToast:@"相机不可使用!"];
        DLog(@"相机不可使用!");
    }
}

- (void)openAlbum{
    UIImagePickerControllerSourceType sourctType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourctType;
        isNewPhoto = NO;
        [self presentViewController:picker animated:YES completion:^{
            
        }];
    }else{
        [ToastUtil showToast:@"相册不可使用!"];
        DLog(@"相册不可使用!");
    }
}

- (void)resetPetMessage:(UserMessageData*)model{
    [_gougouHeadImg sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:nil];
    _masterNameTF.text = model.alias;
    _phoneNumTF.text = model.phone;
    _gougouClassifyLbl.text = model.petType;
    _gougouSexLbl.text  = ((model.gender == 1)?@"雄性":@"雌性");
    _gougouAgeLbl.text = model.petBirthday;
    _petBirthday = model.petBirthday;
    _districeLbl.text = model.petArea;
}

- (NSInteger)petTypeId{
    for (PetListPetTypeList *tempModel in petTypeModelArray) {
        if ([tempModel.name isEqualToString:_petTypeStr]) {
            return tempModel.petTypeId;
        }
    }
    return 1;
}

- (NSInteger)petDistrictId{
    for (PetAreaList *tempModel in petDistrictModelArray) {
        if ([tempModel.area isEqualToString:_petDistrictStr]) {
            return tempModel.petAreaId;
        }
    }
    return 1;
}


#pragma mark - 响应用户操作部分
- (void)chooseHeadImage{
    _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                               delegate:self
                                      cancelButtonTitle:@"取消"
                                 destructiveButtonTitle:@"相机"
                                      otherButtonTitles:@"相册", nil];
    [_actionSheet showInView:self.view];
}

- (void)chooseFromPicker:(UITapGestureRecognizer*)gesture{
    UIView *view = gesture.view;
    
    @weakify(self)
    if (view  == _gougouClassifyView) {
        if (!_isPetTypeReady) {
            return;
        }
        _picker.hidden = !_picker.hidden;
        if (_petTypeStr && ![_petTypeStr isEqualToString:@""]) {
            [_picker resetReturnOrInitDataArray:[NSMutableArray arrayWithObject:_petTypeStr]];
        }
        [_picker setDataArray:_petTypeArray cancelComeBack:^(id cancel) {
            
        } andComfirmComeBack:^(NSArray* confirmArray) {
           @strongify(self)
            if (confirmArray.count<=0) {
                return ;
            }
            _petTypeStr = confirmArray[0];
            self.gougouClassifyLbl.text = confirmArray[0];
        }];
        
    }else if (view == _gougouAgeView){
        if (!_isPetAgeReady) {
            return;
        }
        
        _datePicker.hidden = !_datePicker.hidden;
        if (_petBirthday && ![_petBirthday isEqualToString:@""]) {
            [_datePicker resetInitDate:_petBirthday];
        }
        [_datePicker responseToCancelBtnClick:^(id cancel) {
            
        } confirmBtnClick:^(NSString *str) {
            _petBirthday = str;
            self.gougouAgeLbl.text = _petBirthday;
        }];
        
//        if (_petAgeYearStr&&![_petAgeYearStr isEqualToString:@""]&&_petAgeMouthStr&&![_petAgeMouthStr isEqualToString:@""]) {
//            [_picker resetReturnOrInitDataArray: [NSMutableArray arrayWithObjects:_petAgeYearStr,_petAgeMouthStr, nil]];
//        }
//        [_picker setDataArray:_petAgeArray cancelComeBack:^(id cancel) {
//            
//        } andComfirmComeBack:^(NSArray* confirmArray) {
//            @strongify(self)
//            if (confirmArray.count<=1) {
//                return ;
//            }
//            _petAgeYearStr = confirmArray[0];
//            _petAgeMouthStr = confirmArray[1];
//            self.gougouAgeLbl.text = [NSString stringWithFormat:@"%@%@",confirmArray[0],confirmArray[1]];
//        }];
        
    }else if(view == _districtView){
        if (!_isPetDistrictReady) {
            return;
        }
        
        _picker.hidden = !_picker.hidden;
        if (_petDistrictStr && ![_petDistrictStr isEqualToString:@""]) {
            [_picker resetReturnOrInitDataArray:[NSMutableArray arrayWithObject:_petDistrictStr]];
        }
        [_picker setDataArray:_petDistrictArray cancelComeBack:^(id cancel) {
            
        } andComfirmComeBack:^(NSArray* confirmArray) {
            @strongify(self)
            if (confirmArray.count <= 0) {
                return ;
            }
            _petDistrictStr = confirmArray[0];
            self.districeLbl.text = confirmArray[0];
        }];
        
    }else if (view == _gougouSexView){
        _picker.hidden = !_picker.hidden;
        if (_petSexStr && ![_petSexStr isEqualToString:@""]) {
            [_picker resetReturnOrInitDataArray:[@[_petSexStr] mutableCopy]];
        }
        [_picker setDataArray:@[@[@"雄性",@"雌性"]] cancelComeBack:^(id cancel) {
        
        } andComfirmComeBack:^(NSArray* confirmArray) {
            @strongify(self)
            if (confirmArray.count <= 0) {
                return ;
            }
            _petSexStr = confirmArray[0];
            self.gougouSexLbl.text = confirmArray[0];
        }];
    }else{
        if (_isPetDistrictReady&&_isPetTypeReady&&_isPetAgeReady) {
            _picker.hidden = YES;
        }
    }
}

#pragma mark - 网络请求部分
- (void)getPetTypeListService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    [AppNetService getPetTypeService:nil complete:^(BOOL isSuccess, PetListData *model) {
        [hub hide:YES];
        if (isSuccess) {
            petTypeModelArray = model.petTypeList;
            NSMutableArray *tepArray = [NSMutableArray array];
            for (PetListPetTypeList *tepModel in model.petTypeList) {
                [tepArray addObject:tepModel.name];
            }
            _petTypeArray = @[(NSArray*)tepArray];
            _isPetTypeReady = YES;
        }
    }];
}

- (void)getPetAgeService{
    
    NSMutableArray *tempYearArray = [NSMutableArray array];
    NSMutableArray *tempMouthArray = [NSMutableArray array];
    for (int i = 0;i< 10 ;i++) {
        [tempYearArray addObject:[NSString stringWithFormat:@"%d年",i]];
    }
    
    for (int i = 1; i<12; i++) {
        [tempMouthArray addObject:[NSString stringWithFormat:@"%d月",i]];
    }
    
    _petAgeArray = @[(NSArray*)tempYearArray,(NSArray*)tempMouthArray];
    _isPetAgeReady = YES;
}

- (void)getPetDistrictService{
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    [AppNetService getPetDistrictsService:nil complete:^(BOOL isSuccess, PetAreaListData *model) {
        [hub hide:YES];
        if (isSuccess) {
            petDistrictModelArray = model.petAreaList;
            NSMutableArray *tepArray = [NSMutableArray array];
            for (PetAreaList* tepArea in model.petAreaList) {
                [tepArray addObject:tepArea.area];
            }
            _isPetDistrictReady = YES;
            _petDistrictArray = @[(NSArray*)tepArray];
        }
    }];
}

- (void)getPetHeadImageUploadService{
    NSMutableURLRequest *retuest = [[AFHTTPRequestSerializer serializer]
                                    multipartFormRequestWithMethod:@"POST"
                                    URLString:kSOAP_URL_USER_UPLOADPICTURE
                                    parameters:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"]}
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        [formData appendPartWithFileData:_headImgData name:@"photo" fileName:@"photo.png" mimeType:@"image/png"];
                                    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSProgress *progress = nil;
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:retuest progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            DLog(@"Error:%@",error);
        }else{
            kDebugNSlog
            DLog(@"%@ %@",response,responseObject);
            if (responseObject) {
                if ([responseObject[@"error_code"] integerValue] == 1) {
                    _imageId = [responseObject[@"data"][@"img_id"] integerValue];
                }
            }
        }
    }];
    [uploadTask resume];
}

- (void)getUpdataPetMessageService{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[UserDefaultsUtils valueWithKey:@"TOKEN"] forKey:@"token"];
    [dic setObject:@(_imageId) forKey:@"img_id"];
    [dic setObject:_masterNameTF.text forKey:@"alias"];
    [dic setObject:_phoneNumTF.text forKey:@"phone"];
    [dic setObject:([_petSexStr isEqualToString:@"雄性"]?@(1):@(2)) forKey:@"gender"];
    [dic setObject:@([self petTypeId]) forKey:@"pet_type_id"];
    [dic setObject:_petBirthday forKey:@"pet_birthday"];
    [dic setObject:@([self petDistrictId]) forKey:@"pet_area_id"];
    
    [AppNetService getUserMessageChangeService:dic complete:^(BOOL isSuccess, UserMessageData *model) {
        if (isSuccess) {
            [ToastUtil showToast:@"修改成功"];
        }
    }];
}

- (void)getPetMessageService{
    @weakify(self)
    [AppNetService getUserMessageService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"]} complete:^(BOOL isSuccess, UserMessageData *model) {
        @strongify(self)
        if (isSuccess) {
            [self resetPetMessage:model];
        }
    }];
}

#pragma mark - Delegate
#pragma mark - ActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet == _actionSheet) {
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            [actionSheet dismissWithClickedButtonIndex:actionSheet.cancelButtonIndex animated:YES];
        }else if (buttonIndex == actionSheet.destructiveButtonIndex){
            [self openCamera];
        }else if (buttonIndex == actionSheet.firstOtherButtonIndex){
            [self openAlbum];
        }
    }
}

#pragma mark - ImagePickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];

    if ([type isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSData *data;
        self.gougouHeadImg.image = image;
        if (UIImagePNGRepresentation(image)) {
            data = UIImagePNGRepresentation(image);
        }else{
            data = UIImageJPEGRepresentation(image, 1.0);
        }
        
        _headImgData = [image resizedAndReturnData];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self getPetHeadImageUploadService];
        });
        
        if (isNewPhoto) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if(error){
        [ToastUtil showToast:@"将图片添加相册失败!"];
    }
}
@end
