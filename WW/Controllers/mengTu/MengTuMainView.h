//
//  MengTuMainView.h
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuHrizontal.h"
#import "ScrollPageView.h"

@interface MengTuMainView : UIView<MenuHrizontalDelegate,ScrollPageViewDelegate>
{
    MenuHrizontal *mMenuHriZontal;
    ScrollPageView *mScrollPageView;
}

@property (nonatomic,strong) MenuHrizontal *mMenuHriZontal;
@property (nonatomic,strong) ScrollPageView *mScrollPageView;


@end
