//
//  MengTuController.m
//  WW
//
//  Created by Macro on 15/6/5.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "MengTuDetailController.h"
#import "MengTuCommentTableCell.h"
#import "MengTuShareTableCell.h"
#import "MaskView.h"
#import "WriteArticleController.h"
#import "PictureDetailData.h"
#import "CommentListData.h"
#import "ShareListData.h"
#import "UIView+Toast.h"

@interface MengTuDetailController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    BOOL _isComment; //是否是评论页面
    
    PictureDetailData *_detailModel;
    CommentListData *_commentListModel;
    ShareListData *_shareListModel;
    
    BOOL _isOriginalAttentionStatu;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *headImg;  //头像
@property (strong, nonatomic) IBOutlet UIImageView *sexImg;   //性别
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;      //姓名
@property (strong, nonatomic) IBOutlet UILabel *infoLbl;     //宠物详情
@property (strong, nonatomic) IBOutlet UIButton *payAttentionBtn;  //关注Ta按钮
@property (strong, nonatomic) IBOutlet UIImageView *dogImg;     //狗狗相片
@property (strong, nonatomic) IBOutlet UILabel *dogInfoLbl;     //图片描述
@property (strong, nonatomic) IBOutlet UIButton *sharebtn;
@property (strong, nonatomic) IBOutlet UILabel *timeReplyLbl;

@property (strong, nonatomic) IBOutlet UIButton *wishBtn;
@property (strong, nonatomic) IBOutlet UIButton *replyBtn;
@property (strong, nonatomic) IBOutlet UIButton *looksBtn;

@property (strong, nonatomic) IBOutlet UIView *lookedView;  //查看过了的头像View
@property (strong, nonatomic) IBOutlet UIButton *lookAllBtn;    //查看所有按钮

@property (strong, nonatomic) IBOutlet UIImageView *commentAndShareBgImg;
@property (strong, nonatomic) IBOutlet UIButton *commentBtn;       //评论按钮
@property (strong, nonatomic) IBOutlet UIButton *shareBtn;          //分享按钮

@property (strong, nonatomic) IBOutlet UIView *commentView;
@property (strong, nonatomic) IBOutlet UIButton *commentHeadBtn;
@property (strong, nonatomic) IBOutlet UITextField *commentTF;
@property (strong, nonatomic) IBOutlet UITableView *commentedTableView;

@property (strong, nonatomic) IBOutlet UIView *shareView;
@property (strong, nonatomic) IBOutlet UITableView *shareTableView;

@end

@implementation MengTuDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBaseProperties];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    _isComment = YES;
    [self getArticleDetailService];
    
    [self getCommentListService:_articleId complete:^{
        
    }];
    
    [self getShareListService:_articleId complete:^{
        
    }];
}


- (void)initControls{
    @weakify(self)
    //点击了评论按钮
    [[self.commentBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        _isComment = YES;
        [self swithSegToShow];
    }];
    
    //点击了分享按钮
    [[self.shareBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        _isComment = NO;
        [self swithSegToShow];
    }];
    
    //给评论页面加下拉刷新和上拉请求更多
    [_commentedTableView addLegendHeaderWithRefreshingBlock:^{
        @strongify(self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.commentedTableView reloadData];
            [self.commentedTableView.header endRefreshing];
        });
    }];
    [_commentedTableView addLegendFooterWithRefreshingBlock:^{
        @strongify(self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [self.commentedTableView endUpdates];
              [self.commentedTableView.footer endRefreshing];
        });
    }];
    
    //给分享页面加下拉刷新和上拉请求更多
    [_shareTableView addLegendHeaderWithRefreshingBlock:^{
        @strongify(self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.shareTableView reloadData];
            [self.shareTableView.header endRefreshing];
        });
    }];
    [_shareTableView addLegendFooterWithRefreshingBlock:^{
        @strongify(self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.shareTableView endUpdates];
            [self.shareTableView.footer endRefreshing];
        });
    }];
    
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setNavTitle:@"萌图"];
    
    [self setNavRightImage:[UIImage imageNamed:@"Camera"] complete:^{
        MaskView *view = [[MaskView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, kWindowHight)];
        [[[UIApplication sharedApplication] keyWindow] addSubview:view];
        [[view.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            [view removeFromSuperview];
        }];
        
        [view responseToBtnClickBlock:^(int tag) {
            [view removeFromSuperview];
            @strongify(self)
            [self performSegueWithIdentifier:@"MengTuDetailToWriteArticle" sender:@(tag)];
        }];
    }];

    self.headImg.layer.cornerRadius = 5;
    self.headImg.layer.masksToBounds = YES;
    
    self.commentedTableView.tableFooterView = [[UIView alloc] init];
    self.shareTableView.tableFooterView = [[UIView alloc] init];
    
    self.commentHeadBtn.layer.cornerRadius = 5;
    self.commentHeadBtn.layer.masksToBounds = YES;
    self.commentTF.delegate = self;
    
    [self swithSegToShow];
}

#pragma mark - 自定义函数部分

- (void)swithSegToShow{
    if (_isComment) {
        _commentView.hidden = NO;
        _shareView.hidden = YES;
    }else{
        _commentView.hidden = YES;
        _shareView.hidden = NO;
    }
    
    self.commentAndShareBgImg.image = _isComment?[UIImage imageNamed:@"btn_switch_first"]:[UIImage imageNamed:@"btn_switch_second"];
    
    if (_isComment) {
        [self getCommentListService:_articleId complete:^{
            [self.commentedTableView reloadData];
        }];
    }else{
        [self getShareListService:_articleId complete:^{
            [self.shareTableView reloadData];
        }];
    }
}

- (void)resetViewData:(PictureDetailData*)model{
    @weakify(self)
    
    _isOriginalAttentionStatu = model.isFollowed;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:nil];
    self.sexImg.image = (model.gender==1)?[UIImage imageNamed:@"flag_m"]:[UIImage imageNamed:@"flag_f"];
    self.nameLbl.text = model.alias;
    self.infoLbl.text = [NSString stringWithFormat:@"%@|%@|%@",model.petType,model.petAge,model.petArea];
    
    //关注按钮
    [self.payAttentionBtn setBackgroundImage:(_isOriginalAttentionStatu?[UIImage imageNamed:@"button_attention"]:[UIImage imageNamed:@"button_cancelAttention"]) forState:UIControlStateNormal];
    [[self.payAttentionBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self getAttentionService:model.userId complete:^{
            [ToastUtil showToast:(!_isOriginalAttentionStatu?@"取消关注成功!":@"关注成功!")];
            [self.payAttentionBtn setBackgroundImage:(!_isOriginalAttentionStatu?[UIImage imageNamed:@"button_cancelAttention"]:[UIImage imageNamed:@"button_attention"]) forState:UIControlStateNormal];
        }];
    }];
    
    
    [self.dogImg sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:nil];
    self.dogInfoLbl.text = model.content;
    self.timeReplyLbl.text = model.dateDes;
    
    [self.wishBtn setTitle:[NSString stringWithFormat:@"%ld",(long)model.praiseNumber] forState:UIControlStateNormal];
    [[self.wishBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self getZanService:_articleId complete:^{
           [self.wishBtn setTitle:[NSString stringWithFormat:@"%ld",(long)model.praiseNumber+1] forState:UIControlStateNormal];
        }];
    }];
    
    //评论接口
    [self.replyBtn setTitle:[NSString stringWithFormat:@"%ld",(long)model.commentNumber] forState:UIControlStateNormal];
    [[self.replyBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self.commentTF becomeFirstResponder];
    }];
    
    [self.looksBtn setTitle:[NSString stringWithFormat:@"%ld",(long)model.viewNumber] forState:UIControlStateNormal];
    [[self.looksBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
    }];
    
    [self.commentHeadBtn setBackgroundImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.avatar]]] forState:UIControlStateNormal];
    
}

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分
- (void)getArticleDetailService{
    @weakify(self)
    [AppNetService getPictureDetailService:@{@"article_id":@(self.articleId),@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"]} complete:^(BOOL isSuccess, PictureDetailData *model) {
        @strongify(self)
        if (isSuccess) {
            _detailModel = model;
            [self resetViewData:model];
        }
    }];
}

- (void)getAttentionService:(NSInteger)userId complete:(void(^)(void))comBack{
    [AppNetService getUserPayOrCancelAttentionService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"user_id":@(userId)} complete:^(BOOL isSuccess , NSString *errorMsg) {
        if (isSuccess) {
            _isOriginalAttentionStatu = !_isOriginalAttentionStatu;
            comBack();
        }
    }];
}

- (void)getCommentListService:(NSInteger)articleId complete:(void(^)(void))comBack{
    @weakify(self)
    [AppNetService getCommentListService:@{@"article_id":@(articleId)} complete:^(BOOL isSuccess, CommentListData *model) {
        @strongify(self)
        if (isSuccess) {
            _commentListModel = model;
            [self.commentBtn setTitle:[NSString stringWithFormat:@"评论%ld",model.commentList.count] forState:UIControlStateNormal];
            comBack();
        }
    }];
}

- (void)getShareListService:(NSInteger)articleId complete:(void(^)(void))comBack{
    @weakify(self)
    [AppNetService getShareListService:@{@"article_id":@(articleId)} complete:^(BOOL isSuccess, ShareListData *model) {
        @strongify(self)
        if (isSuccess) {
            _shareListModel = model;
            [self.shareBtn setTitle:[NSString stringWithFormat:@"分享%ld",model.shareList.count] forState:UIControlStateNormal];
            comBack();
        }
    }];
}

- (void)getAddCommentService:(NSInteger)articleId comment:(NSString*)comment complete:(void(^)(void))comeBack{
    [AppNetService getCommentAddService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"articke_id":@(articleId),@"comment":comment} complete:^(BOOL isSuccess, NSString *errorMsg) {
        if (isSuccess) {
            comeBack();
        }
    }];
}

- (void)getZanService:(NSInteger)articleId complete:(void(^)(void))comeBack{
    @weakify(self)
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self.view];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self.view andHUD:hub];
    [AppNetService getPraiseAddService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"article_id":@(articleId)} complete:^(BOOL isSuccess, NSString*errorMsg) {
        @strongify(self)
        [hub hide:YES];
        if (isSuccess) {
            UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zan"]];
            [self.view showToast:view duration:0.5 position:@"center"];
            comeBack();
        }else{
            UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zan"]];
            [self.view showToast:view duration:0.5 position:@"center"];
        }
    }];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _commentedTableView) {
        return _commentListModel.commentList.count;
    }else{
        return _shareListModel.shareList.count;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _commentedTableView) {
        MengTuCommentTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MengTuCommentTableCell"];
        CommentList *model = _commentListModel.commentList[indexPath.row];
        [cell.headBtn setBackgroundImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.avatar]]] forState:UIControlStateNormal];
        cell.headBtn.layer.cornerRadius = 5;
        cell.headBtn.layer.masksToBounds = YES;
        cell.nameLbl.text = model.alias;
        cell.datelbl.text = model.addDate;
        cell.infoLbl.text = model.content;
        return cell;
    }else{
        MengTuShareTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MengTuShareTableCell"];
        ShareList *model = _shareListModel.shareList[indexPath.row];
        [cell.headBtn setBackgroundImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.avatar]]] forState:UIControlStateNormal];
        cell.headBtn.layer.cornerRadius = 5;
        cell.headBtn.layer.masksToBounds = YES;
        cell.namelbl.text = model.alias;
        cell.dateLbl.text = model.addDate;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55*kViewControllerRate;
}

#pragma mark - push Delegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"MengTuDetailToWriteArticle"]) {
        WriteArticleController *ctrl = (WriteArticleController*)segue.destinationViewController;
        ctrl.photoSourceType = [(NSNumber*)sender integerValue];
    }
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField.text isEqualToString:@""]) {
        return NO;
    }
    [self getAddCommentService:_articleId comment:textField.text complete:^{
        
    }];
    return YES;
}

@end
