//
//  MengTuMainView.m
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "MengTuMainView.h"
#define MENUHEIHT 40

@implementation MengTuMainView
@synthesize mMenuHriZontal;
@synthesize mScrollPageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self commInit];
    }
    return self;
}

-(void)commInit{
    NSArray *vButtonItemArray = @[@{kNormalKey: @"",
                                    kHighlightKey:@"tab_line",
                                    kTitleKey:@"精选",
                                    kTitleWidthKey:[NSNumber numberWithFloat:70]
                                    },
                                  @{kNormalKey: @"",
                                    kHighlightKey:@"tab_line",
                                    kTitleKey:@"关注",
                                    kTitleWidthKey:[NSNumber numberWithFloat:70]
                                    },
                                  @{kNormalKey: @"",
                                    kHighlightKey:@"tab_line",
                                    kTitleKey:@"最新",
                                    kTitleWidthKey:[NSNumber numberWithFloat:70]
                                    }];
    
    if (mMenuHriZontal == nil) {
        mMenuHriZontal = [[MenuHrizontal alloc] initWithFrame:CGRectMake(kWindowWidth/2-105, 0, 70*3, MENUHEIHT) buttonItems:vButtonItemArray];
        mMenuHriZontal.delegate = self;
    }
    //初始化滑动列表
    if (mScrollPageView == nil) {
        mScrollPageView = [[ScrollPageView alloc] initWithFrame:CGRectMake(0, MENUHEIHT, self.frame.size.width, self.frame.size.height - (MENUHEIHT + 48))];
        mScrollPageView.delegate = self;
    }
    [mScrollPageView setContentOfTables:vButtonItemArray.count];
    //默认选中第一个button
    [mMenuHriZontal clickButtonAtIndex:0];
    //-------
    [self addSubview:mScrollPageView];
    [self addSubview:mMenuHriZontal];
}

#pragma mark 内存相关
-(void)dealloc{
    mMenuHriZontal = nil;
    mScrollPageView = nil;
}

#pragma mark - 其他辅助功能
#pragma mark MenuHrizontalDelegate
-(void)didMenuHrizontalClickedButtonAtIndex:(NSInteger)aIndex{
    NSLog(@"第%ld个Button点击了",(long)aIndex);
    [mScrollPageView moveScrollowViewAthIndex:aIndex];
}

#pragma mark ScrollPageViewDelegate
-(void)didScrollPageViewChangedPage:(NSInteger)aPage{
    NSLog(@"CurrentPage:%ld",(long)aPage);
    [mMenuHriZontal changeButtonStateAtIndex:aPage];
    //    if (aPage == 3) {
    //刷新当页数据
    [mScrollPageView freshContentTableAtIndex:aPage];
    //    }
}


@end
