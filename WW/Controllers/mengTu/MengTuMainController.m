//
//  MengTuMainController.m
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "MengTuMainController.h"
#import "MengTuMainView.h"
#import "LoginController.h"
#import "MaskView.h"
#import "WriteArticleController.h"
#import "MengTuDetailController.h"

@interface MengTuMainController ()<UIAlertViewDelegate>
{
    MengTuMainView *mengTuMainView;
}
@end

@implementation MengTuMainController

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
    @weakify(self)
    mengTuMainView = (MengTuMainView*)self.view;
    [mengTuMainView.mScrollPageView didSelectedTableRowComplete:^(NSInteger index) {
        @strongify(self)
        [self performSegueWithIdentifier:@"MengTuMainToDetail" sender:@(index)];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responsseToNotLogin) name:@"pushToLoginCtrl" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseToWriteArticleBtn:) name:@"clickMaskViewWriteArticleBtn" object:nil];
    
    [self setNavTitle:@"萌图"];
    
//    [self setNavLeftImageWithImageData:[[AppContextManager shareManager] headData] complete:^{
////        [TabbarUtil changeViewControllerWithTabbarIndex:4];
//    }];
    
    [RACObserve([AppContextManager shareManager], headData) subscribeNext:^(id x) {
        @strongify(self)
        [self setNavLeftImageWithImageData:[[AppContextManager shareManager] headData] complete:^{
            [TabbarUtil changeViewControllerWithTabbarIndex:3];
        }];
    }];
    

    [self setNavRightImage:[UIImage imageNamed:@"Camera"] complete:^{
        MaskView *view = [[MaskView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, kWindowHight)];
        [[[UIApplication sharedApplication] keyWindow] addSubview:view];
        [[view.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            [view removeFromSuperview];
        }];
        
        [view responseToBtnClickBlock:^(int tag) {
            [view removeFromSuperview];
            @strongify(self)
            [self performSegueWithIdentifier:@"MengTuMainToWriteArticle" sender:@(tag)];
        }];
    }];
}

#pragma mark - 自定义函数部分
- (void)responsseToNotLogin{

    [[[UIAlertView alloc] initWithTitle:@"提示" message:@"您未登录，请先登录再查看此部分相关的内容！"  delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil] show];
}

- (void)responseToWriteArticleBtn:(NSNotification*)notification{
     [self performSegueWithIdentifier:@"MengTuMainToWriteArticle" sender:(NSNumber*)notification.object];
}
#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分

#pragma mark - Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginController *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if(buttonIndex == alertView.cancelButtonIndex){
        [mengTuMainView.mScrollPageView moveScrollowViewAthIndex:0];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"MengTuMainToWriteArticle"]) {
        WriteArticleController *ctrl = (WriteArticleController*)segue.destinationViewController;
        ctrl.photoSourceType = [(NSNumber*)sender integerValue];
    }else if ([segue.identifier isEqualToString:@"MengTuMainToDetail"]){
        MengTuDetailController *detailCtrl = (MengTuDetailController *)segue.destinationViewController;
        detailCtrl.articleId = [(NSNumber*)sender integerValue];
    }
}

@end
