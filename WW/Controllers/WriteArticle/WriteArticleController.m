//
//  WriteArticleController.m
//  WW
//
//  Created by Macro on 15/7/1.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "WriteArticleController.h"
#import <IQKeyboardManager.h>
#import <IQUIView+IQKeyboardToolbar.h>
#import "InputHelper.h"
#import <objc/runtime.h>

//static NSString *kInputHelperRootViewOriginalOriginY = @"kInputHelperRootViewOriginalOriginY";
//static NSString *kInputHelperRootViewOriginalOriginH = @"kInputHelperRootViewOriginalOriginH";

//static void setAssociatedObjectForKey(UIView *view ,NSString *key,id value){
//    if (view) {
//        objc_setAssociatedObject(view, (__bridge  const void *)(key), value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    }
//}
//
//static id getAssociatedObjectForKey(UIView *view ,NSString *key) {
//    return  view ? objc_getAssociatedObject(view, (__bridge  const void *)(key)) : nil;
//}

@interface WriteArticleController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    BOOL isNewPhoto;
    UIActionSheet *_actionSheet;
    UIView *_topShareBtnsView;
    CGFloat _keyboardOrginY;
    BOOL isAllowUp;
    UIView *_currentRootView;
    BOOL _isUploadSuccess;
//    NSData *_picData;
}

@property (strong,nonatomic) NSData *picData;
@property (strong, nonatomic) IBOutlet UIImageView *picImgView;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@end

@implementation WriteArticleController
@synthesize picData = _picData;

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initControls];
    [self initBaseProperties];
    [self initDatasource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabbarUtil isTabbarHide:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TabbarUtil isTabbarHide:NO];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    isNewPhoto = NO;
    _keyboardOrginY = 0;
    isAllowUp = YES;
    _currentRootView = self.view;
}

- (void)initControls{
    @weakify(self)
    [self setNavBackWithComeplete:^{
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self setNavTitle:@"汪汪"];
    
    [self setNavRightTitle:@"发布" complete:^{
        @strongify(self)
        DLog(@"发布!",nil);
        
        [self uploadPic:self.picData];
    }];
    
    if (_photoSourceType == 1) {
        [self openCamera];
    }else{
        [self OpenAlbum];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(responseTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [_picImgView addGestureRecognizer:tapGesture];
    
    _topShareBtnsView = [[UIView alloc] initWithFrame:CGRectMake(0, kWindowHight - 104, kWindowWidth, 40)];
    _topShareBtnsView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_topShareBtnsView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, 1/[UIScreen mainScreen].scale)];
    view.backgroundColor = [UIColor colorWithHexString:@"7A7A7A"];
    [_topShareBtnsView addSubview:view];
    
    UILabel *textLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 1/[UIScreen mainScreen].scale, 65, 40)];
    textLbl.text = @"内容同步到:";
    textLbl.font = [UIFont systemFontOfSize:12];
    textLbl.textColor = [UIColor colorWithHexString:@"7A7A7A"];
    [_topShareBtnsView addSubview:textLbl];
    
    UIButton *weChatButton = [[UIButton alloc] initWithFrame:CGRectMake(65, 3, 110, 34)];
    [weChatButton setImage:[UIImage imageNamed:@"wx"] forState:UIControlStateNormal];
    weChatButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [weChatButton setTitleColor:[UIColor colorWithHexString:@"7A7A7A"] forState:UIControlStateNormal];
    [weChatButton setTitle:@"微信朋友圈" forState:UIControlStateNormal];
    [_topShareBtnsView addSubview:weChatButton];
    
    UIButton *sinaButton = [[UIButton alloc] initWithFrame:CGRectMake(175, 3, 110, 34)];
    [sinaButton setImage:[UIImage imageNamed:@"xl"] forState:UIControlStateNormal];
    sinaButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [sinaButton setTitleColor:[UIColor colorWithHexString:@"7A7A7A"] forState:UIControlStateNormal];
    [sinaButton setTitle:@"新浪微博" forState:UIControlStateNormal];
    [_topShareBtnsView addSubview:sinaButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)initDatasource{
    
}

#pragma mark - 自定义函数部分
- (void)openCamera{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:^{
            
        }];
        isNewPhoto = YES;
    }else{
        [ToastUtil showToast:@"相机不可使用!"];
        DLog(@"相机不可使用!");
    }
}

- (void)OpenAlbum{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
         UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:^{
            
        }];
    }else{
        [ToastUtil showToast:@"相册打开失败!"];
        DLog(@"相册打开失败!");
    }
}

- (void)firstWarming{
    if (![UserDefaultsUtils boolValueWithKey:@"IsFirstPutImage"]) {
        [UserDefaultsUtils saveBoolValue:YES withKey:@"IsFirstPutImage"];
        [[[UIAlertView alloc] initWithTitle:@"提示" message:@"点击图片可以再次选择替换" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
    }
}

- (void)responseTapGesture:(UITapGestureRecognizer*)gesture{
    [_textField resignFirstResponder];
    if ([gesture.view isKindOfClass:[UIImageView class]]) {
        _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                     destructiveButtonTitle:@"发帖"
                                          otherButtonTitles:@"相册", nil];
        [_actionSheet showInView:self.view];
    }
}

- (void)keyboardWillShow:(NSNotification*)notification{
    kDebugNSlog
    if (!isAllowUp) {
        return;
    }
    isAllowUp = NO;
    
//    CGFloat originY = [getAssociatedObjectForKey(_currentRootView, kInputHelperRootViewOriginalOriginY) floatValue];
//    
//    if (getAssociatedObjectForKey(_currentRootView, kInputHelperRootViewOriginalOriginY) == nil) {
//        setAssociatedObjectForKey(_currentRootView, kInputHelperRootViewOriginalOriginY, @(_currentRootView.frame.origin.y));
//    }
    
    CGSize keyboardSize = [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    if (keyboardSize.height == 216) {
//        keyboardSize.height = 288.5;
//    }
    CGRect rect =  _topShareBtnsView.frame;
    _keyboardOrginY = kWindowHight - 64 - 40;
    _keyboardOrginY = _keyboardOrginY - keyboardSize.height;
    DLog(@"_keyboardOrginY = %f",_keyboardOrginY);
    rect.origin.y = _keyboardOrginY;
    _topShareBtnsView.frame = rect;
}

- (void)keyboardWillHide:(NSNotification*)notification{
    kDebugNSlog
    isAllowUp = YES;
    CGSize keyboardSize = [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect rect =  _topShareBtnsView.frame;
    _keyboardOrginY = _keyboardOrginY + keyboardSize.height;
    rect.origin.y = _keyboardOrginY;
     DLog(@"_keyboardOrginY = %f",_keyboardOrginY);
    _topShareBtnsView.frame = rect;
}

#pragma mark - 响应用户操作部分
- (IBAction)tapGesture:(UITapGestureRecognizer *)sender {
    if ([_textField isFirstResponder]) {
        [_textField resignFirstResponder];
    }else{
        [self.textField becomeFirstResponder];
    }
}

#pragma mark - 网络请求部分
- (void)uploadPic:(NSData*)data{
    [AppNetService getUserUploadPictureService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"photo":data} complete:^(BOOL isSuccess, NSString *errorMsg) {
        if (isSuccess) {
            _isUploadSuccess = YES;
        }
    }];
}

#pragma mark - Delegate
#pragma mark - ImagePickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([type isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSData *data;
        self.picImgView.image = image;
        if (UIImagePNGRepresentation(image)) {
            data = UIImagePNGRepresentation(image);
        }else{
            data = UIImageJPEGRepresentation(image, 1.0);
        }
        
        _picData = data;
        NSString *documentPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"PicDocument"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        [fileManager createDirectoryAtPath:documentPath withIntermediateDirectories:YES attributes:nil error:nil];
        [fileManager createFileAtPath:[documentPath stringByAppendingPathComponent:@"image.png"] contents:data attributes:nil];
        
        if (isNewPhoto) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self firstWarming];
    }];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if(error){
        [ToastUtil showToast:@"将图片添加相册失败!"];
    }
}

#pragma mark - ActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet == _actionSheet) {
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            [actionSheet dismissWithClickedButtonIndex:actionSheet.cancelButtonIndex animated:YES];
        }else if(buttonIndex == actionSheet.destructiveButtonIndex){
            [self openCamera];
        }else if (buttonIndex == actionSheet.firstOtherButtonIndex){
            [self OpenAlbum];
        }
    }
}

@end
