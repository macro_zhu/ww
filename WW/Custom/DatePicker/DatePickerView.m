//
//  DataPickerView.m
//  WW
//
//  Created by Macro on 15/7/6.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "DatePickerView.h"

@interface DatePickerView (){
    NSString *_dateStr;
}

@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *confirmBtn;

@property (nonatomic,copy) DatePickerTopBtnComeBackBlock cancelBlock;
@property (nonatomic,copy) DatePickerTopBtnComeBackBlock confirmBlock;

@end

@implementation DatePickerView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"DatePickerView" owner:self options:nil] objectAtIndex:0];
        self.frame = frame;
        [self initControls];
        self.hidden = YES;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        _selectDate = [dateFormatter stringFromDate:[NSDate date]];
    }
    return self;
}

- (void)initControls{
    //取消
    [[_cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        self.hidden = YES;
        _cancelBlock(nil);
    }];
    
    //确定
    [[_confirmBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        self.hidden = YES;
        _confirmBlock(_selectDate);
    }];
    
    [self.dataPicker setDate:[NSDate date]];
    self.dataPicker.maximumDate = [NSDate date];
}

- (void)initDatePicker{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:_dateStr];
    [_dataPicker setDate:date];
}

- (void)resetInitDate:(NSString *)dateStr{
    if (!dateStr || [dateStr isEqualToString:@""]) {
        return;
    }
    
    _dateStr = dateStr;
    [self initDatePicker];
}

- (void)responseToCancelBtnClick:(DatePickerTopBtnComeBackBlock)block confirmBtnClick:(DatePickerTopBtnComeBackBlock)block2{
    _cancelBlock = [block copy];
    _confirmBlock = [block2 copy];
}

- (IBAction)datePick:(UIDatePicker *)sender {
    NSDate *seleteDate = [sender date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateAndTime = [formatter stringFromDate:seleteDate];
    _selectDate = [dateAndTime copy];
}

@end
