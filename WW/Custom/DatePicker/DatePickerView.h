//
//  DataPickerView.h
//  WW
//
//  Created by Macro on 15/7/6.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DatePickerTopBtnComeBackBlock)(id);

@interface DatePickerView : UIView

@property (strong, nonatomic) IBOutlet UIDatePicker *dataPicker;

@property (nonatomic,copy) NSString *selectDate;

- (void)resetInitDate:(NSString *)dateStr;

- (void)responseToCancelBtnClick:(DatePickerTopBtnComeBackBlock)block confirmBtnClick:(DatePickerTopBtnComeBackBlock)block2;

@end
