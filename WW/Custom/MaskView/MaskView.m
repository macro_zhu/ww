//
//  MaskView.m
//  WW
//
//  Created by Macro on 15/6/30.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "MaskView.h"

@implementation MaskView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"MaskView" owner:self options:nil] firstObject];
        self.frame = frame;
    }
    return self;
}

- (IBAction)clickWriteArticleBtn:(UIButton *)sender {
    _block(1);
}

- (IBAction)clickPhotoBtn:(UIButton *)sender {
    _block(2);
}

- (void)responseToBtnClickBlock:(void(^)(int))comeBackBlock{
    _block = [comeBackBlock copy];
}

@end
