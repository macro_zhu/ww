//
//  MaskView.h
//  WW
//
//  Created by Macro on 15/6/30.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MaskView : UIView
@property (strong, nonatomic) IBOutlet UIButton *writeArticleBtn;
@property (strong, nonatomic) IBOutlet UIButton *photoBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (nonatomic,copy) ResponseToUserClick_Btn_Tag block;

- (void)responseToBtnClickBlock:(void(^)(int))comeBackBlock;

@end
