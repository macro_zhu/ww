//
//  CustomTabBarController.h
//  WW
//
//  Created by Macro on 15/6/8.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabBarController : UITabBarController

- (void)changeViewControllerWithIndex:(NSInteger)index;

- (void)isHideCustomTabbar:(BOOL)isHide;

- (void)isHideTabbarUserEnable:(BOOL)isEnable;

- (void)changeCenterBtn:(UIButton*)sender;
@end
