//
//  CustomTabBarController.m
//  WW
//
//  Created by Macro on 15/6/8.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "CustomTabBarController.h"
#import "HorizontalButton.h"
#import "UIColor+HEX.h"
#import <objc/runtime.h>
#import "MaskView.h"
#import "WriteArticleController.h"

#define  kTabbarBackgroudColor [UIColor colorWithHexString:@"FFFFFF"]

static CGFloat tabbarHeight = 49;

@interface CustomTabBarController ()
{
//    UIImageView *_tabbarIMV; //自定义的tabbar
    
    HorizontalButton *_previousButton;//记录前一次选中的按钮
    CGFloat _tabBarViewY;       //tabbar的y坐标
    
    NSArray *_viewControllerArray;   //组成tabbar的viewController的数组
}


@property (nonatomic,strong) UIImageView *tabbarIMV;
@end

@implementation CustomTabBarController

- (void)dealloc{

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.hidden = YES;
    _tabBarViewY = self.view.frame.size.height - tabbarHeight;

    _tabbarIMV = [[UIImageView alloc] initWithFrame:CGRectMake(0, _tabBarViewY, kWindowWidth, tabbarHeight)];
    _tabbarIMV.userInteractionEnabled = YES;
    
    _tabbarIMV.backgroundColor = kTabbarBackgroudColor;
    [self.view addSubview:_tabbarIMV];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UINavigationController *nav1 = [storyboard instantiateViewControllerWithIdentifier:@"MengTuNav"];
    UINavigationController *nav2 = [storyboard instantiateViewControllerWithIdentifier:@"TeskNav"];
    UINavigationController *nav3 = [storyboard instantiateViewControllerWithIdentifier:@"DuiHuanNav"];
    UINavigationController *nav4 = [storyboard instantiateViewControllerWithIdentifier:@"MineNav"];
    
    self.viewControllers = @[nav1,nav2,nav3,nav4];
    
    [self creatButtonWithNormalName:@"home_icon_men" andSelectName:@"home_icon_men_h" andTitle:@"萌图" andIndex:0];
    [self creatButtonWithNormalName:@"home_icon_task" andSelectName:@"home_icon_task_h" andTitle:@"任务" andIndex:1];
    [self creatButtonWithNormalName:@"home_icon_publish" andSelectName:@"home_icon_publish" andIndex:0];
    [self creatButtonWithNormalName:@"home_icon_change" andSelectName:@"home_icon_change_h" andTitle:@"兑换" andIndex:2];
    [self creatButtonWithNormalName:@"home_icon_me" andSelectName:@"home_icon_me_h" andTitle:@"我" andIndex:3];
    [self changeViewControllerWithIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.view.backgroundColor = kTabbarBackgroudColor;
    }
    return self ;
}


#pragma mark - tabbar操作
- (void)changeCenterBtn:(UIButton*)sender{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    MaskView *view = [[MaskView alloc] initWithFrame:CGRectMake(0, 0, kWindowWidth, kWindowHight)];
    [window addSubview:view];
    
    [[view.cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [view removeFromSuperview];
    }];
    
//    @weakify(self)
    [view responseToBtnClickBlock:^(int tag) {
//        @strongify(self)
            [view removeFromSuperview];
            [TabbarUtil changeViewControllerWithTabbarIndex:0];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"clickMaskViewWriteArticleBtn" object:@(tag)];
    }];
}

-(void)changeViewController:(HorizontalButton*)sender{
    NSInteger tag = sender.tag;
    self.selectedIndex = tag-100;
    DLog(@"self.selectedIndex = %ld sender.tag = %ld",self.selectedIndex,sender.tag);
    sender.enabled = NO;
    if (_previousButton != sender) {
        _previousButton.enabled = YES;
    }
    
    _previousButton = sender;
    self.selectedViewController = self.viewControllers[tag - 100];
}

- (void)changeViewControllerWithIndex:(NSInteger)index{
    HorizontalButton *btn = (HorizontalButton*)[self.view viewWithTag:index+100];
    NSInteger tag = btn.tag;
    btn.enabled = NO;
    if (_previousButton != btn) {
        _previousButton.enabled = YES;
    }
    
    _previousButton = btn;
    self.selectedViewController = self.viewControllers[tag - 100];
}

- (void)isHideCustomTabbar:(BOOL)isHide{
    self.tabBar.hidden = YES;
    _tabbarIMV.hidden = isHide;
}

- (void)isHideTabbarUserEnable:(BOOL)isEnable{
    _tabbarIMV.userInteractionEnabled = isEnable;
}

#pragma mark - 自定义函数部分
- (void)creatButtonWithNormalName:(NSString *)normal andSelectName:(NSString *)selected andIndex:(int)index{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = index + 100;
    
    CGFloat buttonW = _tabbarIMV.frame.size.width / (self.viewControllers.count +1);
    CGFloat buttonH = _tabbarIMV.frame.size.height;
    
    button.frame = CGRectMake(buttonW * index, 0, buttonW, buttonH);
    button.center = CGPointMake(kWindowWidth/2, 24);
    [button setImage:[UIImage imageNamed:normal] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:selected] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(changeCenterBtn:) forControlEvents:UIControlEventTouchDown];
    
    button.imageView.contentMode = UIViewContentModeCenter;
    
    [_tabbarIMV addSubview:button];
}

- (void)creatButtonWithNormalName:(NSString *)normal andSelectName:(NSString *)selected andTitle:(NSString *)title andIndex:(int)index{
    HorizontalButton * customButton = [HorizontalButton buttonWithType:UIButtonTypeCustom];
    customButton.tag = index+100;
    
    CGFloat buttonW = _tabbarIMV.frame.size.width / (self.viewControllers.count + 1);
    CGFloat buttonH = _tabbarIMV.frame.size.height;
    if (index<=1) {
      customButton.frame = CGRectMake(buttonW* index, 0, buttonW, buttonH);
    }else{
        customButton.frame = CGRectMake(buttonW* index +buttonW, 0, buttonW, buttonH);
    }
    
    [customButton setImage:[UIImage imageNamed:normal] forState:UIControlStateNormal];
    [customButton setImage:[UIImage imageNamed:selected] forState:UIControlStateDisabled];
    [customButton setTitleColor:[UIColor colorWithHexString:@"756b5f"] forState:UIControlStateNormal];
    [customButton setTitleColor:[UIColor colorWithHexString:@"ff6f0e"] forState:UIControlStateDisabled];
    [customButton setTitle:title forState:UIControlStateNormal];
    
    [customButton addTarget:self action:@selector(changeViewController:) forControlEvents:UIControlEventTouchDown];
    
    customButton.imageView.contentMode = UIViewContentModeCenter;
    customButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    customButton.titleLabel.font = [UIFont systemFontOfSize:10];
    
    [_tabbarIMV addSubview:customButton];
}

@end
