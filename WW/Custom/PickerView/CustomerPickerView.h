//
//  CustomerPickerView.h
//  WW
//
//  Created by Macro on 15/6/10.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PickerViewTopBtnComeBackBlock)(id);

@interface CustomerPickerView : UIView

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@property (nonatomic,strong) NSMutableArray *returnOrInitDataArray;

- (void)resetReturnOrInitDataArray:(NSMutableArray *)array;

- (void)setDataArray:(NSArray*)dataArray cancelComeBack:(PickerViewTopBtnComeBackBlock)block andComfirmComeBack:(PickerViewTopBtnComeBackBlock)block2;

@end
