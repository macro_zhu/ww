//
//  CustomerPickerView.m
//  WW
//
//  Created by Macro on 15/6/10.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "CustomerPickerView.h"

@interface CustomerPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    NSArray *_dataArray;
}
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *confirmBtn;

@property (nonatomic,copy) PickerViewTopBtnComeBackBlock cancelBackBlock;
@property (nonatomic,copy) PickerViewTopBtnComeBackBlock confirmBackBlock;

@end

@implementation CustomerPickerView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"CustomerPickerView" owner:self options:nil] objectAtIndex:0];
        self.frame = frame;
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        [self initControls];
        
        _returnOrInitDataArray = [NSMutableArray array];
        self.hidden = YES;
    }
    return self;
}

#pragma mark - 初始化
- (void)initControls{
    //取消
    [[_cancelBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        self.hidden = YES;
        _cancelBackBlock(nil);
    }];
    
    //确定
    [[_confirmBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        self.hidden = YES;
        _confirmBackBlock(_returnOrInitDataArray);
        [_returnOrInitDataArray removeAllObjects];
    }];
}

- (void)initComeBackDataArray{
    if (_returnOrInitDataArray.count == 0 ||_returnOrInitDataArray.count < _dataArray.count) {
        for (NSArray* array in _dataArray) {
            [_returnOrInitDataArray addObject:array[0]];
        }
    }
    
    NSInteger count = _dataArray.count;
    for (int i = 0; i < count; i++) {
        if (i == 0) {
            if ([(NSArray*)_dataArray[0] count]>0) {
                [_dataArray[0] enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL *stop) {
                    if ([_returnOrInitDataArray[0] isEqualToString:obj]) {
                      [_pickerView selectRow:idx inComponent:0 animated:YES];
                        *stop = YES;
                    }
                }];
            }
        }else if (i == 1){
            if ([(NSArray*)_dataArray[1] count]>0) {
                [_dataArray[1] enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL *stop) {
                    if ([_returnOrInitDataArray[1] isEqualToString:obj]) {
                        [_pickerView selectRow:idx inComponent:1 animated:YES];
                        *stop = YES;
                    }
                }];
            }
        }else{
            if ([(NSArray*)_dataArray[2] count]>0) {
                [_dataArray[2] enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL *stop) {
                    if ([_returnOrInitDataArray[2] isEqualToString:obj]) {
                        [_pickerView selectRow:idx inComponent:2 animated:YES];
                        *stop = YES;
                    }
                }];
            }
        }
    }
}


#pragma mark - Action
- (void)setDataArray:(NSArray*)dataArray cancelComeBack:(PickerViewTopBtnComeBackBlock)block andComfirmComeBack:(PickerViewTopBtnComeBackBlock)block2{
    _dataArray = dataArray;
    _cancelBackBlock = [block copy];
    _confirmBackBlock = [block2 copy];
    self.hidden = NO;
//    [_returnOrInitDataArray removeAllObjects];
    [self reloadData];
    [self initComeBackDataArray];
}

- (void)reloadData{
    [_pickerView reloadAllComponents];
}

- (void)resetReturnOrInitDataArray:(NSMutableArray *)array{
    [self.returnOrInitDataArray removeAllObjects];
    self.returnOrInitDataArray = array;
}

#pragma mark - Delegate
#pragma mark - PickerView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return _dataArray.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_dataArray[component] count];
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return ([_dataArray[component] count]>0)?_dataArray[component][row]:@"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSString *componentStr = _dataArray[component][row];
    [_returnOrInitDataArray replaceObjectAtIndex:component withObject:componentStr];
}

//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
//    NSMutableParagraphStyle *ps = [[NSMutableParagraphStyle alloc] init];
//    [ps setAlignment:NSTextAlignmentCenter];
//    NSDictionary *attribs = @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica Neue" size:27],NSParagraphStyleAttributeName:ps,NSForegroundColorAttributeName:[UIColor colorWithHexString:@"2b2b2b"]};
//    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"123" attributes:attribs];
//    return str;
//}

@end
