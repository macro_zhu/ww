//
//  BaseHandler.h
//  MyFramework
//
//  Created by 朱健 on 15/2/8.
//  Copyright (c) 2015年 朱健. All rights reserved.
//

#import <Foundation/Foundation.h>

//处理完事务之后的回调
typedef void(^CompleteBlock)();

//处理成功时的成功回调
typedef void(^SuccessBlock)(id obj);

//处理失败时的失败回调
typedef void(^FailedBlock)(id obj);

@interface BasicHandler : NSObject

+(NSString*)requestUrlWithPath:(NSString*)path;

@end
