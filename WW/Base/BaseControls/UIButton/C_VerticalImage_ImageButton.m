//
//  C_VerticalImage_ImageButton.m
//  ASYC
//
//  Created by 朱健 on 15/5/10.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "C_VerticalImage_ImageButton.h"

@interface C_VerticalImage_ImageButton (){
    NSMutableArray *_buttomIMVArray; //用于存储底部imageView
}

@end

@implementation C_VerticalImage_ImageButton

-(id)init{
    if (self = [super init]) {
        
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {

    }
    return self;
}

#pragma mark - 设置button内部的image范围
-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.height;
    return CGRectMake(0, 0, imageW, imageH);
}

@end
