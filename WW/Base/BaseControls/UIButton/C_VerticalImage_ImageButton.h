//
//  C_VerticalImage_ImageButton.h
//  ASYC
//
//  Created by 朱健 on 15/5/10.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface C_VerticalImage_ImageButton : UIButton

@property (nonatomic,strong) UIColor *normalColor;
@property (nonatomic,strong) UIColor *selectedColor;
@property (nonatomic,strong) UIImageView *buttomImageView;

@end
