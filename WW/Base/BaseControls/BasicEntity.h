//
//  BaseEntity.h
//  MyFramework
//
//  Created by 朱健 on 15/2/8.
//  Copyright (c) 2015年 朱健. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasicEntity : NSObject

@property (nonatomic,copy)      NSString *_id;      //ID
@property (nonatomic,assign)    int status;         //状态
@property (nonatomic,copy)      NSString *msg;      //状态信息
@property (nonatomic,copy)      NSString *version;  //版本号
@property (nonatomic,copy)      NSString *updateURL;//升级URL

/**
 *  解析HTTP返回异常JSON
 *
 *  @param json
 *
 *  @return
 */
+(instancetype)parseResponseErrorJSON:(id)json;

/**
 *  解析成功或失败状态JSON
 *
 *  @param json
 *
 *  @return
 */
+ (BasicEntity *)parseResponseStatusJSON:(id)json;

/**
 *  解析版本号及升级URL JSON
 *
 *  @param json
 *
 *  @return
 */
+ (BasicEntity *)parseResponseUpdateJSON:(id)json;


@end
