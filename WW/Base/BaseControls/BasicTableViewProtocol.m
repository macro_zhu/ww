//
//  BasicTableViewProtocol.m
//  MyFramework
//
//  Created by 朱健 on 15/2/8.
//  Copyright (c) 2015年 朱健. All rights reserved.
//

#import "BasicTableViewProtocol.h"

@interface BasicTableViewProtocol ()

@property (nonatomic,copy) NSString  *cellIdentifier;
@property (nonatomic,strong) NSArray *items;
@property (nonatomic,assign) NSInteger numberOfSection;
@property (nonatomic,copy) TableViewCellConfigurate cellConfigurateBlock;
@property (nonatomic,copy) TableViewNumberOfRowsInSectionConfigurate rowInSectionConfiguratteBlock;
@end

@implementation BasicTableViewProtocol

-(id)initWithItems:(NSArray *)aItems
    cellIdentifier:(NSString *)aCellIdentifier
  numberOfSections:(NSInteger)aSectionNumber
numberOfRowsInSectionConfigureBlock:(TableViewNumberOfRowsInSectionConfigurate)aNunberOfRowsInSectionConfigureBlock
cellConfigureBlock:(TableViewCellConfigurate)aCellConfigureBlock{
    self = [super init];
    if (self) {
        self.items = aItems;
        self.cellIdentifier = aCellIdentifier;
        self.numberOfSection = aSectionNumber;
        self.rowInSectionConfiguratteBlock = [aNunberOfRowsInSectionConfigureBlock copy];
        self.cellConfigurateBlock = [aCellConfigureBlock copy];
    }
    return self;
}

-(id)initWithItems:(NSArray *)aItems
    cellIdentifier:(NSString *)aCellIdentifier
cellConfigureBlock:(TableViewCellConfigurate)aCellConfigureBlock{
    return  [self initWithItems:aItems
                 cellIdentifier:aCellIdentifier
               numberOfSections:1
numberOfRowsInSectionConfigureBlock:nil
             cellConfigureBlock:aCellConfigureBlock];
}

#pragma mark UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.numberOfSection;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectRowAtIndexPath:)]) {
        [self.delegate selectRowAtIndexPath:indexPath];
    }
}

#pragma mark UITableViewDataSource Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.rowInSectionConfiguratteBlock) {
        return self.rowInSectionConfiguratteBlock(section);
    }else{
        return self.items.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    NSAssert(cell, @"cell is nil");
    id item = self.items[indexPath.row];
    self.cellConfigurateBlock(cell,item,indexPath);
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(deleteRowAtIndexPath:)]) {
            [self.delegate deleteRowAtIndexPath:indexPath];
        }
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(isCellEditable)]) {
        return [self.delegate isCellEditable];
    }else{
        return NO;
    }
}

@end
