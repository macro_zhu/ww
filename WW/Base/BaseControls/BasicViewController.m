//
//  BasicViewController.m
//  MyFramework
//
//  Created by 朱健 on 15/2/8.
//  Copyright (c) 2015年 朱健. All rights reserved.
//

#import "BasicViewController.h"

@interface BasicViewController ()

@end

@implementation BasicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.edgesForExtendedLayout =UIRectEdgeNone ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化
- (void)initBaseProperties{
    
}


- (void)initControls{
    
}

- (void)initDatasource{

}

#pragma mark - 自定义函数部分

#pragma mark - 响应用户操作部分

#pragma mark - 网络请求部分

#pragma mark - Delegate

@end
