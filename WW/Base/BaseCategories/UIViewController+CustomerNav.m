//
//  UIViewController+CustomerNav.m
//  ASYC
//
//  Created by 朱健 on 15/5/3.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "UIViewController+CustomerNav.h"
#import "HorizontalButton.h"


@implementation UIViewController (CustomerNav)


- (void)setNavTitle:(NSString *)title {
  UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
  titleLbl.textColor = [UIColor whiteColor];
  titleLbl.font = [UIFont systemFontOfSize:20];
  titleLbl.textAlignment = NSTextAlignmentCenter;
  titleLbl.text = title;
  self.navigationItem.titleView = titleLbl;
}

-(void)setNavImage:(NSString*)imageName{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    imageView.center = CGPointMake(kWindowWidth/2, 44);
    imageView.image = [UIImage imageNamed:imageName];
    self.navigationItem.titleView = imageView;
}

- (void)setNavLeftImage:(NSString*)imageName complete:(void(^)(void))comeBack{
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    leftBtn.layer.cornerRadius = leftBtn.frame.size.width/2;
    leftBtn.layer.masksToBounds = YES;
    leftBtn.layer.borderWidth = 1;
    leftBtn.layer.borderColor = [UIColor colorWithHexString:@"35D2AD"].CGColor;
    [[leftBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        comeBack();
    }];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
}

- (void)setNavLeftImageWithImageData:(NSData*)imageData complete:(void(^)(void))comeBack{
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftBtn setImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
    leftBtn.layer.cornerRadius = leftBtn.frame.size.width/2;
    leftBtn.layer.masksToBounds = YES;
    leftBtn.layer.borderWidth = 1;
    leftBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [[leftBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        comeBack();
    }];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
}

-(void)setNavBackWithComeplete:(void(^)(void))comeBack{
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    [leftBtn setImage:[UIImage imageNamed:@"PC2_Back"] forState:UIControlStateNormal];
//    [leftBtn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [[leftBtn rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(id x) {
        comeBack();
    }];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
}

- (void)setNavBackItemWithTitle:(NSString *)title  complete:(void(^)(void))comeBack{
  UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];

  // 按钮背景图片自适应
  UIImage *buttonImage = [[UIImage imageNamed:@""]
      resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 5)];
  UIImage *buttonPressedImage =
      [[UIImage imageNamed:@""]
          resizableImageWithCapInsets:UIEdgeInsetsMake(0, 17, 0, 5)];

  // 按钮文字样式
  [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:15.0]];
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [button setTitleColor:[UIColor whiteColor]
               forState:UIControlStateHighlighted];
  [button setTitleShadowColor:[UIColor clearColor]
                     forState:UIControlStateNormal];
  [button setTitleShadowColor:[UIColor clearColor]
                     forState:UIControlStateHighlighted];
  [[button titleLabel] setShadowOffset:CGSizeMake(0.0, 1.0)];

  // 设置按钮大小
  CGRect buttonFrame = [button frame];
  buttonFrame.size.width =
      [title
          boundingRectWithSize:CGSizeMake(MAXFLOAT, 20)
                       options:NSStringDrawingUsesLineFragmentOrigin |
                               NSStringDrawingUsesFontLeading
                    attributes:@{
                      NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0]
                    } context:nil]
          .size.width;
  buttonFrame.size.height = buttonImage.size.height;
  [button setFrame:buttonFrame];

  // 设置按钮背景
  [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
  [button setBackgroundImage:buttonPressedImage
                    forState:UIControlStateHighlighted];
  [button setTitle:[NSString stringWithFormat:@"%@", title]
          forState:UIControlStateNormal];

  // 按钮事件
//  [button addTarget:self
//                action:@selector(BackBarItemClick)
//      forControlEvents:UIControlEventTouchUpInside];
[[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
    comeBack();
}];
    
  // 添加到导航条
  self.navigationItem.leftBarButtonItem =
      [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)BackBarItemClick {
  NSArray *viewControllerArray = [self.navigationController viewControllers];
  if ([viewControllerArray count] > 1) {
    [self.navigationController popViewControllerAnimated:YES];
  } else {
    [self dismissViewControllerAnimated:YES
                             completion:^{
                             }];
  }
}

-(void)setNavRightImage:(UIImage*)image complete:(void(^)(void))comeBackBlock{
    UIButton *rightNavBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightNavBtn setImage:image forState:UIControlStateNormal];
    [[rightNavBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        comeBackBlock();
    }];
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:rightNavBtn];
}

-(UIButton*)setNavRightTitle:(NSString*)title complete:(void(^)(void))comeBackBlock{
    UIButton *rightNavBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [rightNavBtn setTitle:title forState:UIControlStateNormal];
    [rightNavBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[rightNavBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        comeBackBlock();
    }];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightNavBtn];
    return rightNavBtn;
}

@end
