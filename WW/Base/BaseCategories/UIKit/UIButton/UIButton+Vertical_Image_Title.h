//
//  UIButton+Vertical_Image_Title.h
//  WW
//
//  Created by Macro on 15/6/8.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+Block.h"
@interface UIButton (Vertical_Image_Title)

- (id)initVerticalButtonWithFrame:(CGRect)frame title:(NSString*)title Image:(NSString*)imageName andActionHander:(TouchedBlock)block;

@end
