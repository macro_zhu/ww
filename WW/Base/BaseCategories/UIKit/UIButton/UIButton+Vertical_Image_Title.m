//
//  UIButton+Vertical_Image_Title.m
//  WW
//
//  Created by Macro on 15/6/8.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "UIButton+Vertical_Image_Title.h"
#import <objc/runtime.h>

static const void *UIButtonBlockKey = &UIButtonBlockKey;
static NSString *const UILabelKey      = @"UILableKey";

@implementation UIButton (Vertical_Image_Title)

- (id)initVerticalButtonWithFrame:(CGRect)frame title:(NSString*)title Image:(NSString*)imageName andActionHander:(TouchedBlock)block{
    
    objc_setAssociatedObject(self, UIButtonBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height-20)];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = [UIImage imageNamed:imageName];
    [button addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,  frame.size.height - 20, frame.size.width, 20)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.highlightedTextColor = [UIColor orangeColor];
    label.highlighted = NO;
    label.text = title;
    label.font = [UIFont systemFontOfSize:12];
    objc_setAssociatedObject(self, &UILabelKey, label, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [button addSubview:label];
    
    [button addTarget:self action:@selector(actionTouched:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(void)actionTouched:(UIButton*)btn{
    TouchedBlock block = objc_getAssociatedObject(self, UIButtonBlockKey);
    UILabel *label = objc_getAssociatedObject(self, &UILabelKey);
    label.highlighted = !label.highlighted;
    if (block) {
        block(btn.tag);
    }
}

@end
