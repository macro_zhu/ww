//
//  UIViewController+CustomerNav.h
//  ASYC
//
//  Created by 朱健 on 15/5/3.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (CustomerNav)

/**
 *  设置标题
 *
 *  @param title 标题
 */
-(void)setNavTitle:(NSString*)title;

/**
 *  设置左导航栏图片以及点击事件
 *
 *  @param imageName 图片名称
 *  @param comeBack  点击按钮回调
 */
- (void)setNavLeftImage:(NSString*)imageName complete:(void(^)(void))comeBack;

/**
 *  设置左导航栏图片以及点击事件
 *
 *  @param imageData 图片二进制数据
 *  @param comeBack  点击按钮回调
 */
- (void)setNavLeftImageWithImageData:(NSData*)imageData complete:(void(^)(void))comeBack;
/**
 *  设置标题图片
 *
 *  @param imageName 图片名
 */
-(void)setNavImage:(NSString*)imageName;

/**
 *  设置返回按钮
 *
 *  @param comeBack 点击返回按钮回调
 */
-(void)setNavBackWithComeplete:(void(^)(void))comeBack;
/**
 *  设置导航栏左侧按钮
 *
 *  @param leftBtn  左按钮
 */
- (void)setNavBackItemWithTitle:(NSString *)title  complete:(void(^)(void))comeBack;

/**
 *  设置右导航键（图片）
 *
 *  @param image         图片
 *  @param comeBackBlock 请求完成回调
 */
-(void)setNavRightImage:(UIImage*)image complete:(void(^)(void))comeBackBlock;

/**
 *  设置右导航键（biaoti）
 *
 *  @param title         标题
 *  @param comeBackBlock 请求完成回调
 */
-(UIButton*)setNavRightTitle:(NSString*)title complete:(void(^)(void))comeBackBlock;
/**
 *  设置导航栏左侧多个按钮
 *
 *  @param leftBtnArray 按钮数组
 */
//-(void)setNavLeftBtns:(NSArray*)leftBtnArray;



@end
