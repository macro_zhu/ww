#import <UIKit/UIKit.h>
#import "UserMessageData.h"

@interface UserMessageResult : NSObject

@property (nonatomic, strong) UserMessageData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end