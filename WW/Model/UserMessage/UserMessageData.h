#import <UIKit/UIKit.h>

@interface UserMessageData : NSObject

@property (nonatomic, strong) NSString * alias;
@property (nonatomic, strong) NSString * avatar;
@property (nonatomic, assign) NSInteger gender;
@property (nonatomic, strong) NSString * petAge;
@property (nonatomic, strong) NSString * petArea;
@property (nonatomic, assign) NSInteger petAreaId;
@property (nonatomic, strong) NSString * petBirthday;
@property (nonatomic, strong) NSString * petType;
@property (nonatomic, assign) NSInteger petTypeId;
@property (nonatomic, strong) NSString * phone;
@property (nonatomic, assign) NSInteger userId;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end