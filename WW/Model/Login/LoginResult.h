#import <UIKit/UIKit.h>
#import "LoginData.h"

@interface LoginResult : NSObject

@property (nonatomic, strong) LoginData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end