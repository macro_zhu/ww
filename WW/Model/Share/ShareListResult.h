#import <UIKit/UIKit.h>
#import "ShareListData.h"

@interface ShareListResult : NSObject

@property (nonatomic, strong) ShareListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end