//
//	ShareAddData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "ShareAddData.h"

@interface ShareAddData ()
@end
@implementation ShareAddData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"add_date"] isKindOfClass:[NSNull class]]){
		self.addDate = dictionary[@"add_date"];
	}	
	if(![dictionary[@"add_time"] isKindOfClass:[NSNull class]]){
		self.addTime = [dictionary[@"add_time"] integerValue];
	}

	if(![dictionary[@"alias"] isKindOfClass:[NSNull class]]){
		self.alias = dictionary[@"alias"];
	}	
	if(![dictionary[@"avatar"] isKindOfClass:[NSNull class]]){
		self.avatar = dictionary[@"avatar"];
	}	
	if(![dictionary[@"share_id"] isKindOfClass:[NSNull class]]){
		self.shareId = [dictionary[@"share_id"] integerValue];
	}

	if(![dictionary[@"user_id"] isKindOfClass:[NSNull class]]){
		self.userId = [dictionary[@"user_id"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.addDate != nil){
		dictionary[@"add_date"] = self.addDate;
	}
	dictionary[@"add_time"] = @(self.addTime);
	if(self.alias != nil){
		dictionary[@"alias"] = self.alias;
	}
	if(self.avatar != nil){
		dictionary[@"avatar"] = self.avatar;
	}
	dictionary[@"share_id"] = @(self.shareId);
	dictionary[@"user_id"] = @(self.userId);
	return dictionary;

}
@end