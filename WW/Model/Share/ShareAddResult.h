#import <UIKit/UIKit.h>
#import "ShareAddData.h"

@interface ShareAddResult : NSObject

@property (nonatomic, strong) ShareAddData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end