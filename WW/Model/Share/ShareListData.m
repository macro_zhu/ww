//
//	ShareListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "ShareListData.h"

@interface ShareListData ()
@end
@implementation ShareListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"share_list"] != nil && ![dictionary[@"share_list"] isKindOfClass:[NSNull class]]){
		NSArray * shareListDictionaries = dictionary[@"share_list"];
		NSMutableArray * shareListItems = [NSMutableArray array];
		for(NSDictionary * shareListDictionary in shareListDictionaries){
			ShareList * shareListItem = [[ShareList alloc] initWithDictionary:shareListDictionary];
			[shareListItems addObject:shareListItem];
		}
		self.shareList = shareListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.shareList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(ShareList * shareListElement in self.shareList){
			[dictionaryElements addObject:[shareListElement toDictionary]];
		}
		dictionary[@"share_list"] = dictionaryElements;
	}
	return dictionary;

}
@end