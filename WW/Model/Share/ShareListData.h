#import <UIKit/UIKit.h>
#import "ShareList.h"

@interface ShareListData : NSObject

@property (nonatomic, strong) NSArray * shareList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end