#import <UIKit/UIKit.h>

@interface PicturePublishData : NSObject

@property (nonatomic, assign) NSInteger articleId;
@property (nonatomic, strong) NSString * gained;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end