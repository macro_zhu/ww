//
//	PicturePublishData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PicturePublishData.h"

@interface PicturePublishData ()
@end
@implementation PicturePublishData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"article_id"] isKindOfClass:[NSNull class]]){
		self.articleId = [dictionary[@"article_id"] integerValue];
	}

	if(![dictionary[@"gained"] isKindOfClass:[NSNull class]]){
		self.gained = dictionary[@"gained"];
	}	
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	dictionary[@"article_id"] = @(self.articleId);
	if(self.gained != nil){
		dictionary[@"gained"] = self.gained;
	}
	return dictionary;

}
@end