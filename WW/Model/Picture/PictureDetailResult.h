#import <UIKit/UIKit.h>
#import "PictureDetailData.h"

@interface PictureDetailResult : NSObject

@property (nonatomic, strong) PictureDetailData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end