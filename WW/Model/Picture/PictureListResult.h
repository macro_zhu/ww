#import <UIKit/UIKit.h>
#import "PictureListData.h"

@interface PictureListResult : NSObject

@property (nonatomic, strong) PictureListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end