//
//	PictureDetailData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PictureDetailData.h"

@interface PictureDetailData ()
@end
@implementation PictureDetailData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"alias"] isKindOfClass:[NSNull class]]){
		self.alias = dictionary[@"alias"];
	}	
	if(![dictionary[@"article_id"] isKindOfClass:[NSNull class]]){
		self.articleId = [dictionary[@"article_id"] integerValue];
	}

	if(![dictionary[@"avatar"] isKindOfClass:[NSNull class]]){
		self.avatar = dictionary[@"avatar"];
	}	
	if(![dictionary[@"city"] isKindOfClass:[NSNull class]]){
		self.city = dictionary[@"city"];
	}	
	if(![dictionary[@"comment_number"] isKindOfClass:[NSNull class]]){
		self.commentNumber = [dictionary[@"comment_number"] integerValue];
	}

	if(![dictionary[@"content"] isKindOfClass:[NSNull class]]){
		self.content = dictionary[@"content"];
	}	
	if(![dictionary[@"date_des"] isKindOfClass:[NSNull class]]){
		self.dateDes = dictionary[@"date_des"];
	}	
	if(![dictionary[@"gender"] isKindOfClass:[NSNull class]]){
		self.gender = [dictionary[@"gender"] integerValue];
	}

	if(![dictionary[@"image"] isKindOfClass:[NSNull class]]){
		self.image = dictionary[@"image"];
	}	
	if(![dictionary[@"is_followed"] isKindOfClass:[NSNull class]]){
		self.isFollowed = [dictionary[@"is_followed"] boolValue];
	}

	if(![dictionary[@"is_praised"] isKindOfClass:[NSNull class]]){
		self.isPraised = [dictionary[@"is_praised"] boolValue];
	}

	if(![dictionary[@"pet_age"] isKindOfClass:[NSNull class]]){
		self.petAge = dictionary[@"pet_age"];
	}	
	if(![dictionary[@"pet_area"] isKindOfClass:[NSNull class]]){
		self.petArea = dictionary[@"pet_area"];
	}	
	if(![dictionary[@"pet_birthday"] isKindOfClass:[NSNull class]]){
		self.petBirthday = dictionary[@"pet_birthday"];
	}	
	if(![dictionary[@"pet_type"] isKindOfClass:[NSNull class]]){
		self.petType = dictionary[@"pet_type"];
	}	
	if(![dictionary[@"praise_number"] isKindOfClass:[NSNull class]]){
		self.praiseNumber = [dictionary[@"praise_number"] integerValue];
	}

	if(![dictionary[@"tags"] isKindOfClass:[NSNull class]]){
		self.tags = dictionary[@"tags"];
	}	
	if(![dictionary[@"user_id"] isKindOfClass:[NSNull class]]){
		self.userId = [dictionary[@"user_id"] integerValue];
	}

	if(![dictionary[@"view_number"] isKindOfClass:[NSNull class]]){
		self.viewNumber = [dictionary[@"view_number"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.alias != nil){
		dictionary[@"alias"] = self.alias;
	}
	dictionary[@"article_id"] = @(self.articleId);
	if(self.avatar != nil){
		dictionary[@"avatar"] = self.avatar;
	}
	if(self.city != nil){
		dictionary[@"city"] = self.city;
	}
	dictionary[@"comment_number"] = @(self.commentNumber);
	if(self.content != nil){
		dictionary[@"content"] = self.content;
	}
	if(self.dateDes != nil){
		dictionary[@"date_des"] = self.dateDes;
	}
	dictionary[@"gender"] = @(self.gender);
	if(self.image != nil){
		dictionary[@"image"] = self.image;
	}
	dictionary[@"is_followed"] = @(self.isFollowed);
	dictionary[@"is_praised"] = @(self.isPraised);
	if(self.petAge != nil){
		dictionary[@"pet_age"] = self.petAge;
	}
	if(self.petArea != nil){
		dictionary[@"pet_area"] = self.petArea;
	}
	if(self.petBirthday != nil){
		dictionary[@"pet_birthday"] = self.petBirthday;
	}
	if(self.petType != nil){
		dictionary[@"pet_type"] = self.petType;
	}
	dictionary[@"praise_number"] = @(self.praiseNumber);
	if(self.tags != nil){
		dictionary[@"tags"] = self.tags;
	}
	dictionary[@"user_id"] = @(self.userId);
	dictionary[@"view_number"] = @(self.viewNumber);
	return dictionary;

}
@end