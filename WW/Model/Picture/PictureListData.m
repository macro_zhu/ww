//
//	PictureListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PictureListData.h"

@interface PictureListData ()
@end
@implementation PictureListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"article_list"] != nil && ![dictionary[@"article_list"] isKindOfClass:[NSNull class]]){
		NSArray * articleListDictionaries = dictionary[@"article_list"];
		NSMutableArray * articleListItems = [NSMutableArray array];
		for(NSDictionary * articleListDictionary in articleListDictionaries){
			PictureListArticleList * articleListItem = [[PictureListArticleList alloc] initWithDictionary:articleListDictionary];
			[articleListItems addObject:articleListItem];
		}
		self.articleList = articleListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.articleList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(PictureListArticleList * articleListElement in self.articleList){
			[dictionaryElements addObject:[articleListElement toDictionary]];
		}
		dictionary[@"article_list"] = dictionaryElements;
	}
	return dictionary;

}
@end