#import <UIKit/UIKit.h>
#import "PicturePublishData.h"

@interface PicturePublishResult : NSObject

@property (nonatomic, strong) PicturePublishData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end