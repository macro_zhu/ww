#import <UIKit/UIKit.h>

@interface PictureDetailData : NSObject

@property (nonatomic, strong) NSString * alias;
@property (nonatomic, assign) NSInteger articleId;
@property (nonatomic, strong) NSString * avatar;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, assign) NSInteger commentNumber;
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * dateDes;
@property (nonatomic, assign) NSInteger gender;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, assign) BOOL isFollowed;
@property (nonatomic, assign) BOOL isPraised;
@property (nonatomic, strong) NSString * petAge;
@property (nonatomic, strong) NSString * petArea;
@property (nonatomic, strong) NSString * petBirthday;
@property (nonatomic, strong) NSString * petType;
@property (nonatomic, assign) NSInteger praiseNumber;
@property (nonatomic, strong) NSArray * tags;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger viewNumber;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end