#import <UIKit/UIKit.h>
#import "PictureListArticleList.h"

@interface PictureListData : NSObject

@property (nonatomic, strong) NSArray * articleList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end