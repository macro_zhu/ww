//
//	ExchangeList.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "ExchangeList.h"

@interface ExchangeList ()
@end
@implementation ExchangeList




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"add_date"] isKindOfClass:[NSNull class]]){
		self.addDate = dictionary[@"add_date"];
	}	
	if(![dictionary[@"add_time"] isKindOfClass:[NSNull class]]){
		self.addTime = [dictionary[@"add_time"] integerValue];
	}

	if(![dictionary[@"exchange_id"] isKindOfClass:[NSNull class]]){
		self.exchangeId = [dictionary[@"exchange_id"] integerValue];
	}

	if(![dictionary[@"exchange_status"] isKindOfClass:[NSNull class]]){
		self.exchangeStatus = [dictionary[@"exchange_status"] integerValue];
	}

	if(dictionary[@"product_info"] != nil && ![dictionary[@"product_info"] isKindOfClass:[NSNull class]]){
		NSArray * productInfoDictionaries = dictionary[@"product_info"];
		NSMutableArray * productInfoItems = [NSMutableArray array];
		for(NSDictionary * productInfoDictionary in productInfoDictionaries){
			ExchangeListProductInfo * productInfoItem = [[ExchangeListProductInfo alloc] initWithDictionary:productInfoDictionary];
			[productInfoItems addObject:productInfoItem];
		}
		self.productInfo = productInfoItems;
	}
	if(![dictionary[@"total_fee"] isKindOfClass:[NSNull class]]){
		self.totalFee = [dictionary[@"total_fee"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.addDate != nil){
		dictionary[@"add_date"] = self.addDate;
	}
	dictionary[@"add_time"] = @(self.addTime);
	dictionary[@"exchange_id"] = @(self.exchangeId);
	dictionary[@"exchange_status"] = @(self.exchangeStatus);
	if(self.productInfo != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(ExchangeListProductInfo * productInfoElement in self.productInfo){
			[dictionaryElements addObject:[productInfoElement toDictionary]];
		}
		dictionary[@"product_info"] = dictionaryElements;
	}
	dictionary[@"total_fee"] = @(self.totalFee);
	return dictionary;

}
@end