//
//	ExchangeDetailProductInfo.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "ExchangeDetailProductInfo.h"

@interface ExchangeDetailProductInfo ()
@end
@implementation ExchangeDetailProductInfo




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"category_name"] isKindOfClass:[NSNull class]]){
		self.categoryName = dictionary[@"category_name"];
	}	
	if(![dictionary[@"image"] isKindOfClass:[NSNull class]]){
		self.image = dictionary[@"image"];
	}	
	if(![dictionary[@"number"] isKindOfClass:[NSNull class]]){
		self.number = [dictionary[@"number"] integerValue];
	}

	if(![dictionary[@"price"] isKindOfClass:[NSNull class]]){
		self.price = [dictionary[@"price"] integerValue];
	}

	if(![dictionary[@"product_id"] isKindOfClass:[NSNull class]]){
		self.productId = [dictionary[@"product_id"] integerValue];
	}

	if(![dictionary[@"product_name"] isKindOfClass:[NSNull class]]){
		self.productName = dictionary[@"product_name"];
	}	
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.categoryName != nil){
		dictionary[@"category_name"] = self.categoryName;
	}
	if(self.image != nil){
		dictionary[@"image"] = self.image;
	}
	dictionary[@"number"] = @(self.number);
	dictionary[@"price"] = @(self.price);
	dictionary[@"product_id"] = @(self.productId);
	if(self.productName != nil){
		dictionary[@"product_name"] = self.productName;
	}
	return dictionary;

}
@end