#import <UIKit/UIKit.h>
#import "ExchangeList.h"

@interface ExchangeListData : NSObject

@property (nonatomic, strong) NSArray * exchangeList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end