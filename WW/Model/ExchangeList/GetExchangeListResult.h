#import <UIKit/UIKit.h>
#import "ExchangeListData.h"

@interface GetExchangeListResult : NSObject

@property (nonatomic, strong) ExchangeListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end