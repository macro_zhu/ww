#import <UIKit/UIKit.h>
#import "ExchangeDetailProductInfo.h"

@interface ExchangeDetailData : NSObject

@property (nonatomic, strong) NSString * addDate;
@property (nonatomic, assign) NSInteger addTime;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * consignee;
@property (nonatomic, strong) NSString * district;
@property (nonatomic, assign) NSInteger exchangeId;
@property (nonatomic, assign) NSInteger exchangeStatus;
@property (nonatomic, strong) NSString * phone;
@property (nonatomic, strong) NSArray * productInfo;
@property (nonatomic, strong) NSString * province;
@property (nonatomic, assign) NSInteger totalFee;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end