#import <UIKit/UIKit.h>
#import "ExchangeDetailData.h"

@interface ExchangeDetailResult : NSObject

@property (nonatomic, strong) ExchangeDetailData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end