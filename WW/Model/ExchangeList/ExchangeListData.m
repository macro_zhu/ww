//
//	ExchangeListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "ExchangeListData.h"

@interface ExchangeListData ()
@end
@implementation ExchangeListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"exchange_list"] != nil && ![dictionary[@"exchange_list"] isKindOfClass:[NSNull class]]){
		NSArray * exchangeListDictionaries = dictionary[@"exchange_list"];
		NSMutableArray * exchangeListItems = [NSMutableArray array];
		for(NSDictionary * exchangeListDictionary in exchangeListDictionaries){
			ExchangeList * exchangeListItem = [[ExchangeList alloc] initWithDictionary:exchangeListDictionary];
			[exchangeListItems addObject:exchangeListItem];
		}
		self.exchangeList = exchangeListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.exchangeList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(ExchangeList * exchangeListElement in self.exchangeList){
			[dictionaryElements addObject:[exchangeListElement toDictionary]];
		}
		dictionary[@"exchange_list"] = dictionaryElements;
	}
	return dictionary;

}
@end