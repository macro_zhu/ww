#import <UIKit/UIKit.h>
#import "ExchangeListProductInfo.h"

@interface ExchangeList : NSObject

@property (nonatomic, strong) NSString * addDate;
@property (nonatomic, assign) NSInteger addTime;
@property (nonatomic, assign) NSInteger exchangeId;
@property (nonatomic, assign) NSInteger exchangeStatus;
@property (nonatomic, strong) NSArray * productInfo;
@property (nonatomic, assign) NSInteger totalFee;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end