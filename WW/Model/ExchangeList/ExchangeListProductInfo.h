#import <UIKit/UIKit.h>

@interface ExchangeListProductInfo : NSObject

@property (nonatomic, strong) NSString * categoryName;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) NSInteger productId;
@property (nonatomic, strong) NSString * productName;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end