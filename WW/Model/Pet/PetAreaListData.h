#import <UIKit/UIKit.h>
#import "PetAreaList.h"

@interface PetAreaListData : NSObject

@property (nonatomic, strong) NSArray * petAreaList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end