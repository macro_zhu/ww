//
//	PetAreaList.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PetAreaList.h"

@interface PetAreaList ()
@end
@implementation PetAreaList




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"area"] isKindOfClass:[NSNull class]]){
		self.area = dictionary[@"area"];
	}	
	if(![dictionary[@"pet_area_id"] isKindOfClass:[NSNull class]]){
		self.petAreaId = [dictionary[@"pet_area_id"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.area != nil){
		dictionary[@"area"] = self.area;
	}
	dictionary[@"pet_area_id"] = @(self.petAreaId);
	return dictionary;

}
@end