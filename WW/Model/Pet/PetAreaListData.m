//
//	PetAreaListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PetAreaListData.h"

@interface PetAreaListData ()
@end
@implementation PetAreaListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"pet_area_list"] != nil && ![dictionary[@"pet_area_list"] isKindOfClass:[NSNull class]]){
		NSArray * petAreaListDictionaries = dictionary[@"pet_area_list"];
		NSMutableArray * petAreaListItems = [NSMutableArray array];
		for(NSDictionary * petAreaListDictionary in petAreaListDictionaries){
			PetAreaList * petAreaListItem = [[PetAreaList alloc] initWithDictionary:petAreaListDictionary];
			[petAreaListItems addObject:petAreaListItem];
		}
		self.petAreaList = petAreaListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.petAreaList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(PetAreaList * petAreaListElement in self.petAreaList){
			[dictionaryElements addObject:[petAreaListElement toDictionary]];
		}
		dictionary[@"pet_area_list"] = dictionaryElements;
	}
	return dictionary;

}
@end