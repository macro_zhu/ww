#import <UIKit/UIKit.h>

@interface PetListPetTypeList : NSObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, assign) NSInteger petTypeId;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end