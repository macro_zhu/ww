#import <UIKit/UIKit.h>
#import "PetAreaListData.h"

@interface PetAreaListResult : NSObject

@property (nonatomic, strong) PetAreaListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end