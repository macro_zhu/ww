#import <UIKit/UIKit.h>
#import "PetListPetTypeList.h"

@interface PetListData : NSObject

@property (nonatomic, strong) NSArray * petTypeList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end