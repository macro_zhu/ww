//
//	PetListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PetListData.h"

@interface PetListData ()
@end
@implementation PetListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"pet_type_list"] != nil && ![dictionary[@"pet_type_list"] isKindOfClass:[NSNull class]]){
		NSArray * petTypeListDictionaries = dictionary[@"pet_type_list"];
		NSMutableArray * petTypeListItems = [NSMutableArray array];
		for(NSDictionary * petTypeListDictionary in petTypeListDictionaries){
			PetListPetTypeList * petTypeListItem = [[PetListPetTypeList alloc] initWithDictionary:petTypeListDictionary];
			[petTypeListItems addObject:petTypeListItem];
		}
		self.petTypeList = petTypeListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.petTypeList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(PetListPetTypeList * petTypeListElement in self.petTypeList){
			[dictionaryElements addObject:[petTypeListElement toDictionary]];
		}
		dictionary[@"pet_type_list"] = dictionaryElements;
	}
	return dictionary;

}
@end