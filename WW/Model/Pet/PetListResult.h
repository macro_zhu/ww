#import <UIKit/UIKit.h>
#import "PetListData.h"

@interface PetListResult : NSObject

@property (nonatomic, strong) PetListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end