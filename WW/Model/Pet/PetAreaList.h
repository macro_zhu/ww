#import <UIKit/UIKit.h>

@interface PetAreaList : NSObject

@property (nonatomic, strong) NSString * area;
@property (nonatomic, assign) NSInteger petAreaId;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end