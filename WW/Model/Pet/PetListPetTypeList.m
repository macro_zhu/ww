//
//	PetListPetTypeList.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PetListPetTypeList.h"

@interface PetListPetTypeList ()
@end
@implementation PetListPetTypeList




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"name"] isKindOfClass:[NSNull class]]){
		self.name = dictionary[@"name"];
	}	
	if(![dictionary[@"pet_type_id"] isKindOfClass:[NSNull class]]){
		self.petTypeId = [dictionary[@"pet_type_id"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.name != nil){
		dictionary[@"name"] = self.name;
	}
	dictionary[@"pet_type_id"] = @(self.petTypeId);
	return dictionary;

}
@end