//
//	UserInfoData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "UserInfoData.h"

@interface UserInfoData ()
@end
@implementation UserInfoData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"alias"] isKindOfClass:[NSNull class]]){
		self.alias = dictionary[@"alias"];
	}	
	if(![dictionary[@"article_number"] isKindOfClass:[NSNull class]]){
		self.articleNumber = [dictionary[@"article_number"] integerValue];
	}

	if(![dictionary[@"avatar"] isKindOfClass:[NSNull class]]){
		self.avatar = dictionary[@"avatar"];
	}	
	if(![dictionary[@"coins_gained_today"] isKindOfClass:[NSNull class]]){
		self.coinsGainedToday = [dictionary[@"coins_gained_today"] integerValue];
	}

	if(![dictionary[@"coins_limit"] isKindOfClass:[NSNull class]]){
		self.coinsLimit = [dictionary[@"coins_limit"] integerValue];
	}

	if(![dictionary[@"comment_number"] isKindOfClass:[NSNull class]]){
		self.commentNumber = [dictionary[@"comment_number"] integerValue];
	}

	if(![dictionary[@"fans_number"] isKindOfClass:[NSNull class]]){
		self.fansNumber = [dictionary[@"fans_number"] integerValue];
	}

	if(![dictionary[@"follow_number"] isKindOfClass:[NSNull class]]){
		self.followNumber = [dictionary[@"follow_number"] integerValue];
	}

	if(![dictionary[@"gender"] isKindOfClass:[NSNull class]]){
		self.gender = [dictionary[@"gender"] integerValue];
	}

	if(![dictionary[@"pet_age"] isKindOfClass:[NSNull class]]){
		self.petAge = dictionary[@"pet_age"];
	}	
	if(![dictionary[@"pet_area"] isKindOfClass:[NSNull class]]){
		self.petArea = dictionary[@"pet_area"];
	}	
	if(![dictionary[@"pet_area_id"] isKindOfClass:[NSNull class]]){
		self.petAreaId = [dictionary[@"pet_area_id"] integerValue];
	}

	if(![dictionary[@"pet_birthday"] isKindOfClass:[NSNull class]]){
		self.petBirthday = dictionary[@"pet_birthday"];
	}	
	if(![dictionary[@"pet_type"] isKindOfClass:[NSNull class]]){
		self.petType = dictionary[@"pet_type"];
	}	
	if(![dictionary[@"pet_type_id"] isKindOfClass:[NSNull class]]){
		self.petTypeId = [dictionary[@"pet_type_id"] integerValue];
	}

	if(![dictionary[@"phone"] isKindOfClass:[NSNull class]]){
		self.phone = dictionary[@"phone"];
	}	
	if(![dictionary[@"praise_number"] isKindOfClass:[NSNull class]]){
		self.praiseNumber = [dictionary[@"praise_number"] integerValue];
	}

	if(![dictionary[@"share_number"] isKindOfClass:[NSNull class]]){
		self.shareNumber = [dictionary[@"share_number"] integerValue];
	}

	if(![dictionary[@"total_coins"] isKindOfClass:[NSNull class]]){
		self.totalCoins = [dictionary[@"total_coins"] integerValue];
	}

	if(![dictionary[@"user_id"] isKindOfClass:[NSNull class]]){
		self.userId = [dictionary[@"user_id"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.alias != nil){
		dictionary[@"alias"] = self.alias;
	}
	dictionary[@"article_number"] = @(self.articleNumber);
	if(self.avatar != nil){
		dictionary[@"avatar"] = self.avatar;
	}
	dictionary[@"coins_gained_today"] = @(self.coinsGainedToday);
	dictionary[@"coins_limit"] = @(self.coinsLimit);
	dictionary[@"comment_number"] = @(self.commentNumber);
	dictionary[@"fans_number"] = @(self.fansNumber);
	dictionary[@"follow_number"] = @(self.followNumber);
	dictionary[@"gender"] = @(self.gender);
	if(self.petAge != nil){
		dictionary[@"pet_age"] = self.petAge;
	}
	if(self.petArea != nil){
		dictionary[@"pet_area"] = self.petArea;
	}
	dictionary[@"pet_area_id"] = @(self.petAreaId);
	if(self.petBirthday != nil){
		dictionary[@"pet_birthday"] = self.petBirthday;
	}
	if(self.petType != nil){
		dictionary[@"pet_type"] = self.petType;
	}
	dictionary[@"pet_type_id"] = @(self.petTypeId);
	if(self.phone != nil){
		dictionary[@"phone"] = self.phone;
	}
	dictionary[@"praise_number"] = @(self.praiseNumber);
	dictionary[@"share_number"] = @(self.shareNumber);
	dictionary[@"total_coins"] = @(self.totalCoins);
	dictionary[@"user_id"] = @(self.userId);
	return dictionary;

}
@end