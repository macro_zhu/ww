#import <UIKit/UIKit.h>
#import "UserTaskInfoTaskInfo.h"

@interface UserTaskInfoData : NSObject

@property (nonatomic, strong) NSString * avatar;
@property (nonatomic, assign) NSInteger coinsGainedToday;
@property (nonatomic, assign) NSInteger coinsLimit;
@property (nonatomic, strong) NSArray * taskInfo;
@property (nonatomic, assign) NSInteger totalCoins;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end