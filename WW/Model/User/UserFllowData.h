#import <UIKit/UIKit.h>
#import "UserFllowFollowList.h"

@interface UserFllowData : NSObject

@property (nonatomic, strong) NSArray * followList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end