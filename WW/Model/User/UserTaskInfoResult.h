#import <UIKit/UIKit.h>
#import "UserTaskInfoData.h"

@interface UserTaskInfoResult : NSObject

@property (nonatomic, strong) UserTaskInfoData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end