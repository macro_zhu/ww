#import <UIKit/UIKit.h>
#import "UserFllowData.h"

@interface UserFllowResult : NSObject

@property (nonatomic, strong) UserFllowData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end