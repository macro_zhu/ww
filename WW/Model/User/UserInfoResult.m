//
//	UserInfoResult.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "UserInfoResult.h"

@interface UserInfoResult ()
@end
@implementation UserInfoResult




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"data"] isKindOfClass:[NSNull class]]){
		self.data = [[UserInfoData alloc] initWithDictionary:dictionary[@"data"]];
	}

	if(![dictionary[@"error_code"] isKindOfClass:[NSNull class]]){
		self.errorCode = [dictionary[@"error_code"] integerValue];
	}

	if(![dictionary[@"error_msg"] isKindOfClass:[NSNull class]]){
		self.errorMsg = dictionary[@"error_msg"];
	}	
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.data != nil){
		dictionary[@"data"] = [self.data toDictionary];
	}
	dictionary[@"error_code"] = @(self.errorCode);
	if(self.errorMsg != nil){
		dictionary[@"error_msg"] = self.errorMsg;
	}
	return dictionary;

}
@end