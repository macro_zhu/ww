//
//	UserFllowData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "UserFllowData.h"

@interface UserFllowData ()
@end
@implementation UserFllowData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"follow_list"] != nil && ![dictionary[@"follow_list"] isKindOfClass:[NSNull class]]){
		NSArray * followListDictionaries = dictionary[@"follow_list"];
		NSMutableArray * followListItems = [NSMutableArray array];
		for(NSDictionary * followListDictionary in followListDictionaries){
			UserFllowFollowList * followListItem = [[UserFllowFollowList alloc] initWithDictionary:followListDictionary];
			[followListItems addObject:followListItem];
		}
		self.followList = followListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.followList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(UserFllowFollowList * followListElement in self.followList){
			[dictionaryElements addObject:[followListElement toDictionary]];
		}
		dictionary[@"follow_list"] = dictionaryElements;
	}
	return dictionary;

}
@end