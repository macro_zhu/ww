#import <UIKit/UIKit.h>

@interface UserInfoData : NSObject

@property (nonatomic, strong) NSString * alias;
@property (nonatomic, assign) NSInteger articleNumber;
@property (nonatomic, strong) NSString * avatar;
@property (nonatomic, assign) NSInteger coinsGainedToday;
@property (nonatomic, assign) NSInteger coinsLimit;
@property (nonatomic, assign) NSInteger commentNumber;
@property (nonatomic, assign) NSInteger fansNumber;
@property (nonatomic, assign) NSInteger followNumber;
@property (nonatomic, assign) NSInteger gender;
@property (nonatomic, strong) NSString * petAge;
@property (nonatomic, strong) NSString * petArea;
@property (nonatomic, assign) NSInteger petAreaId;
@property (nonatomic, strong) NSString * petBirthday;
@property (nonatomic, strong) NSString * petType;
@property (nonatomic, assign) NSInteger petTypeId;
@property (nonatomic, strong) NSString * phone;
@property (nonatomic, assign) NSInteger praiseNumber;
@property (nonatomic, assign) NSInteger shareNumber;
@property (nonatomic, assign) NSInteger totalCoins;
@property (nonatomic, assign) NSInteger userId;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end