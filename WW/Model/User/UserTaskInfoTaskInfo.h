#import <UIKit/UIKit.h>

@interface UserTaskInfoTaskInfo : NSObject

@property (nonatomic, assign) NSInteger gainLimit;
@property (nonatomic, assign) NSInteger gained;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, assign) NSInteger reward;
@property (nonatomic, assign) NSInteger taskId;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end