//
//	UserTaskInfoTaskInfo.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "UserTaskInfoTaskInfo.h"

@interface UserTaskInfoTaskInfo ()
@end
@implementation UserTaskInfoTaskInfo




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"gain_limit"] isKindOfClass:[NSNull class]]){
		self.gainLimit = [dictionary[@"gain_limit"] integerValue];
	}

	if(![dictionary[@"gained"] isKindOfClass:[NSNull class]]){
		self.gained = [dictionary[@"gained"] integerValue];
	}

	if(![dictionary[@"name"] isKindOfClass:[NSNull class]]){
		self.name = dictionary[@"name"];
	}	
	if(![dictionary[@"reward"] isKindOfClass:[NSNull class]]){
		self.reward = [dictionary[@"reward"] integerValue];
	}

	if(![dictionary[@"task_id"] isKindOfClass:[NSNull class]]){
		self.taskId = [dictionary[@"task_id"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	dictionary[@"gain_limit"] = @(self.gainLimit);
	dictionary[@"gained"] = @(self.gained);
	if(self.name != nil){
		dictionary[@"name"] = self.name;
	}
	dictionary[@"reward"] = @(self.reward);
	dictionary[@"task_id"] = @(self.taskId);
	return dictionary;

}
@end