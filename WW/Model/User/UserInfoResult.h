#import <UIKit/UIKit.h>
#import "UserInfoData.h"

@interface UserInfoResult : NSObject

@property (nonatomic, strong) UserInfoData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end