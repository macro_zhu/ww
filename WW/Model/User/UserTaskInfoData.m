//
//	UserTaskInfoData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "UserTaskInfoData.h"

@interface UserTaskInfoData ()
@end
@implementation UserTaskInfoData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"avatar"] isKindOfClass:[NSNull class]]){
		self.avatar = dictionary[@"avatar"];
	}	
	if(![dictionary[@"coins_gained_today"] isKindOfClass:[NSNull class]]){
		self.coinsGainedToday = [dictionary[@"coins_gained_today"] integerValue];
	}

	if(![dictionary[@"coins_limit"] isKindOfClass:[NSNull class]]){
		self.coinsLimit = [dictionary[@"coins_limit"] integerValue];
	}

	if(dictionary[@"task_info"] != nil && ![dictionary[@"task_info"] isKindOfClass:[NSNull class]]){
		NSArray * taskInfoDictionaries = dictionary[@"task_info"];
		NSMutableArray * taskInfoItems = [NSMutableArray array];
		for(NSDictionary * taskInfoDictionary in taskInfoDictionaries){
			UserTaskInfoTaskInfo * taskInfoItem = [[UserTaskInfoTaskInfo alloc] initWithDictionary:taskInfoDictionary];
			[taskInfoItems addObject:taskInfoItem];
		}
		self.taskInfo = taskInfoItems;
	}
	if(![dictionary[@"total_coins"] isKindOfClass:[NSNull class]]){
		self.totalCoins = [dictionary[@"total_coins"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.avatar != nil){
		dictionary[@"avatar"] = self.avatar;
	}
	dictionary[@"coins_gained_today"] = @(self.coinsGainedToday);
	dictionary[@"coins_limit"] = @(self.coinsLimit);
	if(self.taskInfo != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(UserTaskInfoTaskInfo * taskInfoElement in self.taskInfo){
			[dictionaryElements addObject:[taskInfoElement toDictionary]];
		}
		dictionary[@"task_info"] = dictionaryElements;
	}
	dictionary[@"total_coins"] = @(self.totalCoins);
	return dictionary;

}
@end