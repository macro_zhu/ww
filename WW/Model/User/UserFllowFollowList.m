//
//	UserFllowFollowList.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "UserFllowFollowList.h"

@interface UserFllowFollowList ()
@end
@implementation UserFllowFollowList




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"alias"] isKindOfClass:[NSNull class]]){
		self.alias = dictionary[@"alias"];
	}	
	if(![dictionary[@"avatar"] isKindOfClass:[NSNull class]]){
		self.avatar = dictionary[@"avatar"];
	}	
	if(![dictionary[@"gender"] isKindOfClass:[NSNull class]]){
		self.gender = [dictionary[@"gender"] integerValue];
	}

	if(![dictionary[@"pet_age"] isKindOfClass:[NSNull class]]){
		self.petAge = dictionary[@"pet_age"];
	}	
	if(![dictionary[@"pet_area"] isKindOfClass:[NSNull class]]){
		self.petArea = dictionary[@"pet_area"];
	}	
	if(![dictionary[@"pet_area_id"] isKindOfClass:[NSNull class]]){
		self.petAreaId = [dictionary[@"pet_area_id"] integerValue];
	}

	if(![dictionary[@"pet_birthday"] isKindOfClass:[NSNull class]]){
		self.petBirthday = dictionary[@"pet_birthday"];
	}	
	if(![dictionary[@"pet_type"] isKindOfClass:[NSNull class]]){
		self.petType = dictionary[@"pet_type"];
	}	
	if(![dictionary[@"pet_type_id"] isKindOfClass:[NSNull class]]){
		self.petTypeId = [dictionary[@"pet_type_id"] integerValue];
	}

	if(![dictionary[@"phone"] isKindOfClass:[NSNull class]]){
		self.phone = dictionary[@"phone"];
	}	
	if(![dictionary[@"total_article"] isKindOfClass:[NSNull class]]){
		self.totalArticle = [dictionary[@"total_article"] integerValue];
	}

	if(![dictionary[@"user_id"] isKindOfClass:[NSNull class]]){
		self.userId = [dictionary[@"user_id"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.alias != nil){
		dictionary[@"alias"] = self.alias;
	}
	if(self.avatar != nil){
		dictionary[@"avatar"] = self.avatar;
	}
	dictionary[@"gender"] = @(self.gender);
	if(self.petAge != nil){
		dictionary[@"pet_age"] = self.petAge;
	}
	if(self.petArea != nil){
		dictionary[@"pet_area"] = self.petArea;
	}
	dictionary[@"pet_area_id"] = @(self.petAreaId);
	if(self.petBirthday != nil){
		dictionary[@"pet_birthday"] = self.petBirthday;
	}
	if(self.petType != nil){
		dictionary[@"pet_type"] = self.petType;
	}
	dictionary[@"pet_type_id"] = @(self.petTypeId);
	if(self.phone != nil){
		dictionary[@"phone"] = self.phone;
	}
	dictionary[@"total_article"] = @(self.totalArticle);
	dictionary[@"user_id"] = @(self.userId);
	return dictionary;

}
@end