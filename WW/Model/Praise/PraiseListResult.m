//
//	PraiseListResult.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "PraiseListResult.h"

@interface PraiseListResult ()
@end
@implementation PraiseListResult




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"data"] != nil && ![dictionary[@"data"] isKindOfClass:[NSNull class]]){
		NSArray * dataDictionaries = dictionary[@"data"];
		NSMutableArray * dataItems = [NSMutableArray array];
		for(NSDictionary * dataDictionary in dataDictionaries){
			PraiseListData * dataItem = [[PraiseListData alloc] initWithDictionary:dataDictionary];
			[dataItems addObject:dataItem];
		}
		self.data = dataItems;
	}
	if(![dictionary[@"error_code"] isKindOfClass:[NSNull class]]){
		self.errorCode = [dictionary[@"error_code"] integerValue];
	}

	if(![dictionary[@"error_msg"] isKindOfClass:[NSNull class]]){
		self.errorMsg = dictionary[@"error_msg"];
	}	
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.data != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(PraiseListData * dataElement in self.data){
			[dictionaryElements addObject:[dataElement toDictionary]];
		}
		dictionary[@"data"] = dictionaryElements;
	}
	dictionary[@"error_code"] = @(self.errorCode);
	if(self.errorMsg != nil){
		dictionary[@"error_msg"] = self.errorMsg;
	}
	return dictionary;

}
@end