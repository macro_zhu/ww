#import <UIKit/UIKit.h>
#import "PraiseListData.h"

@interface PraiseListResult : NSObject

@property (nonatomic, strong) NSArray * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end