#import <UIKit/UIKit.h>

@interface PraiseListData : NSObject

@property (nonatomic, strong) NSString * addDate;
@property (nonatomic, assign) NSInteger addTime;
@property (nonatomic, strong) NSString * alias;
@property (nonatomic, strong) NSString * avatar;
@property (nonatomic, assign) NSInteger praiseId;
@property (nonatomic, assign) NSInteger userId;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end