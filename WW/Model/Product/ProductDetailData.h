#import <UIKit/UIKit.h>

@interface ProductDetailData : NSObject

@property (nonatomic, strong) NSString * categoryName;
@property (nonatomic, strong) NSString * detailUrl;
@property (nonatomic, strong) NSArray * imageList;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) NSInteger productId;
@property (nonatomic, strong) NSString * productName;
@property (nonatomic, assign) NSInteger sales;
@property (nonatomic, strong) NSString * summary;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end