//
//	ProductDetailData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "ProductDetailData.h"

@interface ProductDetailData ()
@end
@implementation ProductDetailData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"category_name"] isKindOfClass:[NSNull class]]){
		self.categoryName = dictionary[@"category_name"];
	}	
	if(![dictionary[@"detail_url"] isKindOfClass:[NSNull class]]){
		self.detailUrl = dictionary[@"detail_url"];
	}	
	if(![dictionary[@"image_list"] isKindOfClass:[NSNull class]]){
		self.imageList = dictionary[@"image_list"];
	}	
	if(![dictionary[@"price"] isKindOfClass:[NSNull class]]){
		self.price = [dictionary[@"price"] integerValue];
	}

	if(![dictionary[@"product_id"] isKindOfClass:[NSNull class]]){
		self.productId = [dictionary[@"product_id"] integerValue];
	}

	if(![dictionary[@"product_name"] isKindOfClass:[NSNull class]]){
		self.productName = dictionary[@"product_name"];
	}	
	if(![dictionary[@"sales"] isKindOfClass:[NSNull class]]){
		self.sales = [dictionary[@"sales"] integerValue];
	}

	if(![dictionary[@"summary"] isKindOfClass:[NSNull class]]){
		self.summary = dictionary[@"summary"];
	}	
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.categoryName != nil){
		dictionary[@"category_name"] = self.categoryName;
	}
	if(self.detailUrl != nil){
		dictionary[@"detail_url"] = self.detailUrl;
	}
	if(self.imageList != nil){
		dictionary[@"image_list"] = self.imageList;
	}
	dictionary[@"price"] = @(self.price);
	dictionary[@"product_id"] = @(self.productId);
	if(self.productName != nil){
		dictionary[@"product_name"] = self.productName;
	}
	dictionary[@"sales"] = @(self.sales);
	if(self.summary != nil){
		dictionary[@"summary"] = self.summary;
	}
	return dictionary;

}
@end