//
//	ProductListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "ProductListData.h"

@interface ProductListData ()
@end
@implementation ProductListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"product_list"] != nil && ![dictionary[@"product_list"] isKindOfClass:[NSNull class]]){
		NSArray * productListDictionaries = dictionary[@"product_list"];
		NSMutableArray * productListItems = [NSMutableArray array];
		for(NSDictionary * productListDictionary in productListDictionaries){
			ProductList * productListItem = [[ProductList alloc] initWithDictionary:productListDictionary];
			[productListItems addObject:productListItem];
		}
		self.productList = productListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.productList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(ProductList * productListElement in self.productList){
			[dictionaryElements addObject:[productListElement toDictionary]];
		}
		dictionary[@"product_list"] = dictionaryElements;
	}
	return dictionary;

}
@end