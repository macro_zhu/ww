#import <UIKit/UIKit.h>
#import "ProductDetailData.h"

@interface ProductDetailResult : NSObject

@property (nonatomic, strong) ProductDetailData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end