#import <UIKit/UIKit.h>
#import "ProductList.h"

@interface ProductListData : NSObject

@property (nonatomic, strong) NSArray * productList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end