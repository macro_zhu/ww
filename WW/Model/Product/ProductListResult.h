#import <UIKit/UIKit.h>
#import "ProductListData.h"

@interface ProductListResult : NSObject

@property (nonatomic, strong) ProductListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end