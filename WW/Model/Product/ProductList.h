#import <UIKit/UIKit.h>

@interface ProductList : NSObject

@property (nonatomic, strong) NSString * categoryName;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) NSInteger productId;
@property (nonatomic, strong) NSString * productName;
@property (nonatomic, strong) NSString * summary;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end