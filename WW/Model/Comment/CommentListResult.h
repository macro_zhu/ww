#import <UIKit/UIKit.h>
#import "CommentListData.h"

@interface CommentListResult : NSObject

@property (nonatomic, strong) CommentListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end