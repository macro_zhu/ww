//
//	CommentListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "CommentListData.h"

@interface CommentListData ()
@end
@implementation CommentListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"comment_list"] != nil && ![dictionary[@"comment_list"] isKindOfClass:[NSNull class]]){
		NSArray * commentListDictionaries = dictionary[@"comment_list"];
		NSMutableArray * commentListItems = [NSMutableArray array];
		for(NSDictionary * commentListDictionary in commentListDictionaries){
			CommentList * commentListItem = [[CommentList alloc] initWithDictionary:commentListDictionary];
			[commentListItems addObject:commentListItem];
		}
		self.commentList = commentListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.commentList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(CommentList * commentListElement in self.commentList){
			[dictionaryElements addObject:[commentListElement toDictionary]];
		}
		dictionary[@"comment_list"] = dictionaryElements;
	}
	return dictionary;

}
@end