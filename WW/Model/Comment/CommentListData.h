#import <UIKit/UIKit.h>
#import "CommentList.h"

@interface CommentListData : NSObject

@property (nonatomic, strong) NSArray * commentList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end