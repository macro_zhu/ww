//
//	CommentList.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "CommentList.h"

@interface CommentList ()
@end
@implementation CommentList




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"add_date"] isKindOfClass:[NSNull class]]){
		self.addDate = dictionary[@"add_date"];
	}	
	if(![dictionary[@"add_time"] isKindOfClass:[NSNull class]]){
		self.addTime = [dictionary[@"add_time"] integerValue];
	}

	if(![dictionary[@"alias"] isKindOfClass:[NSNull class]]){
		self.alias = dictionary[@"alias"];
	}	
	if(![dictionary[@"avatar"] isKindOfClass:[NSNull class]]){
		self.avatar = dictionary[@"avatar"];
	}	
	if(![dictionary[@"comment_id"] isKindOfClass:[NSNull class]]){
		self.commentId = [dictionary[@"comment_id"] integerValue];
	}

	if(![dictionary[@"content"] isKindOfClass:[NSNull class]]){
		self.content = dictionary[@"content"];
	}	
	if(![dictionary[@"user_id"] isKindOfClass:[NSNull class]]){
		self.userId = [dictionary[@"user_id"] integerValue];
	}

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.addDate != nil){
		dictionary[@"add_date"] = self.addDate;
	}
	dictionary[@"add_time"] = @(self.addTime);
	if(self.alias != nil){
		dictionary[@"alias"] = self.alias;
	}
	if(self.avatar != nil){
		dictionary[@"avatar"] = self.avatar;
	}
	dictionary[@"comment_id"] = @(self.commentId);
	if(self.content != nil){
		dictionary[@"content"] = self.content;
	}
	dictionary[@"user_id"] = @(self.userId);
	return dictionary;

}
@end