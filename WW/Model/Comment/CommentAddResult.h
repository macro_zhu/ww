#import <UIKit/UIKit.h>
#import "CommentAddData.h"

@interface CommentAddResult : NSObject

@property (nonatomic, strong) CommentAddData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end