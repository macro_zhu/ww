#import <UIKit/UIKit.h>
#import "AddressGetProductListAddressList.h"

@interface AddressGetProductListData : NSObject

@property (nonatomic, strong) NSArray * addressList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end