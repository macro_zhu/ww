#import <UIKit/UIKit.h>
#import "AddressGetProductListData.h"

@interface AddressGetProductListResult : NSObject

@property (nonatomic, strong) AddressGetProductListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end