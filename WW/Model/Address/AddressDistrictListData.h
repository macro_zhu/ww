#import <UIKit/UIKit.h>
#import "AddressDistrictListRegionList.h"

@interface AddressDistrictListData : NSObject

@property (nonatomic, strong) NSArray * regionList;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end