#import <UIKit/UIKit.h>

@interface AddressDistrictListRegionList : NSObject

@property (nonatomic, assign) NSInteger regionId;
@property (nonatomic, strong) NSString * regionName;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end