#import <UIKit/UIKit.h>

@interface AddressGetProductListAddressList : NSObject

@property (nonatomic, strong) NSString * address;
@property (nonatomic, assign) NSInteger addressId;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, assign) NSInteger cityId;
@property (nonatomic, strong) NSString * consignee;
@property (nonatomic, strong) NSString * district;
@property (nonatomic, assign) NSInteger districtId;
@property (nonatomic, strong) NSString * phone;
@property (nonatomic, strong) NSString * province;
@property (nonatomic, assign) NSInteger provinceId;
@property (nonatomic, assign) BOOL isDefault;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end