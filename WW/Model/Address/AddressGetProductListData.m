//
//	AddressGetProductListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "AddressGetProductListData.h"

@interface AddressGetProductListData ()
@end
@implementation AddressGetProductListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"address_list"] != nil && ![dictionary[@"address_list"] isKindOfClass:[NSNull class]]){
		NSArray * addressListDictionaries = dictionary[@"address_list"];
		NSMutableArray * addressListItems = [NSMutableArray array];
		for(NSDictionary * addressListDictionary in addressListDictionaries){
			AddressGetProductListAddressList * addressListItem = [[AddressGetProductListAddressList alloc] initWithDictionary:addressListDictionary];
			[addressListItems addObject:addressListItem];
		}
		self.addressList = addressListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.addressList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(AddressGetProductListAddressList * addressListElement in self.addressList){
			[dictionaryElements addObject:[addressListElement toDictionary]];
		}
		dictionary[@"address_list"] = dictionaryElements;
	}
	return dictionary;

}
@end