//
//	AddressDistrictListData.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "AddressDistrictListData.h"

@interface AddressDistrictListData ()
@end
@implementation AddressDistrictListData




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"region_list"] != nil && ![dictionary[@"region_list"] isKindOfClass:[NSNull class]]){
		NSArray * regionListDictionaries = dictionary[@"region_list"];
		NSMutableArray * regionListItems = [NSMutableArray array];
		for(NSDictionary * regionListDictionary in regionListDictionaries){
			AddressDistrictListRegionList * regionListItem = [[AddressDistrictListRegionList alloc] initWithDictionary:regionListDictionary];
			[regionListItems addObject:regionListItem];
		}
		self.regionList = regionListItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.regionList != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(AddressDistrictListRegionList * regionListElement in self.regionList){
			[dictionaryElements addObject:[regionListElement toDictionary]];
		}
		dictionary[@"region_list"] = dictionaryElements;
	}
	return dictionary;

}
@end