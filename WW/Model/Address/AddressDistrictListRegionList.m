//
//	AddressDistrictListRegionList.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "AddressDistrictListRegionList.h"

@interface AddressDistrictListRegionList ()
@end
@implementation AddressDistrictListRegionList




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"region_id"] isKindOfClass:[NSNull class]]){
		self.regionId = [dictionary[@"region_id"] integerValue];
	}

	if(![dictionary[@"region_name"] isKindOfClass:[NSNull class]]){
		self.regionName = dictionary[@"region_name"];
	}	
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	dictionary[@"region_id"] = @(self.regionId);
	if(self.regionName != nil){
		dictionary[@"region_name"] = self.regionName;
	}
	return dictionary;

}
@end