#import <UIKit/UIKit.h>
#import "AddressDistrictListData.h"

@interface AddressDistrictListResult : NSObject

@property (nonatomic, strong) AddressDistrictListData * data;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong) NSString * errorMsg;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end