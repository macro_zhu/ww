//
//	AddressGetProductListAddressList.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "AddressGetProductListAddressList.h"

@interface AddressGetProductListAddressList ()
@end
@implementation AddressGetProductListAddressList




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"address"] isKindOfClass:[NSNull class]]){
		self.address = dictionary[@"address"];
	}	
	if(![dictionary[@"address_id"] isKindOfClass:[NSNull class]]){
		self.addressId = [dictionary[@"address_id"] integerValue];
	}

	if(![dictionary[@"city"] isKindOfClass:[NSNull class]]){
		self.city = dictionary[@"city"];
	}	
	if(![dictionary[@"city_id"] isKindOfClass:[NSNull class]]){
		self.cityId = [dictionary[@"city_id"] integerValue];
	}

	if(![dictionary[@"consignee"] isKindOfClass:[NSNull class]]){
		self.consignee = dictionary[@"consignee"];
	}	
	if(![dictionary[@"district"] isKindOfClass:[NSNull class]]){
		self.district = dictionary[@"district"];
	}	
	if(![dictionary[@"district_id"] isKindOfClass:[NSNull class]]){
		self.districtId = [dictionary[@"district_id"] integerValue];
	}

	if(![dictionary[@"phone"] isKindOfClass:[NSNull class]]){
		self.phone = dictionary[@"phone"];
	}	
	if(![dictionary[@"province"] isKindOfClass:[NSNull class]]){
		self.province = dictionary[@"province"];
	}	
	if(![dictionary[@"province_id"] isKindOfClass:[NSNull class]]){
		self.provinceId = [dictionary[@"province_id"] integerValue];
	}
    if(![dictionary[@"isdefault"] isKindOfClass:[NSNull class]]) {
        self.isDefault = [dictionary[@"isdefault"] isEqualToString:@"1"];
    }
    
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.address != nil){
		dictionary[@"address"] = self.address;
	}
	dictionary[@"address_id"] = @(self.addressId);
	if(self.city != nil){
		dictionary[@"city"] = self.city;
	}
	dictionary[@"city_id"] = @(self.cityId);
	if(self.consignee != nil){
		dictionary[@"consignee"] = self.consignee;
	}
	if(self.district != nil){
		dictionary[@"district"] = self.district;
	}
	dictionary[@"district_id"] = @(self.districtId);
	if(self.phone != nil){
		dictionary[@"phone"] = self.phone;
	}
	if(self.province != nil){
		dictionary[@"province"] = self.province;
	}
	dictionary[@"province_id"] = @(self.provinceId);
    
    dictionary[@"isdefault"] = self.isDefault?@"1":@"0";
	return dictionary;

}
@end