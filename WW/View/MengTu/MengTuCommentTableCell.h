//
//  MengTuCommentTableCell.h
//  WW
//
//  Created by Macro on 15/6/5.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MengTuCommentTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *headBtn;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *datelbl;
@property (strong, nonatomic) IBOutlet UILabel *infoLbl;

@end
