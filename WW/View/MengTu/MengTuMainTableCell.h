//
//  MengTuMainTableCell.h
//  WW
//
//  Created by Macro on 15/6/4.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MengTuMainTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UIImageView *sexImg;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *infoLbl;
@property (strong, nonatomic) IBOutlet UIButton *attentionBtn;
@property (strong, nonatomic) IBOutlet UIImageView *mainImg;
@property (strong, nonatomic) IBOutlet UILabel *contentLbl;


@property (strong, nonatomic) IBOutlet UIButton *shareBtn;
@property (strong, nonatomic) IBOutlet UILabel *timeReplyLbl;
@property (strong, nonatomic) IBOutlet UIButton *addWishBtn;
@property (strong, nonatomic) IBOutlet UIButton *commentBtn;
@property (strong, nonatomic) IBOutlet UIButton *peopleLooksBtn;

@property (strong, nonatomic) IBOutlet UIButton *user1HeadImg;
@property (strong, nonatomic) IBOutlet UITextField *commentTF;

@property (strong, nonatomic) IBOutlet UIButton *commenterHeadImg;
@property (strong, nonatomic) IBOutlet UILabel *commenterNamelbl;
@property (strong, nonatomic) IBOutlet UILabel *commenterDataLbl;
@property (strong, nonatomic) IBOutlet UILabel *commenterCommentLbl;
@property (strong, nonatomic) IBOutlet UIButton *lookForAllCommentBtn;


@end
