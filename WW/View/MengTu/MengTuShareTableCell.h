//
//  MengTuShareTableCell.h
//  WW
//
//  Created by Macro on 15/6/5.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MengTuShareTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *headBtn;
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *shareInfoLbl;

@end
