//
//  DistrictListTableCell.h
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistrictListDefaultTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *phoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *districtlbl;
@property (strong, nonatomic) IBOutlet UIImageView *defaultImg;

@end
