//
//  AttentionAndFansTableCell.h
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttentionAndFansTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UIImageView *sexImg;
@property (strong, nonatomic) IBOutlet UILabel *districtLbl;
@property (strong, nonatomic) IBOutlet UILabel *mengTuCountLbl;
@property (strong, nonatomic) IBOutlet UILabel *gougouClassifyLbl;
@property (strong, nonatomic) IBOutlet UILabel *gougouAgeLbl;
@property (strong, nonatomic) IBOutlet UIButton *attentionBtn;

@end
