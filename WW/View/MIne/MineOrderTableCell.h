//
//  MineOrderTableCell.h
//  WW
//
//  Created by Macro on 15/6/12.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineOrderTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *orderStatuLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderTimeLbl;
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UIImageView *shoppingImg;
@property (strong, nonatomic) IBOutlet UILabel *shoppingNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingInfoLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingNumLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppingCountLbl;

@end
