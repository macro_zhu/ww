//
//  MineMainTableCell.h
//  WW
//
//  Created by Macro on 15/6/9.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineMainTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconImg;
@property (strong, nonatomic) IBOutlet UILabel *infoLbl;

@end
