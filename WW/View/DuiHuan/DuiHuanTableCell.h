//
//  DuiHuanTableCell.h
//  WW
//
//  Created by Macro on 15/6/8.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DuiHuanTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
//商品分类列表
@property (strong, nonatomic) IBOutlet UILabel *classifyTitleLbl;
//商品名称
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *shoppinginfoLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;

@end
