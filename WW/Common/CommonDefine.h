//
//  CommonDefine.h
//  ASYC
//
//  Created by 朱健 on 15/4/27.
//  Copyright (c) 2015年 macro. All rights reserved.
//


#ifndef ASYC_CommonDefine_h
#define ASYC_CommonDefine_h

/**
 *  基本块宏名义
 */
//当滑动范围超过一定的限定时 调用
typedef void(^ScrollAheadBlock)(void);

//响应按钮点击回调
typedef void(^ResponseToUserClick_Btn_Tag)(int tag);
typedef void(^ResponseToUserClick_Btn) (UIButton *btn);

/**
 *  常见常量宏定义
 */
#define kScrollAheadMax   30
//定义弱引用
#define WEAKSELF typeof(self) __weak weakSelf = self;

/**
 *  基本页面属性宏定义
 */
#define kWindowWidth ([UIScreen mainScreen].bounds.size.width)
#define kWindowHight ([UIScreen mainScreen].bounds.size.height)

#define kViewControllerHeight   self.view.frame.size.height
#define kViewControllerWidth    self.view.frame.size.width
#define kViewHeight             self.frame.size.height
#define kViewWidth              self.frame.size.width
#define kViewRate               self.frame.size.width/320
#define kViewControllerRate     self.view.frame.size.width/320

//定义debug情况下的日志打印
#ifdef DEBUG
#define DLog( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DLog( s, ... )
#endif

#define kDebugNSlog   NSLog(@"Runing %@   '%@'",self.class,NSStringFromSelector(_cmd));

#define ErrorLog( s,...) kDebugNSlog; NSLog( @"<%p %@:(%d)> %@",self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )

/**
 *  常用的枚举
 */
typedef  NS_ENUM(NSInteger, kSessionType){
    kSessionSpring = 0,
    kSessionSummer,
    kSessionAutumn,
    kSessionWinter
};

#endif
