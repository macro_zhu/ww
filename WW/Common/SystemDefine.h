//
//  SystemDefine.h
//  ASYC
//
//  Created by 朱健 on 15/4/28.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#ifndef ASYC_SystemDefine_h
#define ASYC_SystemDefine_h

#define IS_IOS8  [UIDevice currentDevice].systemVersion.floatValue >=8.0

#endif
