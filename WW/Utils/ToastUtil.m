//
//  ToastUtil.m
//  ASYC
//
//  Created by Macro on 15/5/19.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "ToastUtil.h"
#import "UIProvideToast.h"
#import "UIView+Toast.h"

@implementation ToastUtil

+ (void)showToast:(NSString *)aStr{
    if (nil == aStr || [aStr length] == 0) {
        return;
    }

    kDebugNSlog
    
    UIProvideToast *toast = [[UIProvideToast alloc] init];
    [toast show:aStr];
}

+ (void)showToastImage:(NSString*)imageName{

}

@end
