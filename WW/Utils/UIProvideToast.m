//
//  UIProvideToast.h
//  CaiBaObject
//
//  Created by ximenwuyou on 14-8-21.
//  Copyright (c) 2014年 ximenwuyou. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIProvideToast.h"

#define WIDTH_INTERVAL_FROM_IMAGE 14
#define HEIGHT_INTERVAL_FROM_IMAGE 16
#define TOAST_BOUNDS CGSizeMake(28, 32)
#define TOAST_TEXT_WIDTH 260
#define TEXT_FONT [UIFont fontWithName:@"HelveticaNeue" size:16.0]
#define VIEW_BACKGROUND_COLOR [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.6f]

#define TOAST_VIEW_TAG 123456789
#define UILINEBREAKMODEWORDWRAP UILineBreakModeWordWrap
// 将通知回调切换到主线程
#define CHANGE_NOTIFICATION_CALLBACK_TO_MAINTHREAD(notification) \
if ([NSThread currentThread] != [NSThread mainThread])\
{\
return [self performSelectorOnMainThread:_cmd withObject:notification waitUntilDone:NO];\
}


@implementation UIProvideToast

- (id)init{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = NO;
        self.tag = TOAST_VIEW_TAG;
    }
    return self;
}

- (void)clearVisualToast{
    UIWindow *rootView = [UIApplication sharedApplication].keyWindow;
    UIView *toastView = [rootView viewWithTag:TOAST_VIEW_TAG];
    if(nil != toastView){
        [toastView setHidden:TRUE];
        NSInteger vLength = [toastView.subviews count];
        while (vLength-- > 0)
        {
            UIView *v = [toastView.subviews objectAtIndex:vLength];
            [v removeFromSuperview];
        }
        [toastView removeFromSuperview];
    }
}


//开始动画
- (void)beginAnimationWithDuration:(CGFloat)duTime{
	[UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:duTime];
}

//结束动画
- (void)commitModalAnimationsWithSelector:(SEL)seletor
{
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:seletor];
	[UIView commitAnimations];
}

- (void)delayShow{
    UIWindow *rootView = [UIApplication sharedApplication].keyWindow;
    
    [rootView addSubview:self];
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:1.5];
}

// 在屏幕的中间显示toast提示
- (void)show:(NSString*)content
{
    //必须在主线程
    CHANGE_NOTIFICATION_CALLBACK_TO_MAINTHREAD(content);
    // 清除已经显示的Toast
    [self clearVisualToast];
    
    animation = 0;
    
    UIWindow *rootView = [UIApplication sharedApplication].keyWindow;
    CGRect rect = rootView.screen.bounds;
    
    CGSize labFrame = [content boundingRectWithSize:CGSizeMake(TOAST_TEXT_WIDTH, 400) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:[NSDictionary dictionaryWithObjectsAndKeys:TEXT_FONT,NSFontAttributeName, nil] context:nil].size;
    CGSize size = TOAST_BOUNDS;
    labFrame.width += size.width;
	labFrame.height += size.height + 5;
    
    //设置view边框
    self.frame = CGRectMake((rect.size.width-labFrame.width)/2, (rect.size.height-labFrame.height)/2, 
                            labFrame.width, labFrame.height);
    self.layer.cornerRadius = 6;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.8f;
//    self.layer.shadowRadius = 8.0f;
    self.layer.shadowOffset = CGSizeMake(2, 2);
    self.backgroundColor = VIEW_BACKGROUND_COLOR;
    
    //设置文字输入框
    UILabel *labelTip = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH_INTERVAL_FROM_IMAGE, HEIGHT_INTERVAL_FROM_IMAGE, 
                                                                  labFrame.width - 2 * WIDTH_INTERVAL_FROM_IMAGE, 
                                                                  labFrame.height - 2 * HEIGHT_INTERVAL_FROM_IMAGE)];        
    labelTip.textAlignment = NSTextAlignmentCenter;
    labelTip.backgroundColor = [UIColor clearColor];
    labelTip.textColor = [UIColor whiteColor];
    labelTip.text = content;
    labelTip.font = TEXT_FONT;
    labelTip.lineBreakMode = NSLineBreakByCharWrapping;
    labelTip.numberOfLines = 0;
    [self addSubview:labelTip];
    
    [rootView addSubview:self];    
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:1.5];
}

// 在屏幕的任意高度显示toast提示
- (void)show:(NSString*)content withHeight:(float)aHeight
{
    // 清除已经显示的Toast
    [self clearVisualToast];
    
    animation = 0;
    
    UIWindow *rootView = [UIApplication sharedApplication].keyWindow;;
    CGRect rect = rootView.screen.bounds;
    
    CGSize labFrame = [content boundingRectWithSize:CGSizeMake(TOAST_TEXT_WIDTH, 400) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:[NSDictionary dictionaryWithObjectsAndKeys:TEXT_FONT,NSFontAttributeName, nil] context:nil].size;
    
    CGSize size = TOAST_BOUNDS;
    labFrame.width += size.width;
	labFrame.height += size.height + 5;
    
    //设置view边框
    self.frame = CGRectMake((rect.size.width-labFrame.width)/2, aHeight, 
                            labFrame.width, labFrame.height);
    self.layer.cornerRadius = 6;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.8f;
//    self.layer.shadowRadius = 8.0f;
    self.layer.shadowOffset = CGSizeMake(2, 2);
    self.backgroundColor = VIEW_BACKGROUND_COLOR;
    
    //设置文字输入框
    UILabel *labelTip = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH_INTERVAL_FROM_IMAGE, HEIGHT_INTERVAL_FROM_IMAGE, 
                                                                  labFrame.width - 2 * WIDTH_INTERVAL_FROM_IMAGE, 
                                                                  labFrame.height - 2 * HEIGHT_INTERVAL_FROM_IMAGE)];        
    labelTip.textAlignment = NSTextAlignmentCenter;
    labelTip.backgroundColor = [UIColor clearColor];
    labelTip.textColor = [UIColor whiteColor];
    labelTip.text = content;
    labelTip.font = TEXT_FONT;
    labelTip.lineBreakMode = NSLineBreakByCharWrapping;
    labelTip.numberOfLines = 0;
    [self addSubview:labelTip];
    [rootView addSubview:self];    
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:1.5];
}

//隐藏窗口
- (void)dismiss
{
    switch (animation++) 
    {
        case 0:
        {
            [self beginAnimationWithDuration:0.5];
            self.alpha = 0.1f;
			[self commitModalAnimationsWithSelector:@selector(dismiss)];
            break;
        }
        case 1:
        {
            NSInteger vLength = [self.subviews count];
            while (vLength-->0) 
            {
                UIView *v = [self.subviews objectAtIndex:vLength];
                [v removeFromSuperview];
            }
            [self removeFromSuperview];
            break;
        }
        default:
			break;
	}
}
@end
