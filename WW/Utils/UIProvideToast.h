//
//  UIProvideToast.h
//  CaiBaObject
//
//  Created by ximenwuyou on 14-8-21.
//  Copyright (c) 2014年 ximenwuyou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIProvideToast : UIView
{
    NSInteger animation;
}

//快速显示toast
-(void)show:(NSString*)content;

// 在屏幕的任意高度显示toast提示
- (void)show:(NSString*)content withHeight:(float)aHeight;

@end
