//
//  TimerUtil.h
//  ASYC
//
//  Created by Macro on 15/6/1.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimerUtil : NSObject

/**
 *  将当前时间转换成时间戳
 *
 *  @return 时间戳
 */
+(NSString *)changeLocalDateToTimeStmp;

@end
