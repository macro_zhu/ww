//
//  TabbarUtil.h
//  ASYC
//
//  Created by 朱健 on 15/5/11.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TabbarUtil : NSObject

+(id)shareManager;

+(void)isTabbarHide:(BOOL)isHide;

+(void)isTabbarUserEnable:(BOOL)isEnable;

+(void)changeViewControllerWithTabbarIndex:(NSInteger)index;

@end
