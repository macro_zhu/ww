//
//  MBProgressHuDUtil.m
//  ASYC
//
//  Created by Macro on 15/5/31.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "MBProgressHuDUtil.h"

@implementation MBProgressHuDUtil

+ (void)showHUD:(NSString *)text andView:(UIView *)view andHUD:(MBProgressHUD *)hud
{
    [view addSubview:hud];
    hud.labelText = text;//显示提示
    hud.dimBackground = NO;//使背景成黑灰色，让MBProgressHUD成高亮显示
    hud.square = YES;//设置显示框的高度和宽度一样
    [hud show:YES];
}

@end
