//
//  TabbarUtil.m
//  ASYC
//
//  Created by 朱健 on 15/5/11.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "TabbarUtil.h"
#import "CustomTabBarController.h"
#import "AppDelegate.h"


static TabbarUtil *tabbarUtil = nil;
@interface TabbarUtil ()

@property (nonatomic,strong) CustomTabBarController *tabbar;

@end

@implementation TabbarUtil

+(id)shareManager{
    static  dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tabbarUtil = [[TabbarUtil alloc] init];
    });
    
    return tabbarUtil;
}

+(void)isTabbarHide:(BOOL)isHide{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.tabbarCtrl isHideCustomTabbar:isHide];
//    tabbarUtil.tabbar.tabbar.hidden = YES;
}

+(void)isTabbarUserEnable:(BOOL)isEnable{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.tabbarCtrl isHideTabbarUserEnable:isEnable];
}

+(void)changeViewControllerWithTabbarIndex:(NSInteger)index{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    delegate.window.rootViewController = delegate.tabbarCtrl;
    [delegate.tabbarCtrl changeViewControllerWithIndex:index];

}

@end
