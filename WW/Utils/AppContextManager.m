//
//  AppContextManager.m
//  ASYC
//
//  Created by 朱健 on 15/5/11.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "AppContextManager.h"
#import "AppDelegate.h"

static AppContextManager *manager = nil;
@implementation AppContextManager
@synthesize headData;

+(instancetype)shareManager{
  static  dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AppContextManager alloc] init];
    });
    
    return manager;
}

- (NSData*)headData{
  NSData *picData = [UserDefaultsUtils valueWithKey:@"HeadPicData"];
    return picData;
}

- (void)setHeadData:(NSData *)head{
    if (head == nil) {
        return;
    }
    [UserDefaultsUtils saveValue:head forKey:@"HeadPicData"];
    headData = head;
}

@end
