//
//  AppContextManager.h
//  ASYC
//
//  Created by 朱健 on 15/5/11.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppContextManager : NSObject

@property (nonatomic,strong) NSData *headData;

+(instancetype)shareManager;

@end
