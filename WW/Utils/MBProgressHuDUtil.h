//
//  MBProgressHuDUtil.h
//  ASYC
//
//  Created by Macro on 15/5/31.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MBProgressHUD.h>

@interface MBProgressHuDUtil : NSObject

+ (void)showHUD:(NSString *)text andView:(UIView *)view andHUD:(MBProgressHUD *)hud;

@end
