//
//  QiNiuUtils.m
//  ASYC
//
//  Created by Macro on 15/6/2.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "QiNiuUtils.h"

static QiNiuUtils *qiniuInstance = nil;
@implementation QiNiuUtils

+(id)share{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        qiniuInstance = [[QiNiuUtils alloc] init];
    });
    return qiniuInstance;
}

@end
