//
//  ToastUtil.h
//  ASYC
//
//  Created by Macro on 15/5/19.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToastUtil : NSObject

// 展示Toast提示
+ (void)showToast:(NSString *)aStr;

+ (void)showToastImage:(NSString*)imageName;

@end
