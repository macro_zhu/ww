//
//  AppNetService.h
//  ASYC
//
//  Created by Macro on 15/5/19.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RWSignInResponse)(BOOL);

typedef void(^ComeBackBlock)(BOOL,id);

@interface AppNetService : NSObject

//创建单例
+(id)shareManager;

/**********************************兑换***************************/

/**
 *  商品兑换
 *
 *  @param parameters token,address_id地址id，product_id 商品id
 *  @param block      请求回调
 */
+ (void)getExchangeService:(id)parameters complete:(ComeBackBlock)block;

/**
 *  兑换列表
 *
 *  @param parameters @{token,page_no,page_num}
 *  @param block      请求回调
 */
+ (void)getExchangedListService:(id)parameters complete:(ComeBackBlock)block;

/**
 *  兑换详情
 *
 *  @param paramters @{token,exchange_id}
 *  @param block     请求回调
 */
+ (void)getExchangeDetailService:(id)paramters complete:(ComeBackBlock)block;


/**********************************商品***************************/

/**
 *  商品列表
 *
 *  @param paramters @{token,page_no,page_num}
 *  @param block     请求回调
 */
+ (void)getProductListService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  商品详情
 *
 *  @param paramters @{token,product_id}
 *  @param block     请求回调
 */
+ (void)getproductDetailService:(id)paramters complete:(ComeBackBlock)block;


/**********************************图片***************************/

/**
 *  发表图片
 *
 *  @param paramters @{token,image_id,content,@[tags..],city}
 *  @param block     请求回调
 */
+ (void)getPicturePublishService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  图片列表
 *
 *  @param paramters @{token,type,page_no,page_num}
 *  @param block     请求回调
 */
+ (void)getPictureListService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  图片详情
 *
 *  @param paramters @{token,article_id}
 *  @param block     请求回调
 */
+ (void)getPictureDetailService:(id)paramters complete:(ComeBackBlock)block;


/**********************************评论***************************/

/**
 *  添加评论
 *
 *  @param paramters @{token,article_id,comment}
 *  @param block     请求回调
 */
+ (void)getCommentAddService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  评论列表
 *
 *  @param paramters @{article_id}
 *  @param block     请求回调
 */
+ (void)getCommentListService:(id)paramters complete:(ComeBackBlock)block;


/**********************************分享***************************/
/**
 *  添加分享
 *
 *  @param paramters @{token,article_id,type}
 *  @param block     请求回调
 */
+ (void)getShareAddService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  分享列表
 *
 *  @param paramters @{article_id,page_no,page_num}
 *  @param block     请求回调
 */
+ (void)getShareListService:(id)paramters complete:(ComeBackBlock)block;


/**********************************点赞***************************/

/**
 *  点赞
 *
 *  @param paramters @{token,article_id}
 *  @param block     请求回调
 */
+ (void)getPraiseAddService:(id)paramters complete:(ComeBackBlock)block;


/**
 *  点赞列表
 *
 *  @param paramters @{article_id,page_no,page_num}
 *  @param block     请求回调
 */
+ (void)getPraiseListService:(id)paramters complete:(ComeBackBlock)block;


/**********************************地址***************************/
/**
 *  地区列表
 *
 *  @param paramters @{region_id}
 *  @param block      请求回调
 */
+ (void)getAddressDistrictListService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  添加收货地址
 *
 *  @param paramters @{token,consignee,phone,provice_id,city_id,distirct_id,address}
 *  @param block     请求回调
 */
+ (void)getAddressAddService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  删除收货地址
 *
 *  @param paramters @{token,address_id}
 *  @param block      请求回调
 */
+ (void)getAddressDeleteService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  修改收货地址
 *
 *  @param paramters @{token,address_id,configness,phone,province_id,city_id,district_id,address}
 *  @param block     请求回调
 */
+ (void)getAddressChangeService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  收货地址列表
 *
 *  @param paramters @{token}
 *  @param block     请求回调
 */
+ (void)getAddressListService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  设置默认收货地址
 *
 *  @param paramters @{token,address_id}
 *  @param block     请求回调
 */
+ (void)setDefaultAddressService:(id)paramters complete:(ComeBackBlock)block;
/**********************************用户-注册***************************/

/**
 *  发送验证码
 *
 *  @param paramters @{phone}
 *  @param block     请求回调
 */
+ (void)getRegisterSendVerifyService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  验证验证码并注册
 *
 *  @param paramters @{phone,password,verify}
 *  @param block     请求回调
 */
+ (void)getRegisterRegiste:(id)paramters complete:(ComeBackBlock)block;


/**********************************用户-登录***************************/
/**
 *  普通方式登录
 *
 *  @param paramters @{user_name,password}
 *  @param block     请求回调
 */
+ (void)getLoginCommonLoginService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  token登录
 *
 *  @param paramters @{token}
 *  @param block     请求回调
 */
+ (void)getLoginTokenLoginService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  QQ登录
 *
 *  @param paramters @{access_token,openid}
 *  @param block     请求回调
 */
+ (void)getLoginQQLoginService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  微信登录
 *
 *  @param paramters @{code}
 *  @param block     请求回调
 */
+ (void)getLoginWeChatLoginService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  第三方登录
 *
 *  @param paramters @{id,photo,nick}
 *  @param block     请求回调
 */
+ (void)getThirdLoginService:(id)paramters complete:(ComeBackBlock)block;

/**********************************用户-信息***************************/
/**
 *  获取用户信息
 *
 *  @param paramters @{token}
 *  @param block     请求回调
 */
+ (void)getUserMessageService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  修改用户信息
 *
 *  @param paramters @{token,img_id,alias,phone,gender,pet_type_id,pet_birthday,pet_area_id}
 *  @param block     请求回调
 */
+ (void)getUserMessageChangeService:(id)paramters complete:(ComeBackBlock)block;


/**********************************用户-宠物***************************/
/**
 *  获取宠物类型
 *
 *  @param paramters @{}
 *  @param block     请求回调
 */
+ (void)getPetTypeService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  获取宠物地区
 *
 *  @param paramters @{}
 *  @param block     请求回调
 */
+ (void)getPetDistrictsService:(id)paramters complete:(ComeBackBlock)block;

/**********************************用户-其它***************************/
/**
 *  上传图片
 *
 *  @param paramters @{token,photo(NSData＊)}
 *  @param block     请求回调
 */
+ (void)getUserUploadPictureService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  修改密码
 *
 *  @param paramters @{token,old_password,new_password}
 *  @param block     请求回调
 */
+ (void)getuserChangePwdService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  添加／取消关注
 *
 *  @param paramters @{token,user_id}
 *  @param block     请求回调
 */
+ (void)getUserPayOrCancelAttentionService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  关注列表
 *
 *  @param paramters @{token,page_no,page_num}
 *  @param block     请求回调
 */
+ (void)getUserPayListService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  粉丝列表
 *
 *  @param paramters @{token,page_no,page_num}
 *  @param block     请求回调
 */
+ (void)getUserFansListService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  任务信息
 *
 *  @param paramters @{token}
 *  @param block     请求回调
 */
+ (void)getUserTaskMessageService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  个人主页
 *
 *  @param paramters @{token}
 *  @param block     请求回调
 */
+ (void)getuserMainHoneService:(id)paramters complete:(ComeBackBlock)block;

//**********************************密码***************************/
/**
 *  发送验证码
 *
 *  @param paramters @{user_name}
 *  @param block     请求回调
 */
+ (void)getPwdSendVerifyService:(id)paramters complete:(ComeBackBlock)block;

/**
 *  验证并重置密码
 *
 *  @param paramters @{user_name,password,verify}
 *  @param block     请求回调
 */
+ (void)getPwdResetPwdService:(id)paramters complete:(ComeBackBlock)block;

//**********************************首页***************************/
/**
 *  首页图片轮播
 *
 *  @param paramters @{type_id}
 *  @param block     请求回调
 */
+ (void)getLunBoTuService:(id)paramters comlplete:(ComeBackBlock)block;
@end
