//
//  RTHttpClient.m
//  ZLYDoc
//
//  Created by Ryan on 14-4-10.
//  Copyright (c) 2014年 ZLY. All rights reserved.
//

#import "RTHttpClient.h"
#import "Reachability.h" //pod 'Reachability', '~> 3.1.1'
#import <netinet/in.h>

#ifdef DEBUG  //测试环境
#define SERVER_AUTH_USERNAME              @""
#define SERVER_AUTH_PASSWORD              @""
/***************SERVER HOST***************/
#define SERVER_HOST                       @""
#define SERVER_HOST_PAYPAL                @""
#else
#define SERVER_AUTH_USERNAME              @""
#define SERVER_AUTH_PASSWORD              @""
/***************SERVER HOST***************/
#define SERVER_HOST                       @""
#define SERVER_HOST_PAYPAL                @""

#endif

/***************是否需要鉴权：1、需要 0、不需要***************/
#define SERVER_AUTH_ISNEEDAUTH            0

/***************是否需要Token：1、需要 0、不需要***************/
#define SERVER_AUTH_TOKEN                   0

/***************是否打开本地测试环境***************/
#define LOCAL_SERVER_ISOPEN               NO

@interface RTHttpClient ()
@property(nonatomic, strong) AFHTTPRequestOperationManager *manager;
@end

@implementation RTHttpClient

- (void)httpInit {
    self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:kSOAP_URL_Base_Prodcut]];
    self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //响应结果序列化类型
    self.manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    [self.manager.responseSerializer
     setAcceptableContentTypes:
     [NSSet setWithObjects:@"application/json", @"text/json",
      @"text/javascript", @"text/html", @"text/css",
      @"application/x-javascript", nil]];
}

- (void)jsonInit {
        self.manager = [AFHTTPRequestOperationManager manager];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //响应结果序列化类型
        self.manager.requestSerializer=[AFJSONRequestSerializer serializer];
        
        [self.manager.responseSerializer
         setAcceptableContentTypes:
         [NSSet setWithObjects:@"application/json", @"text/json",
          @"text/javascript", @"text/html", @"text/css",
          @"application/x-javascript", nil]];
}

+ (RTHttpClient *)httpManager {
    static RTHttpClient *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{ instance = [[self alloc] init]; });
    [instance httpInit];
    return instance;
}

+(RTHttpClient*) jsonManager{
    static RTHttpClient *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[RTHttpClient alloc] init];
    });
    [instance jsonInit];
    return instance;
}

/**
 *  本地测试环境
 *
 *  @param url     配置的服务器地址
 *  @param success 成功回调
 */
- (void)localServerPath:(NSString *)url
                success:(void (^)(AFHTTPRequestOperation *task, id))success {
    NSArray *urls = [url componentsSeparatedByString:@"/"];
    NSString *localPath = [urls lastObject];
    localPath = [[NSBundle mainBundle] pathForResource:localPath ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:localPath];
    NSMutableDictionary *dic =
    [NSJSONSerialization JSONObjectWithData:data
                                    options:NSJSONReadingAllowFragments
                                      error:nil];
    success(nil, dic);
}

- (void)requestWithPath:(NSString *)url
                 method:(NSInteger)method
             parameters:(id)parameters
                success:(void (^)(AFHTTPRequestOperation *task,
                                  id responseObject))success
                failure:(void (^)(AFHTTPRequestOperation *task,
                                  NSError *error))failure {
    NSString *requestUrl_ = [self createRequestURLParams:parameters
                                              methodName:nil
                                              methodType:method];
    [self requestWithPath:[NSString stringWithFormat:@"%@%@",url,requestUrl_]
                   method:method
               parameters:parameters
           prepareExecute:^{}
                  success:success
                  failure:failure];
}

/**
 *  HTTP请求（GET、POST、DELETE、PUT）
 *
 *  @param path
 *  @param methodName     RESTFul请求接口名称
 *  @param methodType     RESTFul请求类型
 *  @param parameters 请求参数
 *  @param success    请求成功处理块
 *  @param failure    请求失败处理块
 */
- (void)requestWithPath:(NSString *)url
             methodName:(NSString *)methodName
             methodType:(NSInteger)methodType
             parameters:(id)parameters
                success:(void (^)(AFHTTPRequestOperation *task,
                                  id responseObject))success
                failure:(void (^)(AFHTTPRequestOperation *task,
                                  NSError *error))failure {
    NSString *requestUrl_ = [self createRequestURLParams:parameters
                                              methodName:methodName
                                              methodType:methodType];
    [self requestWithPath:[NSString stringWithFormat:@"%@%@",url,requestUrl_]
                   method:methodType
               parameters:parameters
           prepareExecute:^{}
                  success:success
                  failure:failure];
}

- (void)requestWithPath:(NSString *)url
                 method:(NSInteger)method
             parameters:(id)parameters
         prepareExecute:(PrepareExecuteBlock)prepareExecute
                success:(void (^)(AFHTTPRequestOperation *task, id))success
                failure:(void (^)(AFHTTPRequestOperation *task,
                                  NSError *))failure {
    //请求的URL
    DLog(@"Request path:%@ and param: %@", url, parameters);
    
    //判断网络状况（有链接：执行请求；无链接：弹出提示）
    if ([self isConnectionAvailable]) {
        //预处理（显示加载信息啥的）
        if (prepareExecute) {
            prepareExecute();
        }
        
        if (LOCAL_SERVER_ISOPEN) {
            [self localServerPath:url success:success];
        } else {
            switch (method) {
                case RTHttpRequestGet: {
                    [self.manager GET:url
                           parameters:[NSDictionary dictionary]
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  if (!responseObject) {
                                      
                                  }else{
                                  // NSDictionary转换为Data
                                  NSData *jsonData = [NSJSONSerialization
                                                      dataWithJSONObject:responseObject
                                                      options:NSJSONWritingPrettyPrinted
                                                      error:nil];
                                  
                                  // Data转换为JSON
                                  NSString *str =
                                  [[NSString alloc] initWithData:jsonData
                                                        encoding:NSUTF8StringEncoding];
                                  
                                  NSLog(@"\nURL:%@\nPARAM:%@\nJSONDATA STRING:%@\n", url,
                                        parameters, str);
                                  success(operation, responseObject);
                              }
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  failure(operation, error);
                              }];
                } break;
                    
                case RTHttpRequestPost: {
                    [self.manager POST:url
                            parameters:parameters
                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                   // NSDictionary转换为Data
                                   NSData *jsonData = [NSJSONSerialization
                                                       dataWithJSONObject:responseObject
                                                       options:NSJSONWritingPrettyPrinted
                                                       error:nil];
                                   
                                   // Data转换为JSON
                                   NSString *str =
                                   [[NSString alloc] initWithData:jsonData
                                                         encoding:NSUTF8StringEncoding];
                                   
                                   NSLog(@"\nURL:%@PARAM:%@JSONDATA STRING:%@\n", url, parameters,
                                         str);
                                   success(operation, responseObject);
                               }
                               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                   failure(operation, error);
                               }];
                } break;
                    
                case RTHttpRequestDelete:
                {
                    [self.manager DELETE:url
                              parameters:parameters
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     // NSDictionary转换为Data
                                     NSData *jsonData = [NSJSONSerialization
                                                         dataWithJSONObject:responseObject
                                                         options:NSJSONWritingPrettyPrinted
                                                         error:nil];
                                     
                                     // Data转换为JSON
                                     NSString *str =
                                     [[NSString alloc] initWithData:jsonData
                                                           encoding:NSUTF8StringEncoding];
                                     
                                     NSLog(@"\nURL:%@PARAM:%@JSONDATA STRING:%@\n", url, parameters,
                                           str);
                                     success(operation, responseObject);
                                 }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                     failure(operation, error);
                                 }];
                }
                    break;
                    
                case RTHttpRequestPut: {
                    [self.manager PUT:url
                           parameters:parameters
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  // NSDictionary转换为Data
                                  NSData *jsonData = [NSJSONSerialization
                                                      dataWithJSONObject:responseObject
                                                      options:NSJSONWritingPrettyPrinted
                                                      error:nil];
                                  
                                  // Data转换为JSON
                                  NSString *str =
                                  [[NSString alloc] initWithData:jsonData
                                                        encoding:NSUTF8StringEncoding];
                                  
                                  NSLog(@"\nURL:%@PARAM:%@JSONDATA STRING:%@\n", url, parameters,
                                        str);
                                  success(operation, responseObject);
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  failure(operation, error);
                              }];
                } break;
                    
                default:
                    break;
            }
        }
    } else {
        //网络错误咯
        [self showExceptionDialog];
        //发出网络异常通知广播
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"k_NOTI_NETWORK_ERROR"
         object:nil];
    }
}

- (void)requestWithPathInHEAD:(NSString *)url
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(AFHTTPRequestOperation *task))success
                      failure:(void (^)(AFHTTPRequestOperation *task,
                                        NSError *error))failure {
    if ([self isConnectionAvailable]) {
        [self.manager HEAD:url
                parameters:parameters
                   success:success
                   failure:failure];
    } else {
        [self showExceptionDialog];
    }
}

//看看网络是不是给力
- (BOOL)isConnectionAvailable {
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability =
    SCNetworkReachabilityCreateWithAddress(NULL,
                                           (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags =
    SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags) {
        DLog(@"Error. Could not recover network reachability flags");
        return NO;
    }
    BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
    BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
    return (isReachable && !needsConnection) ? YES : NO;
}

//弹出网络错误提示框
- (void)showExceptionDialog {
    [[[UIAlertView alloc] initWithTitle:@"提示"
                                message:@"网络异常，请检查网络连接"
                               delegate:self
                      cancelButtonTitle:@"好的"
                      otherButtonTitles:nil, nil] show];
}

// get delete 拼接参数
- (NSString *)createRequestURLParams:(NSDictionary *)dic
                          methodName:(NSString *)methodName
                          methodType:(NSInteger)methodType {
    
    if (methodType == RTHttpRequestGet) {
        return [self getRequestParams:dic methodName:methodName];
    } else if (methodType == RTHttpRequestPost) {
        //
    } else if (methodType == RTHttpRequestPut) {
        //
    } else if (methodType == RTHttpRequestDelete) {
//        return [self deleteRequestParams:dic methodName:methodName];
    } else {
        return @"";
    }
    return @"";
    
    
}


- (NSString *)getRequestParams:(NSDictionary *)dic
                    methodName:(NSString *)methodName {
        return [self getGetStringParams:dic];
}

-(NSString*)getGetStringParams:(NSDictionary*)dic{
    if (dic == nil) {
        return [NSString stringWithFormat:@""];
    }else   if ([self wrapUrlParams:dic]) {
        return [NSString stringWithFormat:@"?%@",[self wrapUrlParams:dic]];
    }
    return nil;
}

- (NSString *)wrapUrlParams:(NSDictionary *)dic {
    NSMutableString *envelope = [[NSMutableString alloc] initWithString:@""];
    for (NSString *keyString in [dic allKeys]) {
        [envelope appendFormat:@"%@=%@&", keyString, [dic objectForKey:keyString]];
    }
    NSString *str = nil;
    if (![envelope isEqualToString:@""]) {
        NSRange range;
        range.length = [envelope length] - 1;
        range.location = 0;
        str = [envelope substringWithRange:range];
    }
    return str;
}

@end
