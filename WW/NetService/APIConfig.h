//
//  APIConfig.h
//  ASYC
//
//  Created by Macro on 15/5/19.
//  Copyright (c) 2015年 macro. All rights reserved.
//


#ifdef DEBUG
//测试环境
#define kSOAP_URL_Base_Prodcut  @"http://wwcw.figo.cn"
#else
//正式环境
#define kSOAP_URL_Base_Prodcut @"http://wwcw.figo.cn"
#endif

#define KPageNum      10


/**********************************兑换***************************/
//兑换-兑换
#define kSOAP_URL_EXCHANGE_ADD          @"/Exchange/add"

//兑换-兑换列表
#define kSOAP_URL_EXCHANGE_LIST         @"/Exchange/getList"

//兑换-兑换详情
#define kSOAP_URL_EXCHANGE_DETAIL       @"/Exchange/detail"


/**********************************商品***************************/
//商品-商品列表
#define kSOAP_URL_PRODUCT_LIST         @"/Product/getList"

//商品-商品详情
#define kSOAP_URL_PRODUCT_DETAIL       @"/Product/detail"


/**********************************图片***************************/
//图片-发表图片
#define kSOAP_URL_PICTURE_PUBLISH     @"/Article/publish"

//图片-图片列表
#define kSOAP_URL_PICTURE_LIST        @"/Article/getList"

//图片-图片详情
#define kSOAP_URL_PICTURE_DETAIL      @"/Article/detail"

//评论-添加评论
#define kSOAP_URL_PICTURE_ADDCOMMENT  @"/Comment/add"

//评论-评论列表
#define kSOAP_URL_PICTURE_COMMENTLIST @"/Comment/getList"

//分享-添加分享
#define kSOAP_URL_PICTURE_COMMENTSHARE @"/Share/add"

//分享-分享列表
#define kSOAP_URL_PICTURE_COMMENTSHARELIST   @"/Share/getList"

//点赞-点赞
#define kSOAP_URL_PICTURE_PRAISE       @"/Article/praise"

//点赞-点赞列表
#define kSOAP_URL_PICTURE_PRAISELIST   @"/Article/praiseList"

/**********************************地址***************************/
//地址-地区列表
#define kSOAP_URL_ADDRESS_DISTRICTLIST @"/Address/regionList"

//地址-添加收货地址
#define kSOAP_URL_ADDRESS_ADDADDRESS   @"/Address/add"

//地址－删除收货地址
#define kSOAP_URL_ADDRESS_DELETEADDRESS @"/Address/delete"

//地址－修改收货地址
#define kSOAP_URL_ADDRESS_CHANGEADDRESS @"/Address/update"

//地址－收货地址列表
#define kSOAP_URL_ADDRESS_ADDRESSLIST   @"/Address/getList"

//地址-设置默认收货地址
#define kSOAP_URL_DEFAULTADDRESS        @"/Address/setDefault"
 /*********************************用户****************************/
//注册－发送验证码
#define kSOAP_URL_REGISTER_SENDVERIFY  @"/Register/sendVerify"

//注册-验证并注册
#define kSOAP_URL_REGISTER_REGISTE     @"/Register/checkVerify"

//登录-用户登录
#define kSOAP_URL_LOGIN_LOGIN          @"/Login/official"

//登录-token登录
#define kSOAP_URL_LOGIN_TOKEN          @"/Login/token"

//登录-QQ联合登录
#define kSOAP_URL_LOGIN_QQ              @"/Login/qq"

//登录-微信联合登录
#define kSOAP_URL_LOGIN_WECHAT          @"/Login/weixin"

//登录－第三方登录
#define kSOAP_URL_THIRD_LOGIN           @"/Login/third"

//用户-获取个人信息
#define kSOAP_URL_USER_GETUSERMESSAGE       @"/User/getInfo"

//用户-修改个人信息
#define kSOAP_URL_USER_UPDATEUSERMESSAGE    @"/User/updateInfo"

//用户-上传图片
#define kSOAP_URL_USER_UPLOADPICTURE         [NSString stringWithFormat:@"%@/Image/up",kSOAP_URL_Base_Prodcut]

//用户-获取宠物类型
#define kSOAP_URL_USER_GETPETTYPELIST       @"/User/getPetTypeList"

//用户-获取宠物地区
#define kSOAP_URL_USER_GETPETAREALIST       @"/User/getPetAreaList"

//用户-修改密码
#define kSOAP_URL_USER_CHANGEPWD            @"/User/changePassword"

//用户-添加/取消关注
#define kSOAP_URL_USER_FLLOW                @"/User/follow"

//用户-关注列表
#define kSOAP_URL_USER_FLLOWLIST            @"/User/followList?"

//用户-粉丝列表
#define kSOAP_URL_USER_FANSLIST             @"/User/fansList?"

//用户-任务信息
#define kSOAP_URL_USER_TASKINFO             @"/User/taskInfo"

//用户-个人主页
#define kSOAP_URL_USER_USERHOME             @"/User/home"

//找回密码-发送验证码
#define kSOAP_URL_USER_SENDVERIFY           @"/User/retrieveSend"

//找回密码－验证并重置密码
#define kSOAP_URL_USER_RESETPWD             @"/User/retrieveCheck"

/*********************************首页****************************/
//首页－首页图片轮播
#define kSOAP_URL_HOME_SLIDELIST            @"/Slides/getList"

#import "RTHttpClient.h"
#import "AppNetService.h"
