//
//  AppNetService.m
//  ASYC
//
//  Created by Macro on 15/5/19.
//  Copyright (c) 2015年 macro. All rights reserved.
//

#import "AppNetService.h"
#import "RTHttpClient.h"
#import "GetExchangeListResult.h"
#import "ExchangeDetailResult.h"
#import "ProductListResult.h"
#import "ProductDetailResult.h"
#import "PicturePublishResult.h"
#import "PictureListResult.h"
#import "CommentAddResult.h"
#import "CommentListResult.h"
#import "PictureDetailResult.h"
#import "ShareAddResult.h"
#import "ShareListResult.h"
#import "PraiseListResult.h"
#import "AddressDistrictListResult.h"
#import "AddressGetProductListResult.h"
#import "LoginResult.h"
#import "UserMessageResult.h"
#import "PetListResult.h"
#import "PetAreaListResult.h"
#import "UserFllowResult.h"
#import "UserTaskInfoResult.h"
#import "UserInfoResult.h"

#define kPageCount  @"10"

static AppNetService *service = nil;

@implementation AppNetService

+(id)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[AppNetService alloc] init];
    });
    return service;
}

#pragma mark - 兑换

+ (void)getExchangeService:(id)parameters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_EXCHANGE_ADD
                                         method:RTHttpRequestPost
                                     parameters:parameters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([(NSNumber*)responseObject[@"error_code"] integerValue] == 1) {
                                                  block(YES,nil);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getExchangedListService:(id)parameters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_EXCHANGE_LIST
                                         method:RTHttpRequestGet
                                     parameters:parameters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                     GetExchangeListResult *result = [[GetExchangeListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;

                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getExchangeDetailService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_EXCHANGE_DETAIL
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    ExchangeDetailResult *result = [[ExchangeDetailResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;

                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 商品
+ (void)getProductListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PRODUCT_LIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    ProductListResult *result = [[ProductListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getproductDetailService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PRODUCT_DETAIL
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    ProductDetailResult *result = [[ProductDetailResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getPicturePublishService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_PUBLISH
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    PicturePublishResult *result = [[PicturePublishResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getPictureListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_LIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    PictureListResult *result = [[PictureListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getPictureDetailService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_DETAIL
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    PictureDetailResult *result = [[PictureDetailResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 评论
+ (void)getCommentAddService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_ADDCOMMENT
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    CommentAddResult *result = [[CommentAddResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getCommentListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_COMMENTLIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    CommentListResult *result = [[CommentListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}


#pragma mark - 分享
+ (void)getShareAddService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_COMMENTSHARE
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    ShareAddResult *result = [[ShareAddResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
  
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getShareListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_COMMENTSHARELIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    ShareListResult *result = [[ShareListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 点赞
+ (void)getPraiseAddService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_PRAISE
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getPraiseListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_PICTURE_PRAISELIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    PraiseListResult *result = [[PraiseListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 地区
+ (void)getAddressDistrictListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_ADDRESS_DISTRICTLIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    AddressDistrictListResult *result = [[AddressDistrictListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getAddressAddService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_ADDRESS_ADDADDRESS
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                              block(NO,nil);
                                        }];
}

+ (void)getAddressDeleteService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_ADDRESS_DELETEADDRESS
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getAddressChangeService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_ADDRESS_CHANGEADDRESS
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getAddressListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_ADDRESS_ADDRESSLIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    AddressGetProductListResult *result = [[AddressGetProductListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)setDefaultAddressService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_DEFAULTADDRESS
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 用户 - 注册
+ (void)getRegisterSendVerifyService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_REGISTER_SENDVERIFY
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getRegisterRegiste:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_REGISTER_REGISTE
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                     LoginResult *result = [[LoginResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);

                                                }
                                               }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 用户－登录
+ (void)getLoginCommonLoginService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_LOGIN_LOGIN
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    LoginResult *result = [[LoginResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getLoginTokenLoginService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_LOGIN_TOKEN
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    LoginResult *result = [[LoginResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getLoginQQLoginService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_LOGIN_QQ
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    LoginResult *result = [[LoginResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getLoginWeChatLoginService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_LOGIN_WECHAT
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    LoginResult *result = [[LoginResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getThirdLoginService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_THIRD_LOGIN
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    LoginResult *result = [[LoginResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                             block(NO,nil);
                                        }];
}

#pragma mark - 用户－信息
+ (void)getUserMessageService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_GETUSERMESSAGE
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    UserMessageResult *result = [[UserMessageResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getUserMessageChangeService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_UPDATEUSERMESSAGE
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    UserMessageResult *result = [[UserMessageResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 宠物
+ (void)getPetTypeService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_GETPETTYPELIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                     PetListResult *result = [[PetListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getPetDistrictsService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_GETPETAREALIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    PetAreaListResult *result = [[PetAreaListResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}


#pragma mark - 用户－其他
+ (void)getUserUploadPictureService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_UPLOADPICTURE
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"data"][@"img_id"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getuserChangePwdService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_CHANGEPWD
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getUserPayOrCancelAttentionService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_FLLOW
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getUserPayListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_FLLOWLIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    UserFllowResult *result = [[UserFllowResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getUserFansListService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_FANSLIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    UserFllowResult *result = [[UserFllowResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getUserTaskMessageService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_TASKINFO
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    UserTaskInfoResult *result = [[UserTaskInfoResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getuserMainHoneService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_USERHOME
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    UserInfoResult *result = [[UserInfoResult alloc] initWithDictionary:responseObject];
                                                    block(YES,result.data);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 用户－密码
+ (void)getPwdSendVerifyService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_SENDVERIFY
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

+ (void)getPwdResetPwdService:(id)paramters complete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_USER_RESETPWD
                                         method:RTHttpRequestPost
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"error_msg"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,nil);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

#pragma mark - 首页
+ (void)getLunBoTuService:(id)paramters comlplete:(ComeBackBlock)block{
    [[RTHttpClient httpManager] requestWithPath:kSOAP_URL_HOME_SLIDELIST
                                         method:RTHttpRequestGet
                                     parameters:paramters
                                        success:^(AFHTTPRequestOperation *task, id responseObject) {
                                            if (responseObject) {
                                                if ([responseObject[@"error_code"] integerValue] == 1) {
                                                    block(YES,responseObject[@"data"]);
                                                    return ;
                                                }
                                            }
                                            block(NO,responseObject[@"error_msg"]);
                                        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                                            block(NO,nil);
                                        }];
}

@end
