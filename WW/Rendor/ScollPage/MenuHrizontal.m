//
//  MenuHrizontal.m
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "MenuHrizontal.h"

#define kMenuHrizontalWidth self.frame.size.width
#define kMenuHrizontalHeight self.frame.size.height

@implementation MenuHrizontal

- (id)initWithFrame:(CGRect)frame buttonItems:(NSArray *)aButtonArray{
    if (self = [super initWithFrame:frame]) {
        if (!_mButtonArray) {
            _mButtonArray = [NSMutableArray array];
        }
        
        if (!_mScrollView) {
            _mScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kMenuHrizontalWidth,kMenuHrizontalHeight)];
            _mScrollView.showsHorizontalScrollIndicator = NO;
        }
        
        if (!_mItemInfoArray) {
            _mItemInfoArray = [NSMutableArray array];
        }
        [_mItemInfoArray removeAllObjects];
        [self createMenuItems:aButtonArray];
    }
    return self;
}

- (void)createMenuItems:(NSArray*)aButtonArray{
    int i = 0;
    float menuWidth = 0.0;
    for (NSDictionary *dic in aButtonArray) {
       __block UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
       __block float buttonWidth;
        
        [dic enumerateKeysAndObjectsUsingBlock:^(NSString* key, NSString* obj, BOOL *stop) {
            if ([key isEqualToString:kNormalKey]) {
                [button setBackgroundImage:[UIImage imageNamed:obj] forState:UIControlStateNormal];
            }
            if ([key isEqualToString:kHighlightKey]) {
                [button setBackgroundImage:[UIImage imageNamed:obj] forState:UIControlStateSelected];
            }
            if ([key isEqualToString:kTitleKey]) {
                [button setTitle:obj forState:UIControlStateNormal];
            }
            
            if ([key isEqualToString:kTitleWidthKey]) {
                buttonWidth = obj.floatValue;
            }
        }];
        
        button.tag = i;
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        
        [button addTarget:self action:@selector(menuButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(menuWidth, 0, buttonWidth, kMenuHrizontalHeight);
        
        [_mScrollView addSubview:button];
        [_mButtonArray addObject:button];
        
        menuWidth += buttonWidth;
        i++;
        
        NSMutableDictionary *newDic = [dic mutableCopy];
        [newDic setObject:@(menuWidth) forKey:kTotalWidthKey];
        [_mItemInfoArray addObject:newDic];
    }
    
    _mScrollView.contentSize = CGSizeMake(menuWidth, kMenuHrizontalHeight);
    [self addSubview:_mScrollView];
    _mTotalWidth = menuWidth;
}

#pragma mark - 取消所有button点击状态
- (void)changeButtonToNormalState{
    for (UIButton *button in _mButtonArray) {
        button.selected = NO;
    }
}

#pragma mark - 选中某个按钮
- (void)clickButtonAtIndex:(NSInteger)aIndex{
    UIButton *button = _mButtonArray[aIndex];
    [self menuButtonClicked:button];
}

- (void)changeButtonStateAtIndex:(NSInteger)aIndex{
    UIButton *button = [_mButtonArray objectAtIndex:aIndex];
    [self changeButtonToNormalState];
    button.selected = YES;
    [self moveScrolViewWithIndex:aIndex];
}

#pragma mark 移动button到可视的区域
-(void)moveScrolViewWithIndex:(NSInteger)aIndex{
    if (_mItemInfoArray.count < aIndex) {
        return;
    }
    
    if (_mTotalWidth <= 320) {
        return;
    }
    
    NSDictionary *dic = [_mItemInfoArray objectAtIndex:aIndex];
    float buttonOrign = [[dic objectForKey:kTotalWidthKey] floatValue];
    
    if (buttonOrign >= 300) {
        if (buttonOrign >= _mScrollView.contentSize.width - 180) {
            [_mScrollView setContentOffset:CGPointMake(_mScrollView.contentSize.width - kWindowWidth,_mScrollView.contentOffset.y) animated:YES];
            return;
        }
        
        float moveToContentOffset = buttonOrign - 180;
        if (moveToContentOffset > 0) {
            [_mScrollView setContentOffset:CGPointMake(moveToContentOffset, _mScrollView.contentOffset.y) animated:YES];
        }
    }else{
        [_mScrollView setContentOffset:CGPointMake(0, _mScrollView.contentOffset.y) animated:YES];
        return;
    }
}

#pragma mark - 点击事件
- (void)menuButtonClicked:(UIButton*)sender{
    [self changeButtonStateAtIndex:sender.tag];
    if (_delegate && [_delegate respondsToSelector:@selector(didMenuHrizontalClickedButtonAtIndex:)]) {
        [_delegate didMenuHrizontalClickedButtonAtIndex:sender.tag];
    }
}

#pragma mark - 内存相关
-(void)dealloc{
    [_mButtonArray removeAllObjects];
    _mButtonArray = nil;
}

@end
