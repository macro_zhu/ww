//
//  ScrollPageView.m
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "ScrollPageView.h"
#import "SGFocusImageItem.h"
#import "SGFocusImageFrame.h"
#import "MengTuMainTableCell.h"
#import "ScrollPageTableView.h"
#import <MJRefresh.h>
#import "UIView+Toast.h"
#import "PictureListData.h"
#import "PictureListArticleList.h"
#import "UIView+Toast.h"
#import "LoginController.h"
#import "CommentAddData.h"

#define kScrollPageViewWidth  self.frame.size.width
#define kScrollPageViewHeight self.frame.size.height


@interface ScrollPageView ()<ScrollPageViewTableViewDelegate,ScrollPageViewTableViewDataSource,UITextFieldDelegate>{
    PictureListData *_dataModel;
    NSInteger _dataPage;
    BOOL _isAllowRequestMore;
    NSInteger articleId;
}

@end

@implementation ScrollPageView

-(void)dealloc{
    self.scrollView = nil;
    [self.contentItems removeAllObjects];
    self.contentItems = nil;
}

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        mNeedUseDelegate = YES;
        [self commInit];
    }
    return self;
}

- (void)commInit{
    if (!_contentItems) {
        _contentItems = [NSMutableArray array];
    }
    
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kScrollPageViewWidth, kScrollPageViewHeight)];
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.backgroundColor = [UIColor whiteColor];
    }
    [self addSubview:_scrollView];
    
    _dataPage = 1;
    _isAllowRequestMore = NO;
}

#pragma mark 添加ScrollowViewd的ContentView
-(void)setContentOfTables:(NSInteger)aNumerOfTables{
    for (int i = 0; i<aNumerOfTables; i++) {
        ScrollPageTableView* view = [[ScrollPageTableView alloc] initWithFrame:CGRectMake(kScrollPageViewWidth * i,0, kScrollPageViewWidth, kScrollPageViewHeight)];
        view.delegate = self;
        view.dataSouce = self;
        [view.tableView registerNib:[UINib nibWithNibName:@"MengTuMainTableCell" bundle:nil] forCellReuseIdentifier:@"MengTuMainTableCell"];
        view.tableView.tableFooterView = [[UIView alloc] init];
        [self addLoopScrollowView:view];
        [_scrollView addSubview:view];
        [_contentItems addObject:view.tableView];
    }
    [_scrollView setContentSize:CGSizeMake(kScrollPageViewWidth*aNumerOfTables, kScrollPageViewHeight)];
}

#pragma mark 添加HeaderView
-(void)addLoopScrollowView:(ScrollPageTableView *)aTableView {
    //添加一张默认图片
    SGFocusImageItem *item = [[SGFocusImageItem alloc] initWithDict:@{@"image": [NSString stringWithFormat:@"girl%d",2]} tag:-1];
    SGFocusImageFrame *bannerView = [[SGFocusImageFrame alloc] initWithFrame:CGRectMake(0, 0, kScrollPageViewWidth, 105*kViewRate) delegate:aTableView imageItems:@[item] isAuto:YES];
    aTableView.tableView.tableHeaderView = bannerView;
}

#pragma mark 滑动到某个页面
-(void)moveScrollowViewAthIndex:(NSInteger)aIndex{
    mNeedUseDelegate = NO;
    CGRect rect = CGRectMake(kScrollPageViewWidth *aIndex, 0, kScrollPageViewWidth, kScrollPageViewHeight);
    [_scrollView scrollRectToVisible:rect animated:YES];
    mCurrentPage = aIndex;
    if (_delegate && [_delegate respondsToSelector:@selector(didScrollPageViewChangedPage:)]) {
        [_delegate didScrollPageViewChangedPage:mCurrentPage];
    }
}

#pragma mark 刷新某个页面
-(void)freshContentTableAtIndex:(NSInteger)aIndex{
    if (_contentItems.count < aIndex) {
        return;
    }
    UITableView *tableView = [_contentItems objectAtIndex:aIndex];
    [tableView.header beginRefreshing];
//    [self getMengTuListService:aIndex];
}

-(void)didSelectedTableRowComplete:(DidTableRowBlock)comeBackBlock{
    self.block = [comeBackBlock copy];
}

#pragma mark 改变TableView上面滚动栏的内容
-(void)changeHeaderContentWithCustomTable:(ScrollPageTableView *)aTableContent{
    [self getLunBoTuService:mCurrentPage+1 complete:^(NSArray * array) {
        NSInteger  length = array.count;
        NSMutableArray *tempArray = [NSMutableArray array];
        for (int i = 0 ; i < length; i++)
        {
            NSDictionary *arrayDic = array[i];
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"title%d",i],@"title" ,
                                   arrayDic[@"image"],@"image",
                                  nil];
            [tempArray addObject:dict];
        }
        
        NSMutableArray *itemArray = [NSMutableArray arrayWithCapacity:length+2];
        //添加最后一张图 用于循环
        if (length > 1)
        {
            NSDictionary *dict = [tempArray objectAtIndex:length-1];
            SGFocusImageItem *item = [[SGFocusImageItem alloc] initWithDict:dict tag:-1];
            [itemArray addObject:item];
        }
        for (int i = 0; i < length; i++)
        {
            NSDictionary *dict = [tempArray objectAtIndex:i];
            SGFocusImageItem *item = [[SGFocusImageItem alloc] initWithDict:dict tag:i];
            [itemArray addObject:item];
            
        }
        //添加第一张图 用于循环
        if (length >1)
        {
            NSDictionary *dict = [tempArray objectAtIndex:0];
            SGFocusImageItem *item = [[SGFocusImageItem alloc] initWithDict:dict tag:length];
            [itemArray addObject:item];
        }
        
        SGFocusImageFrame *vFocusFrame = (SGFocusImageFrame *)aTableContent.tableView.tableHeaderView;
        [vFocusFrame changeImageViewsContent:itemArray];
     
    }];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    mNeedUseDelegate = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = (_scrollView.contentOffset.x+kScrollPageViewWidth/2.0) /kScrollPageViewWidth;
    if (mCurrentPage == page) {
        return;
    }
    mCurrentPage= page;
    if ([_delegate respondsToSelector:@selector(didScrollPageViewChangedPage:)] && mNeedUseDelegate) {
        [_delegate didScrollPageViewChangedPage:mCurrentPage];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
 
    }
}

#pragma mark - ScrollTableViewDataSource
-(NSInteger)numberOfRowsInTableView:(UITableView *)aTableView InSection:(NSInteger)section FromView:(ScrollPageTableView *)aView{
    return aView.tableInfoArray.count;
}

-(UITableViewCell *)cellForRowInTableView:(UITableView *)aTableView IndexPath:(NSIndexPath *)aIndexPath FromView:(ScrollPageTableView *)aView{
    MengTuMainTableCell *cell = [aTableView dequeueReusableCellWithIdentifier:@"MengTuMainTableCell"];
    PictureListArticleList *model = aView.tableInfoArray[aIndexPath.row];
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:nil];
    cell.headImg.layer.cornerRadius = 5;
    cell.headImg.layer.masksToBounds = YES;
    cell.nameLbl.text = model.alias;
    cell.infoLbl.text = [NSString stringWithFormat:@"%@|%@|%@",model.petType,model.petAge,model.petArea];
    [cell.mainImg sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:nil];
    cell.sexImg.image = (model.gender == 1)?[UIImage imageNamed:@"flag_m"]:[UIImage imageNamed:@"flag_f"];
    cell.contentLbl.text = model.content;
    cell.timeReplyLbl.text = model.dateDes;
    [cell.addWishBtn setTitle:[NSString stringWithFormat:@"%ld",(long)model.praiseNumber] forState:UIControlStateNormal];
    [cell.commentBtn setTitle:[NSString stringWithFormat:@"%ld",(long)model.commentNumber] forState:UIControlStateNormal];
    [cell.peopleLooksBtn setTitle:[NSString stringWithFormat:@"%ld",(long)model.viewNumber] forState:UIControlStateNormal];
    
    cell.user1HeadImg.layer.cornerRadius = 5;
    cell.user1HeadImg.layer.masksToBounds = YES;
    
    [cell.user1HeadImg setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.avatar]]] forState:UIControlStateNormal];
    cell.commentTF.delegate = self;
    
    //评论部分
    if (model.commentData.count >= 1) {
        PictureListCommentData *commentModel = model.commentData[0];
//        [cell.commenterHeadImg setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:commentModel.avatar]]] forState:UIControlStateNormal];
        cell.commenterHeadImg.layer.cornerRadius = 5;
        cell.commenterHeadImg.layer.masksToBounds = YES;
        [cell.commenterHeadImg setBackgroundImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:commentModel.avatar]]] forState:UIControlStateNormal];
        cell.commenterNamelbl.text = commentModel.alias;
        cell.commenterDataLbl.text = commentModel.addDate;
        cell.commenterCommentLbl.text = commentModel.content;
        [cell.lookForAllCommentBtn setTitle:[NSString stringWithFormat:@"查看全部%ld条评论",model.commentData.count] forState:UIControlStateNormal];
    }else{
        [cell.commenterHeadImg setBackgroundImage:nil forState:UIControlStateNormal];
        cell.commenterCommentLbl.text = @"";
        cell.commenterDataLbl.text = @"";
        cell.commenterNamelbl.text = @"";
        [cell.lookForAllCommentBtn setTitle:@"" forState:UIControlStateNormal];
    }
    
    @weakify(self)
    [[cell.commentTF.rac_textSignal filter:^BOOL(id value) {
        return cell.commentTF.text.length>=1;
    }] subscribeNext:^(id x) {
        articleId = model.articleId;
        DLog(@"articleId = %ld",model.articleId);
    }];
    
    //点赞
    [[cell.addWishBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        if ([UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
            [self getZanService:model.articleId andComeplete:^{
                model.praiseNumber = model.praiseNumber +1;
            }];
        }else{
            [ToastUtil showToast:@"请先登录"];
        }
    }];
    
    //关注
    [cell.attentionBtn setBackgroundImage:(model.isFollowed?[UIImage imageNamed:@"button_attention"]:[UIImage imageNamed:@"button_cancelAttention"]) forState:UIControlStateNormal];
    
    [[cell.attentionBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        if ([UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
            [self getAttentionService:model.userId complete:^{
                [ToastUtil showToast:(model.isFollowed?@"取消关注成功!":@"关注成功!")];
                [cell.attentionBtn setBackgroundImage:(model.isFollowed?[UIImage imageNamed:@"button_cancelAttention"]:[UIImage imageNamed:@"button_attention"]) forState:UIControlStateNormal];
            }];
        }else{
            [ToastUtil showToast:@"请先登录"];
        }
    }];
    
    //评论
    [[cell.commentBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if ([UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
           [cell.commentTF becomeFirstResponder];
        }else{
            [ToastUtil showToast:@"请先登录"];
        }
    }];
    
    
    return cell;
}

#pragma mark ScrollTableViewDelegate
-(float)heightForRowAthIndexPath:(UITableView *)aTableView IndexPath:(NSIndexPath *)aIndexPath FromView:(ScrollPageTableView *)aView{
    PictureListArticleList *model = aView.tableInfoArray[aIndexPath.row];
    if (model.commentData.count>=1) {
        return 500*kViewRate;
    }else{
        return 460*kViewRate;
    }
}

-(void)didSelectedRowAthIndexPath:(UITableView *)aTableView IndexPath:(NSIndexPath *)aIndexPath FromView:(ScrollPageTableView *)aView{
     PictureListArticleList *model = aView.tableInfoArray[aIndexPath.row];
    if ([UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
        _block(model.articleId);
    }else{
        [ToastUtil showToast:@"请先登录"];
    }
}

-(void)loadData:(void(^)())complete FromView:(ScrollPageTableView *)aView{
    if (!_isAllowRequestMore) {
        return;
    }
   [self getMengTuListService:mCurrentPage andView:aView complete:complete];
}

-(void)refreshData:(void(^)())complete FromView:(ScrollPageTableView *)aView{
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
         [aView.tableInfoArray removeAllObjects];
       
        [self getMengTuListService:mCurrentPage andView:aView complete:complete];
        
        //改变header显示图片
        [self changeHeaderContentWithCustomTable:aView];
    });
}

#pragma mark - 网络数据部分
- (void)getMengTuListService:(NSInteger)index andView:(ScrollPageTableView*)aView complete:(void(^)())complete{
    if (index +1 == 2) {
        if (![UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushToLoginCtrl" object:nil];
        }
    }
    
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self andHUD:hub];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if ([UserDefaultsUtils valueWithKey:@"TOKEN"]) {
        [dic setObject:[UserDefaultsUtils valueWithKey:@"TOKEN"] forKey:@"token"];
    }
    [dic setObject:@(index+1) forKey:@"type"];
    [dic setObject:@(_dataPage) forKey:@"page_no"];
    [dic setObject:@(KPageNum) forKey:@"page_num"];
    [AppNetService getPictureListService:dic complete:^(BOOL isSuccess, PictureListData* model) {
        [hub hide:YES];
        if (isSuccess) {
            _dataModel = model;
            [aView.tableInfoArray addObjectsFromArray:_dataModel.articleList];
            if (_dataModel.articleList.count >= KPageNum) {
                _dataPage++;
                _isAllowRequestMore = YES;
            }
        }
        if (complete) {
            complete();
        }
    }];
}

- (void)getZanService:(NSInteger)articleid andComeplete:(void(^)(void))comeBack{
    @weakify(self)
    __block MBProgressHUD *hub = [[MBProgressHUD alloc] initWithView:self];
    [MBProgressHuDUtil showHUD:@"正在加载..." andView:self andHUD:hub];
    [AppNetService getPraiseAddService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"article_id":@(articleid)} complete:^(BOOL isSuccess, NSString*errorMsg) {
        @strongify(self)
        [hub hide:YES];
        if (isSuccess) {
            UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zan"]];
            [self showToast:view duration:0.5 position:@"center"];
            comeBack();
        }else{
            UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zan"]];
            [self showToast:view duration:0.5 position:@"center"];
        }
    }];
}

- (void)getLunBoTuService:(NSInteger)typeId complete:(void(^)(NSArray*))comeBack{
    [AppNetService getLunBoTuService:@{@"type_id":@(typeId)} comlplete:^(BOOL isSuccess, NSArray* picArray) {
        if (isSuccess) {
            comeBack(picArray);
        }
    }];
}

- (void)getAttentionService:(NSInteger)userId complete:(void(^)(void))comBack{
    [AppNetService getUserPayOrCancelAttentionService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"user_id":@(userId)} complete:^(BOOL isSuccess , NSString *errorMsg) {
        if (isSuccess) {
            comBack();
        }
    }];
}

- (void)getAddCommentServiceWithArticleId:(NSInteger)articleid comment:(NSString *)comment complete:(void(^)(void))comBack{
    [AppNetService getCommentAddService:@{@"token":[UserDefaultsUtils valueWithKey:@"TOKEN"],@"article_id":@(articleid),@"comment":comment} complete:^(BOOL isSuccess, CommentAddData* model) {
        if (isSuccess) {
            [ToastUtil showToast:@"添加评论成功!"];
            comBack();
        }
    }];
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if ([UserDefaultsUtils boolValueWithKey:@"IsLogin"]) {
        if ([textField.text isEqualToString:@""]) {
            return NO;
        }
        [self getAddCommentServiceWithArticleId:articleId comment:textField.text complete:^{
            
        }];
    }
    return YES;
}

@end
