//
//  ScrollPageTableView.m
//  WW
//
//  Created by Macro on 15/6/4.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "ScrollPageTableView.h"
#import <MJRefresh.h>

@implementation ScrollPageTableView

- (void)dealloc{
    [_tableInfoArray removeAllObjects];
    _tableInfoArray = nil;
}

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        if (!_tableInfoArray) {
            _tableInfoArray = [NSMutableArray array];
        }
        
        if (!_tableView) {
            _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            _tableView.delegate = self;
            _tableView.dataSource = self;
            _tableView.backgroundColor = [UIColor clearColor];
            
            if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
                [_tableView setLayoutMargins:UIEdgeInsetsZero];
            }
            
            if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
                [_tableView setSeparatorInset:UIEdgeInsetsZero];
            }
        }
        
        [self addSubview:_tableView];
        
        __weak typeof(self) weakSelf = self;
        [_tableView addLegendHeaderWithRefreshingBlock:^{
            _reloading = YES;
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(refreshData:FromView:)]) {
                [weakSelf.delegate refreshData:^{
                    [weakSelf.tableView reloadData];
                    [weakSelf.tableView.header endRefreshing];
                } FromView:weakSelf];
            }
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [weakSelf.tableView reloadData];
//                [weakSelf.tableView.header endRefreshing];
//            });
            
        }];
        
        [_tableView addLegendFooterWithRefreshingBlock:^{
            if ([weakSelf.delegate respondsToSelector:@selector(loadData:FromView:)]) {
                [weakSelf.delegate loadData:^(int aAddedRowCount) {
                    [weakSelf.tableView reloadData];
                    [weakSelf.tableView.footer endRefreshing];
                } FromView:weakSelf];
            }
        }];
    }
    return self;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_dataSouce && [_dataSouce respondsToSelector:@selector(numberOfRowsInTableView:InSection:FromView:)]) {
      NSInteger rows = [_dataSouce numberOfRowsInTableView:tableView InSection:section FromView:self];
        mRowCount = rows;
        return mRowCount;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate && [_delegate respondsToSelector:@selector(heightForRowAthIndexPath:IndexPath:FromView:)]) {
        CGFloat height = [_delegate heightForRowAthIndexPath:tableView IndexPath:indexPath FromView:self];
        return height;
    }
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_dataSouce && [_dataSouce respondsToSelector:@selector(cellForRowInTableView:IndexPath:FromView:)]) {
        UITableViewCell* cell = [_dataSouce cellForRowInTableView:tableView IndexPath:indexPath FromView:self];
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedRowAthIndexPath:IndexPath:FromView:)]) {
        [_delegate didSelectedRowAthIndexPath:tableView IndexPath:indexPath FromView:self];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark SGFocusImageFrameDelegate
- (void)foucusImageFrame:(SGFocusImageFrame *)imageFrame didSelectItem:(SGFocusImageItem *)item
{
    NSLog(@"%s \n click===>%@",__FUNCTION__,item.title);
}

- (void)foucusImageFrame:(SGFocusImageFrame *)imageFrame currentItem:(int)index;
{
    //    NSLog(@"%s \n scrollToIndex===>%d",__FUNCTION__,index);
}

@end
