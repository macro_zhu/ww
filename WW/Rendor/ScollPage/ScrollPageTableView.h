//
//  ScrollPageTableView.h
//  WW
//
//  Created by Macro on 15/6/4.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SGFocusImageItem.h"
#import "SGFocusImageFrame.h"

@class ScrollPageTableView;
@protocol ScrollPageViewTableViewDelegate <NSObject>
@required
-(float)heightForRowAthIndexPath:(UITableView *)aTableView IndexPath:(NSIndexPath *)aIndexPath FromView:(ScrollPageTableView *)aView;
-(void)didSelectedRowAthIndexPath:(UITableView *)aTableView IndexPath:(NSIndexPath *)aIndexPath FromView:(ScrollPageTableView *)aView;
-(void)loadData:(void(^)())complete FromView:(ScrollPageTableView *)aView;
-(void)refreshData:(void(^)())complete FromView:(ScrollPageTableView *)aView;

@end

@protocol ScrollPageViewTableViewDataSource <NSObject>
@required
-(NSInteger)numberOfRowsInTableView:(UITableView *)aTableView InSection:(NSInteger)section FromView:(ScrollPageTableView *)aView;
-(UITableViewCell *)cellForRowInTableView:(UITableView *)aTableView IndexPath:(NSIndexPath *)aIndexPath FromView:(ScrollPageTableView *)aView;

@end

@interface ScrollPageTableView : UIView<SGFocusImageFrameDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSInteger mRowCount;
}

@property (nonatomic,assign) BOOL reloading;

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *tableInfoArray;

@property (nonatomic,assign) id<ScrollPageViewTableViewDelegate> delegate;
@property (nonatomic,assign) id<ScrollPageViewTableViewDataSource> dataSouce;

@end
