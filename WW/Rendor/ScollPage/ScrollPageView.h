//
//  ScrollPageView.h
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollPageTableView.h"

typedef void(^DidTableRowBlock)(NSInteger);

@protocol ScrollPageViewDelegate <NSObject>

@optional
- (void)didScrollPageViewChangedPage:(NSInteger)aPage;

@end

@interface ScrollPageView : UIView<UIScrollViewDelegate>
{
    NSInteger mCurrentPage;
    BOOL mNeedUseDelegate;
}
@property (nonatomic,retain) UIScrollView *scrollView;

@property (nonatomic,retain) NSMutableArray *contentItems;

@property (nonatomic,assign) id<ScrollPageViewDelegate> delegate;
@property (nonatomic,copy) DidTableRowBlock block;

#pragma mark 添加ScrollowViewd的ContentView
-(void)setContentOfTables:(NSInteger)aNumerOfTables;
#pragma mark 滑动到某个页面
-(void)moveScrollowViewAthIndex:(NSInteger)aIndex;
#pragma mark 刷新某个页面
-(void)freshContentTableAtIndex:(NSInteger)aIndex;
#pragma mark 改变TableView上面滚动栏的内容
-(void)changeHeaderContentWithCustomTable:(ScrollPageTableView *)aTableContent;

-(void)didSelectedTableRowComplete:(DidTableRowBlock)comeBackBlock;

@end
