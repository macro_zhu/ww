//
//  MenuHrizontal.h
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScallPage.h"

@protocol MenuHrizontalDelegate <NSObject>

@optional
//点击菜单按钮响应事件
-(void)didMenuHrizontalClickedButtonAtIndex:(NSInteger)index;

@end

@interface MenuHrizontal : UIView{
    NSMutableArray *_mButtonArray;
    NSMutableArray *_mItemInfoArray;        //按钮信息数组
    UIScrollView   *_mScrollView;
    float          _mTotalWidth;            //总长度
}

@property (nonatomic,assign) id<MenuHrizontalDelegate> delegate;

#pragma mark - 初始化菜单
- (id)initWithFrame:(CGRect)frame buttonItems:(NSArray*)aButtonArray;

#pragma mark - 选中某个按钮
- (void)clickButtonAtIndex:(NSInteger)aIndex;

#pragma mark - 改变第几个button为选中状态，不发送delegate
-(void)changeButtonStateAtIndex:(NSInteger)aIndex;

@end
