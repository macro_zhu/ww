//
//  AppDelegate.m
//  WW
//
//  Created by Macro on 15/6/3.
//  Copyright (c) 2015年 别问我是谁. All rights reserved.
//

#import "AppDelegate.h"
#import "UMSocial.h"
#import "UMSocialQQHandler.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialSinaHandler.h"
#import <IQKeyboardManager.h>
#import "WriteArticleController.h"
#import "MengTuMainController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [[IQKeyboardManager sharedManager] disableToolbarInViewControllerClass:[WriteArticleController class]];
    [[IQKeyboardManager sharedManager] disableToolbarInViewControllerClass:[MengTuMainController class]];
    _tabbarCtrl = [[CustomTabBarController alloc] init];
    self.window.rootViewController = _tabbarCtrl;
    [self.window makeKeyAndVisible];
    
    //设置友盟
    [UMSocialData setAppKey:@"558b6cc767e58eb8fe00108f"];
    
    //设置QQ登录
    [UMSocialQQHandler setQQWithAppId:@"1104650293" appKey:@"MaDOy4TQlZpojQAv" url:@"http://www.umeng.com/social"];
    
    //设置微信登录
    [UMSocialWechatHandler setWXAppId:@"wx33216ce7eb58a430" appSecret:@"911c5959f36890cce13eb76345578c20" url:@"http://www.umeng.com/social"];
    
    //设置微博登录
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
